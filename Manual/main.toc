\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}System configuration and Installation}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Screen resolution issues}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Using Anaconda (recommended)}{2}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Creating environment using the provided .yml file}{2}{subsubsection.2.2.1}%
\contentsline {section}{\numberline {3}Running the CryoNIRSP IPC}{3}{section.3}%
\contentsline {section}{\numberline {4}How to use the IPC}{4}{section.4}%
\contentsline {subsection}{\numberline {4.1}Input and Output tabs}{4}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Common}{5}{subsubsection.4.1.1}%
\contentsline {paragraph}{Observational Settings}{5}{section*.2}%
\contentsline {paragraph}{CryoNIRSP Settings}{6}{section*.3}%
\contentsline {subsubsection}{\numberline {4.1.2}Spectrograph}{6}{subsubsection.4.1.2}%
\contentsline {paragraph}{Wavelength}{6}{section*.4}%
\contentsline {paragraph}{Camera}{6}{section*.5}%
\contentsline {paragraph}{Scanning}{8}{section*.6}%
\contentsline {paragraph}{Slit}{8}{section*.7}%
\contentsline {paragraph}{Configuration}{8}{section*.8}%
\contentsline {subsubsection}{\numberline {4.1.3}Context Imager}{8}{subsubsection.4.1.3}%
\contentsline {paragraph}{Scanning}{9}{section*.9}%
\contentsline {paragraph}{Camera}{9}{section*.10}%
\contentsline {paragraph}{Wavelength}{10}{section*.11}%
\contentsline {paragraph}{Polarimetry}{10}{section*.12}%
\contentsline {paragraph}{Configuration}{11}{section*.13}%
\contentsline {subsubsection}{\numberline {4.1.4}SP output}{11}{subsubsection.4.1.4}%
\contentsline {subsubsection}{\numberline {4.1.5}CI output}{14}{subsubsection.4.1.5}%
\contentsline {subsection}{\numberline {4.2}Map area}{16}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Plot area}{17}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Coronal relative Flux spectrum}{17}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Coronal absolute Flux spectrum}{17}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Corona low resolution SP filter \& spectrum}{18}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}Corona high resolution SP filter \& spectrum}{18}{subsubsection.4.3.4}%
\contentsline {subsubsection}{\numberline {4.3.5}Corona high resolution CI filter \& spectrum}{19}{subsubsection.4.3.5}%
\contentsline {subsubsection}{\numberline {4.3.6}SP coronal line counts in filter passband}{19}{subsubsection.4.3.6}%
\contentsline {subsubsection}{\numberline {4.3.7}SP velocity sensitivity for lines in filter passband}{19}{subsubsection.4.3.7}%
\contentsline {subsubsection}{\numberline {4.3.8}SP magnetic sensitivity for lines in filter passband}{20}{subsubsection.4.3.8}%
\contentsline {subsubsection}{\numberline {4.3.9}Disk Flux spectrum}{20}{subsubsection.4.3.9}%
\contentsline {subsubsection}{\numberline {4.3.10}Disk low resolution SP filter \& spectrum}{20}{subsubsection.4.3.10}%
\contentsline {subsubsection}{\numberline {4.3.11}Disk high resolution SP filter \& spectrum}{20}{subsubsection.4.3.11}%
\contentsline {subsubsection}{\numberline {4.3.12}Disk high resolution CI filter \& spectrum}{21}{subsubsection.4.3.12}%
\contentsline {subsection}{\numberline {4.4}Instrument Program Tree and Timeline}{22}{subsection.4.4}%
\contentsline {paragraph}{Instrument Configuration}{22}{section*.14}%
\contentsline {paragraph}{Instrument Program}{22}{section*.15}%
\contentsline {subsubsection}{\numberline {4.4.1}Add configuration to program control}{22}{subsubsection.4.4.1}%
\contentsline {paragraph}{Simultaneous configuration}{22}{section*.16}%
\contentsline {subsubsection}{\numberline {4.4.2}Tree}{22}{subsubsection.4.4.2}%
\contentsline {subsubsection}{\numberline {4.4.3}Timeline}{23}{subsubsection.4.4.3}%
\contentsline {subsection}{\numberline {4.5}Menu list}{24}{subsection.4.5}%
\contentsline {subsubsection}{\numberline {4.5.1}File}{24}{subsubsection.4.5.1}%
\contentsline {paragraph}{Instrument Configuration}{24}{section*.17}%
\contentsline {paragraph}{Instrument Program}{24}{section*.18}%
\contentsline {subsubsection}{\numberline {4.5.2}Input Models}{25}{subsubsection.4.5.2}%
\contentsline {subsubsection}{\numberline {4.5.3}Advanced}{25}{subsubsection.4.5.3}%
\contentsline {section}{\numberline {5}Change Log}{25}{section.5}%
\contentsline {subsection}{\numberline {5.1}Version 20200515-1 15. May 2020}{25}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Version 1 Initial Release (13. September 2018)}{25}{subsection.5.2}%
\contentsline {section}{References}{25}{section*.19}%
