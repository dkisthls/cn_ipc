#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  4 12:38:21 2019

@author: Andre Fehlmann (afehlmann@nso.edu)

Revision History
----------------
  - 16 Jan 2019: initial version
  - 22 Jan 2019: added idealH2RG and dreamH2RG modes
"""
import numpy as np

class cnCameraTiming():
    """
    TBD
    """
    cameraReadyTime = 0

    # only fast mode supports continuous modulation

    def __init__(self,
                 cameraMode,
                 exposureTime,
                 y,
                 h,
                 modulationMode,
                 modulationSteps=None):

        # set attributes
        self.cameraMode = cameraMode
        self.exposureTime = exposureTime
        self.y = y
        self.h = h
        self.modulationMode = modulationMode
        self.modulationSteps = modulationSteps
        self.slowFrameTime = None
        self.fastFrameTime = None

        # call update methode
        self.update()

    def cameraFrameTimes(self):
        """
        TBD
        """
        self.cameraMode = "slow"
        self.slowFrameTime = self.update(frameTimeOnly=True)
        self.cameraMode = "fast"
        self.fastFrameTime = self.update(frameTimeOnly=True)
        if self.exposureTime >= self.slowFrameTime:
            self.cameraMode = "slow"
        elif (self.exposureTime < self.slowFrameTime) & (self.exposureTime >= self.fastFrameTime):
            self.cameraMode = "fast"
        else:
            self.cameraMode = "lineByLine"
        # print(self.slowFrameTime, self.fastFrameTime,self.cameraMode)

    def update(self, frameTimeOnly=False):
        """
        update
        """
        # figure out the best camera mode
        if self.cameraMode == "automatic":
            self.cameraFrameTimes()

        if self.cameraMode == "slow":
            self.lat = 240e-9
            self.lt = 243400e-9
            self.frst = 1500e-9
            self.fsync = 620e-9
            self.vclk = 500e-9
            self.hrst = None
            self.rend = 0
            self.sdel = None
            self.lrst = None
            self.lend = None
            self.fourLineTime = None

            self.frameTime = self.fsync + self.y*self.vclk + self.h*self.lt

            if frameTimeOnly:
                return self.frameTime
            else:
                assert(self.exposureTime >= self.frameTime), "exposure time to short (< frame time)"
                self.ndr = np.floor(self.exposureTime/self.frameTime)+1
                self.dt = (self.exposureTime - (self.ndr-1)*self.frameTime)/(self.ndr-1)
                # bring dt to integer of clock cycle (20ns)
                self.dt = np.round(self.dt/2e-8)*2e-8
                assert(self.dt >= 0.), "too many NDRs for given exposure time"
                self.rampTime = (self.lat + self.frst + 2*self.frameTime
                                 + (self.ndr-1)*(self.frameTime+self.dt) + self.rend)
                self.frameRate = 1./((self.rampTime-self.lat-self.frst)/self.ndr)
                self.prepTime = self.lat + self.frst + self.frameTime
                self.endTime = 0
                #TODO: this does not include windowing
                self.maxPixelExposure = (self.frameTime            # this is the reset time
                                         + (self.ndr-1)*(self.frameTime+self.dt))

        elif self.cameraMode == "fast":
            self.lat = 240e-9
            self.lineByLineReset = 2090800e-9  # eric 2090785e-9
            self.lt = 44120e-9
            self.frst = 1500e-9
            self.fsync = 180e-9
            self.vclk = 220e-9
            self.hrst = None
            self.rend = 20000e-9   # eric 20000e-9
            self.sdel = None
            self.lrst = None
            self.lend = None
            self.fourLineTime = None

            # extra line because when clocking through the first row (is sampling and putting in
            # hold stage), so clock through last line again to read data out of hold stage
            self.frameTime = self.fsync + self.y*self.vclk + (self.h+1)*self.lt
            # round frame time because of precision issue
            self.frameTime = np.round(self.frameTime,9)
            if frameTimeOnly:
                return self.frameTime
            else:
                # print(self.frameTime, self.exposureTime)
                assert(self.exposureTime >= self.frameTime), "exposure time to short (< frame time)"
                if not self.modulationMode == "Continuous":
                    self.ndr = np.floor(self.exposureTime/self.frameTime)+2
                    self.dt = (self.exposureTime - (self.ndr-2)*self.frameTime)/(self.ndr-2)
                    self.prepTime = (self.lat + self.frst + self.lineByLineReset
                                     + self.frameTime + self.dt)
                    self.endTime = 0
                else:# self.modulationMode is "Discrete":
                    self.ndr = self.modulationSteps +2
                    self.dt = self.exposureTime - self.frameTime
                    self.prepTime = self.lat + self.frst + self.lineByLineReset
                    self.endTime = 0
                # bring dt to integer of clock cycle (20ns)
                self.dt = np.round(self.dt/2e-8)*2e-8
                assert(self.dt >= 0.), "too many NDRs for given exposure time"
                self.rampTime = (self.lat + self.frst + self.lineByLineReset
                                 + self.ndr*self.frameTime
                                 + (self.ndr-1)*self.dt + self.rend)
                self.frameRate = 1./((self.rampTime-self.lat-self.frst
                                      - self.lineByLineReset-self.rend)
                                     /self.ndr)
                # line by line reset
                # print('\nfast mode pixel components', self.lineByLineReset,
                #       (self.ndr-1)*(self.frameTime+self.dt))
                self.maxPixelExposure = (self.lineByLineReset     # reset time
                                         + (self.ndr-1)*(self.frameTime+self.dt))

        elif self.cameraMode == "lineByLine":
            self.lat = 240e-9
            self.lt = 44120e-9
            self.frst = 1500e-9
            self.fsync = 180e-9
            self.vclk = 220e-9
            self.hrst = 320e-9     # eric 313.3e-9
            self.rend = 20000e-9   # eric 20000e-9
            self.sdel = None
            self.lrst = 2000e-9
            self.lend = 44520e-9 # t_clkontoarray + t_1stpixeldwell + (channelWidth + 0.5) * pixelTime + t_lastpixelLow + t_hrst

            self.lineByLineTime = 2*(self.lt + self.hrst - self.vclk)
            # round linebyline time because of precision issue
            self.lineByLineTime = np.round(self.lineByLineTime, 9)
            assert(self.exposureTime >= self.lineByLineTime), "exposure time to short"
            self.ndr = 1
            self.dt = self.exposureTime - 2*(self.lt + self.hrst - self.vclk)
            # bring dt to integer of clock cycle (20ns)
            self.dt = np.round(self.dt/2e-8)*2e-8
            self.fourLineTime = self.lrst + 4*self.lt + 3*self.hrst - 3*self.vclk + self.dt
            self.frameTime = np.round(self.fsync + self.y*self.vclk + self.h*self.fourLineTime + self.lend, 9)
            self.rampTime = np.round(self.lat + self.frst + self.frameTime + self.rend, 9)
            self.frameRate = 1./(self.frameTime/4)
            self.prepTime = self.lat + self.frst
            self.endTime = 0
            self.maxPixelExposure = 3*(self.lt + self.hrst - self.vclk) + self.dt

        elif self.cameraMode == "idealH2RG":
            # to get 10Hz frame rate choose 0.05 s integration time
            self.lat = 0
            self.lt = 23380e-9
            self.frst = 1500e-9
            self.fsync = 180e-9
            self.vclk = 200e-9
            self.hrst = None
            self.rend = 2000e-9
            self.sdel = 2114020e-9
            self.lrst = None
            self.lend = None
            self.fourLineTime = None

            # extra line because when clocking through the first row (is sampling and putting in
            # hold stage), so clock through last line again to read data out of hold stage
            self.frameTime = self.fsync + self.y*self.vclk + (self.h)*self.lt
            if frameTimeOnly:
                return self.frameTime
            else:
                assert(self.exposureTime >= self.frameTime), "exposure time to short (< frame time)"
                if self.modulationMode is not "Continuous":
                    self.ndr = 2 #np.floor(self.exposureTime/self.frameTime)+1
                    self.dt = (self.exposureTime - (self.ndr-1)*self.frameTime)/(self.ndr-1)
                    self.prepTime = self.lat + self.frst + self.sdel
                    self.endTime = 0
                else:
                    self.ndr = 2#self.modulationSteps +1
                    self.dt = self.exposureTime - self.frameTime
                    self.prepTime = self.lat + self.frst + self.sdel
                    self.endTime = 0
                # bring dt to integer of clock cycle (20ns)
                self.dt = np.round(self.dt/2e-8)*2e-8
                assert(self.dt >= 0.), "too many NDRs for given exposure time"
                self.rampTime = (self.lat + self.frst + self.sdel + self.ndr*self.frameTime
                                 + (self.ndr-1)*self.dt + self.rend)
                self.frameRate = 1./((self.rampTime-self.lat-self.frst-self.sdel-self.rend)
                                     / self.ndr)
                self.maxPixelExposure = self.h*self.lt+ (self.ndr-1)*(self.frameTime+self.dt)

        elif self.cameraMode == "dreamH2RG":
            self.lat = None
            self.lt = None
            self.frst = None
            self.fsync = None
            self.vclk = None
            self.hrst = None
            self.rend = 0
            self.sdel = None
            self.lrst = None
            self.lend = None
            self.fourLineTime = None

            # extra line because when clocking through the first row (is sampling and putting in
            # hold stage), so clock through last line again to read data out of hold stage
            self.frameTime = 0.1
            if frameTimeOnly:
                return self.frameTime
            else:
                assert(self.exposureTime >= self.frameTime), "exposure time to short (< frame time)"
                if self.modulationMode is not "Continuous":
                    self.ndr = 1
                    self.dt = self.exposureTime - self.ndr*self.frameTime
                    self.prepTime = 0
                    self.endTime = 0
                else:# self.modulationMode is "Discrete":
                    self.ndr = 1#self.modulationSteps
                    self.dt = self.exposureTime - self.frameTime
                    self.prepTime = 0
                    self.endTime = 0
                # bring dt to integer of clock cycle (20ns)
                self.dt = np.round(self.dt/2e-8)*2e-8
                assert(self.dt >= 0.), "too many NDRs for given exposure time"
                self.rampTime = self.ndr*self.frameTime
                self.frameRate = 1./(self.rampTime/self.ndr)
                self.maxPixelExposure = self.frameTime

        else:
            print("Did not enter any camera mode")

        if self.modulationMode is not "Continuous":
            self.dutyCycle = self.exposureTime/self.rampTime
        else:
            self.dutyCycle = self.modulationSteps*self.exposureTime/self.rampTime


if __name__ == "__main__":
    # self,
    #              cameraMode,
    #              exposureTime,
    #              y,
    #              h,
    #              modulationMode,
    #              modulationSteps=None):
    test = cnCameraTiming("slow", 0.900, 0, 2048, "Discrete")
    print('slow', test.ndr, test.frameTime, test.rampTime, test.dt)
    test = cnCameraTiming("fast", 0.09040206, 0, 2048, "Discrete")
    print('fast', test.ndr, test.frameTime, test.rampTime, test.dt)
    test = cnCameraTiming("fast", 0.09040206, 0, 2048, "Continuous", 8)
    print(test.ndr, test.frameTime, test.rampTime, test.dt)
    test = cnCameraTiming("lineByLine", 0.0001, 0, 2048, "Discrete")
    print('line', test.ndr, test.lineByLineTime, test.fourLineTime, test.frameTime, test.rampTime, test.dt)
