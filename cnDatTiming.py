#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 12:52:59 2019

@author: Andre Fehlmann (afehlmann@nso.edu)

Revision History
----------------
  -24 Jan 2019: initial version


Notes
------
  - modulator in discrete mode supports only dwell time at state and modulation
    rate. But state duration is really only a minimal required dwell time. The
    modulator move time is fixe to 110ms and dwell time will be extendted to
    match requested rate
"""

from cnCameraTiming import cnCameraTiming
import numpy as np

def fm1Timing(stepSize, speed, Ta, Td, Tsettle):
    #   T_total = Ta + distance/speed + Td. For short moves where the motor
    #   does not reach full speed, i.e. distance <= speed*Ta, the move is described
    #   by a triangular move profile and
    #   T_total = sqrt(distance*Ta/speed) + sqrt(distance*Td/speed).
    if stepSize <= speed*Ta:
        # print("triangle",speed*Ta)
        # totalTime = np.sqrt(stepSize*Ta/speed) + np.sqrt(stepSize*Td/speed)
        totalTime = Ta + Td + Tsettle
    else:
        # print("trapezoidal")
        # totalTime = Ta + stepSize/speed + Td
        totalTime = Ta/2 + stepSize/speed + Td/2 + Tsettle
    return totalTime

class cnDatTiming():

    def __init__(self,
                 camTime,
                 modulator,
                 modulationMode,
                 modulationSteps,
                 modulatorMoveTime,
                 instrument,
                 cameraMode,
                 numMeasurements,
                 scanningPattern,
                 horizSpeed,
                 horizTa,
                 horizTd,
                 horizSettle,
                 numHorizPositions,
                 horizStepSize,
                 vertiSpeed,
                 vertiTa,
                 vertiTd,
                 vertiSettle,
                 numVertiPositions,
                 vertiStepSize,
                 secCameraMode=None,
                 secNumMeasurements=None,
                 debug=False):

        self.debug = debug
        self.instrument = instrument
        self.cameraMode = cameraMode
        
        self.numMeasurements = numMeasurements
        self.modulator = modulator
        self.modulationMode = modulationMode
        self.modulationSteps = modulationSteps
        self.modMoveTime = modulatorMoveTime
        self.scanningPattern = scanningPattern
        self.horizSpeed = horizSpeed
        self.horizTa = horizTa
        self.horizTd = horizTd
        self.horizTsettle = horizSettle
        self.vertiSpeed = vertiSpeed
        self.vertiTa = vertiTa
        self.vertiTd = vertiTd
        self.vertiTsettle = vertiSettle
        self.numHorizPositions = numHorizPositions
        self.numVertiPositions = numVertiPositions
        self.horizStepSize = horizStepSize
        self.vertiStepSize = vertiStepSize
        self.secCameraMode = secCameraMode
        # self.secExposureTime = secExposureTime
        # self.secY = secY
        # self.secH = secH
        # self.secCoAdds = secCoAdds
        self.secNumMeasurements = secNumMeasurements
        # define attributes in init
        self.bottomMoveTimes = None
        self.bottomReqMoveTime = None
        self.bottomSlowest = None
        self.bottomFil = None
        self.modDwellTime = None
        self.modPeriod = None
        self.modRate = None
        self.cameraPeriod = None
        self.cameraRate = None
        self.bottomPeriod = None
        self.bottomRate = None
        self.bottomNumFrames = None
        self.bottomExtraModPeriods = None
        self.firstPeriod = None
        self.firstSlowest = None
        self.firstMechDifference = None
        self.firstReqMoveTime = None
        self.firstExtraModPeriods = None
        self.firstReqMoveTime = None
        self.firstRate = None
        self.firstNumFrames = None
        self.secondPeriod = None
        self.secondSlowest = None
        self.secondMechDifference = None
        self.secondExtraModPeriods = None
        self.secondNumFrames = None
        self.secondRate = None


        self.update(camTime)

    def layerCalculation(self, mode, camTime):
        # follow Becky's approach of 3 layers
        # bottom layer is a single step horizontal step position
        # first layer is all steps in the first scan direction
        # second layer is all steps in the second scan direction
        if self.debug:
            print("mode in layer calculation", mode, "\n")

        if mode == "Discrete":
            if self.debug:
                print("modMOveTime", self.modMoveTime, "\n")
            # bottom layer for a single camera ramp
            # distinguish between modulator in or out
            if self.modulator is "On":
                self.bottomMoveTimes = np.array([camTime.prepTime + camTime.cameraReadyTime,
                                                 self.modMoveTime])
            else:
                assert(self.modulationSteps == 1), "with modulator out of the beam, \
                    modulationSteps needs to be 1."
                self.bottomMoveTimes = np.array([camTime.prepTime + camTime.cameraReadyTime])
            # decide if camera or modulator defines bottom layer timing
            ind = np.argmax(self.bottomMoveTimes)
            self.bottomReqMoveTime = self.bottomMoveTimes[ind]
            if self.debug:
                print("bottomReqMoveTime", self.bottomReqMoveTime)
            if ind == 0:
                self.bottomSlowest = "camera"
                self.bottomFil = 0
                # modulator dwell time has to be increased to account for fixed move time
                self.modDwellTime = camTime.rampTime + camTime.cameraReadyTime - self.modMoveTime
            else:
                self.bottomSlowest = "modulator"
                self.bottomFil = self.modMoveTime - camTime.prepTime - camTime.cameraReadyTime
                # modulator dwells only for the time of the actual exposure
                self.modDwellTime = camTime.rampTime - camTime.prepTime

            # independently calculate period and rates to cross check
            self.modPeriod = self.modDwellTime + self.modMoveTime
            self.modRate = 1/self.modPeriod
            self.cameraPeriod = camTime.rampTime + camTime.cameraReadyTime + self.bottomFil
            self.cameraRate = 1/self.cameraPeriod

            if self.debug:
                print("\nBottom layer:")
                print("slowest", self.bottomSlowest)
                print("bottomFil", self.bottomFil)
                print("modDwellTime", self.modDwellTime)
                print("modPeriod", self.modPeriod)
                print("cameraPeriod", self.cameraPeriod)
            # at single slit position ramp can be repeated for a given modulation step
            self.bottomPeriod = self.cameraPeriod*self.modulationSteps*self.numMeasurements
            self.bottomRate = 1/self.bottomPeriod
            self.bottomNumFrames = camTime.ndr*self.modulationSteps*self.numMeasurements
            if self.debug:
                print("bottom period", self.bottomPeriod)


            # first layer Horizontal moves (could be only fixed position)
            if self.numHorizPositions == 1:
                self.firstPeriod = self.bottomPeriod
                self.firstSlowest = None
            else:
                # check if horizontal move time larger than largest bottom move Time
                self.firstMechDifference = self.horizMoveTime - self.bottomReqMoveTime
                if self.firstMechDifference > 0:
                    self.firstSlowest = "horizontal"
                    self.firstReqMoveTime = self.horizMoveTime
                    # how many modPeriods have to be added to cover horizontal move
                    self.firstExtraModPeriods = np.ceil(self.firstMechDifference/self.modPeriod)
                    #!!!: There is no horizontal move at beginning
                    self.firstPeriod = (self.bottomPeriod*self.numHorizPositions +
                                        (self.firstExtraModPeriods*self.modPeriod
                                         *(self.numHorizPositions-1)))
                else:
                    self.firstSlowest = "bottom"
                    self.firstReqMoveTime = self.bottomReqMoveTime
                    self.firstExtraModPeriods = 0
                    self.firstPeriod = self.bottomPeriod*self.numHorizPositions
            self.firstRate = 1/self.firstPeriod
            self.firstNumFrames = self.bottomNumFrames*self.numHorizPositions
            if self.debug:
                print("\nFirst:")
                print("mechDiffernece", self.firstMechDifference)
                print("slowest", self.firstSlowest)
                print("first period", self.firstPeriod)
                print("first extra mod periods", self.firstExtraModPeriods)

            # second layer
            # factor in scanning
            #TODO: both axis could have only one position (think CI)
            if self.numVertiPositions == 1:
                self.secondPeriod = self.firstPeriod
                self.secondSlowest = None
                self.secondMechDifference = None
                self.secondExtraModPeriods = None
                self.secondNumFrames = self.firstNumFrames
            else:
                if self.scanningPattern is "raster":
                    # check horizontal reset time and vertical move time vs modulator move time
                    self.secondMechDifference = (max([self.vertiMoveTime, self.horizResetMoveTime])
                                                 - self.firstReqMoveTime)
                    if self.secondMechDifference > 0:
                        if self.vertiMoveTime > self.horizResetMoveTime:
                            self.secondSlowest = "vertical"
                        else:
                            self.secondSlowest = "horizontal reset"
                        # how many modPeriods have to be added
                        self.secondExtraModPeriods = np.ceil(self.secondMechDifference
                                                             /self.modPeriod)
                        # There is no vertical move in the beginning
                        self.secondPeriod = (self.firstPeriod*self.numVertiPositions
                                             + self.secondExtraModPeriods*self.modPeriod
                                             *(self.numVertiPositions-1))
                    else:
                        self.secondSlowest = "first"
                        self.secondExtraModPeriods = 0
                        self.secondPeriod = self.firstPeriod*self.numVertiPositions
                elif self.scanningPattern is "optimized":
                    # no horizontal move is done after last ramp in first layer
                    # check vertical move time vs modulator move time
                    self.secondMechDifference = self.vertiMoveTime - self.firstReqMoveTime
                    if self.secondMechDifference > 0:
                        self.secondSlowest = "vertical"
                        # how many modPeriods have to be added
                        self.secondExtraModPeriods = np.ceil(self.secondMechDifference
                                                             /self.modPeriod)
                        # After the last ramp there is no more vertical move
                        self.secondPeriod = (self.firstPeriod*self.numVertiPositions +
                                             self.secondExtraModPeriods*self.modPeriod
                                             *(self.numVertiPositions-1))
                    else:
                        self.secondSlowest = "first"
                        self.secondExtraModPeriods = 0
                        self.secondPeriod = self.firstPeriod*self.numVertiPositions
            self.secondRate = 1/self.secondPeriod
            self.secondNumFrames = self.firstNumFrames*self.numVertiPositions
            if self.debug:
                print("\nSecond:")
                print("mechDiffernece", self.secondMechDifference)
                print("slowest", self.secondSlowest)
                print("Period", self.secondPeriod)
                print("second extra mod periods", self.secondExtraModPeriods)

        # continuous mode
        elif mode == "Continuous":
            if self.cameraMode is "fast":
                # bottom layer for a single camera ramp
                # exposing during one modulation state
                self.modPeriod = camTime.exposureTime
                #camTime.cameraReadyTime = 0.15
                # distinguish between modulator in or out
                if self.modulator is "On":
                    self.bottomMoveTimes = np.array([camTime.prepTime + camTime.cameraReadyTime,
                                                     self.modPeriod])
                else:
                    print("conflict continuous mode and out")
                # decide if camera or modulator defines bottom layer timing
                ind = np.argmax(self.bottomMoveTimes)
                self.bottomReqMoveTime = self.bottomMoveTimes[ind]
                if self.debug:
                    print("bottomReqMoveTime", self.bottomReqMoveTime, "\n")
                if ind == 0:
                    self.bottomSlowest = "camera"
                    self.bottomExtraModPeriods = np.ceil(self.bottomMoveTimes[0]
                                                         /self.bottomMoveTimes[1])
                    self.bottomFil = (self.modPeriod*self.bottomExtraModPeriods - camTime.prepTime
                                      - camTime.cameraReadyTime + (camTime.dt-camTime.rend))
                else:
                    self.bottomSlowest = "modulator"
                    self.bottomFil = (self.modPeriod - camTime.prepTime - camTime.cameraReadyTime
                                      + (camTime.dt-camTime.rend))
                    self.bottomExtraModPeriods = 1

                # independently calculate period and rates to cross check
                self.modRate = 1/self.modPeriod
                self.cameraPeriod = camTime.rampTime + camTime.cameraReadyTime + self.bottomFil
                self.cameraRate = 1/self.cameraPeriod

                if self.debug:
                    print("Bottom layer:")
                    print("slowest", self.bottomSlowest)
                    print("bottomFil", self.bottomFil)
                    print("modPeriod", self.modPeriod)
                    print("cameraPeriod", self.cameraPeriod)
                # at single slit position ramp can be repeated but extra mod periods might
                # be needed
                if self.numMeasurements == 1:
                    self.bottomPeriod = self.cameraPeriod
                else:
                    self.bottomPeriod = (((camTime.ndr+self.bottomExtraModPeriods)*self.modPeriod)
                                         *self.numMeasurements)
                self.bottomRate = 1/self.bottomPeriod
                self.bottomNumFrames = camTime.ndr * self.numMeasurements
                if self.debug:
                    print("bottom period", self.bottomPeriod)
                    print("bottom extra mod periods", self.bottomExtraModPeriods, "\n")

                # first layer Horizontal moves (could be only fixed position)
                if self.numHorizPositions == 1:
                    self.firstPeriod = self.bottomPeriod
                    self.firstSlowest = None
                    self.firstMechDifference = None
                    self.firstExtraModPeriods = None
                else:
                    # check if horizontal move time larger than largest bottom move Time
                    self.firstMechDifference = self.horizMoveTime - self.bottomReqMoveTime
                    if self.firstMechDifference > 0:
                        self.firstSlowest = "horizontal"
                        self.firstReqMoveTime = self.horizMoveTime
                        # how many modPeriods have to be added to cover horizontal move
                        self.firstExtraModPeriods = np.ceil(self.firstMechDifference/self.modPeriod)
                        #!!!: There is no horizontal move at beginning
                        self.firstPeriod = (self.bottomPeriod*self.numHorizPositions +
                                            (self.firstExtraModPeriods*self.modPeriod
                                             *(self.numHorizPositions-1)))
                    else:
                        self.firstSlowest = "bottom"
                        self.firstReqMoveTime = self.bottomReqMoveTime
                        self.firstExtraModPeriods = 0
                        self.firstPeriod = self.bottomPeriod*self.numHorizPositions
                self.firstRate = 1/self.firstPeriod
                self.firstNumFrames = self.bottomNumFrames*self.numHorizPositions
                if self.debug:
                    print("First:")
                    print("mechDiffernece", self.firstMechDifference)
                    print("slowest", self.firstSlowest)
                    print("first period", self.firstPeriod)
                    print("first extra mod periods", self.firstExtraModPeriods)

                # second layer
                # factor in scanning
                #TODO: both axis could have only one position (think CI)
                if self.numVertiPositions == 1:
                    self.secondPeriod = self.firstPeriod
                    self.secondSlowest = None
                    self.secondMechDifference = None
                    self.secondExtraModPeriods = None
                    self.secondNumFrames = self.firstNumFrames
                else:
                    if self.scanningPattern is "raster":
                        # check horizontal reset time and vertical move time vs modulator move time
                        self.secondMechDifference = (max([self.vertiMoveTime,
                                                          self.horizResetMoveTime])
                                                     - self.firstReqMoveTime)
                        if self.secondMechDifference > 0:
                            if self.vertiMoveTime > self.horizResetMoveTime:
                                self.secondSlowest = "vertical"
                            else:
                                self.secondSlowest = "horizontal reset"
                            # how many modPeriods have to be added
                            self.secondExtraModPeriods = np.ceil(self.secondMechDifference
                                                                 /self.modPeriod)
                            # There is no vertical move in the beginning
                            self.secondPeriod = (self.firstPeriod*self.numVertiPositions +
                                                 self.secondExtraModPeriods*self.modPeriod
                                                 *(self.numVertiPositions-1))
                        else:
                            self.secondSlowest = "first"
                            self.secondExtraModPeriods = 0
                            self.secondPeriod = self.firstPeriod*self.numVertiPositions
                    elif self.scanningPattern is "optimized":
                        # no horizontal move is done after last ramp in first layer
                        # check vertical move time vs modulator move time
                        self.secondMechDifference = self.vertiMoveTime - self.firstReqMoveTime
                        if self.secondMechDifference > 0:
                            self.secondSlowest = "vertical"
                            # how many modPeriods have to be added
                            self.secondExtraModPeriods = np.ceil(self.secondMechDifference
                                                                 /self.modPeriod)
                            # After the last ramp there is no more vertical move
                            self.secondPeriod = (self.firstPeriod*self.numVertiPositions +
                                                 self.secondExtraModPeriods*self.modPeriod
                                                 *(self.numVertiPositions-1))
                        else:
                            self.secondSlowest = "first"
                            self.secondExtraModPeriods = 0
                            self.secondPeriod = self.firstPeriod*self.numVertiPositions
                self.secondRate = 1/self.secondPeriod
                self.secondNumFrames = self.firstNumFrames*self.numVertiPositions
                if self.debug:
                    print("\nSecond:")
                    print("mechDiffernece", self.secondMechDifference)
                    print("slowest", self.secondSlowest)
                    print("Period", self.secondPeriod)
                    print("second extra mod periods", self.secondExtraModPeriods)
            else:
                # bottom layer for a single camera ramp
                # exposing during one modulation state
                # if self.cameraMode is "dreamH2RG":
                #   self.modPeriod = self.exposureTime
                # else:
                self.modPeriod = camTime.rampTime
                #camTime.cameraReadyTime = 0.15
                # distinguish between modulator in or out
                if self.modulator == "On":
                    self.bottomMoveTimes = np.array([camTime.prepTime + camTime.cameraReadyTime,
                                                     self.modPeriod])
                else:
                    print("conflict continuous mode and out")
                # decide if camera or modulator defines bottom layer timing
                ind = np.argmax(self.bottomMoveTimes)
                self.bottomReqMoveTime = self.bottomMoveTimes[ind]
                if self.debug:
                    print("bottomReqMoveTime", self.bottomReqMoveTime, "\n")
                if ind == 0:
                    self.bottomSlowest = "camera"
                    self.bottomExtraModPeriods = np.ceil(self.bottomMoveTimes[0]
                                                         /self.bottomMoveTimes[1])
                    self.bottomFil = (self.modPeriod*self.bottomExtraModPeriods - camTime.prepTime
                                      - camTime.cameraReadyTime + (camTime.dt-camTime.rend))
                else:
                    self.bottomSlowest = "modulator"
                    print(self.modPeriod, camTime.prepTime, camTime.cameraReadyTime, camTime.dt,
                          camTime.rend)
                    self.bottomFil = (self.modPeriod - camTime.prepTime - camTime.cameraReadyTime
                                      + (camTime.dt-camTime.rend))
                    self.bottomExtraModPeriods = 0

                # independently calculate period and rates to cross check
                self.modRate = 1/self.modPeriod
                self.cameraPeriod = camTime.rampTime #+ camTime.cameraReadyTime + self.bottomFil
                self.cameraRate = 1/self.cameraPeriod

                if self.debug:
                    print("Bottom layer:")
                    print("slowest", self.bottomSlowest)
                    print("bottomFil", self.bottomFil)
                    print("modPeriod", self.modPeriod)
                    print("cameraPeriod", self.cameraPeriod)
                # at single slit position ramp can be repeated but extra mod periods might
                # be needed
                if self.numMeasurements == 1:
                    self.bottomPeriod = self.modPeriod*self.modulationSteps
                else:
                    self.bottomPeriod = self.modulationSteps*self.modPeriod*self.numMeasurements
                self.bottomRate = 1/self.bottomPeriod
                if self.debug:
                    print("bottom period", self.bottomPeriod)
                    print("bottom extra mod periods", self.bottomExtraModPeriods, "\n")

                # first layer Horizontal moves (could be only fixed position)
                if self.numHorizPositions == 1:
                    self.firstPeriod = self.bottomPeriod
                    self.firstSlowest = None
                    self.firstMechDifference = None
                    self.firstExtraModPeriods = None
                else:
                    # check if horizontal move time larger than largest bottom move Time
                    self.firstMechDifference = self.horizMoveTime - self.bottomReqMoveTime
                    if self.firstMechDifference > 0:
                        self.firstSlowest = "horizontal"
                        self.firstReqMoveTime = self.horizMoveTime
                        # how many modPeriods have to be added to cover horizontal move
                        self.firstExtraModPeriods = np.ceil(self.firstMechDifference/self.modPeriod)
                        #!!!: There is no horizontal move at beginning
                        self.firstPeriod = (self.bottomPeriod*self.numHorizPositions +
                                            (self.firstExtraModPeriods*self.modPeriod
                                             *(self.numHorizPositions-1)))
                    else:
                        self.firstSlowest = "bottom"
                        self.firstReqMoveTime = self.bottomReqMoveTime
                        self.firstExtraModPeriods = 0
                        self.firstPeriod = self.bottomPeriod*self.numHorizPositions
                self.firstRate = 1/self.firstPeriod
                if self.debug:
                    print("First:")
                    print("mechDiffernece", self.firstMechDifference)
                    print("slowest", self.firstSlowest)
                    print("first period", self.firstPeriod)
                    print("first extra mod periods", self.firstExtraModPeriods)

                # second layer
                # factor in scanning
                #TODO: both axis could have only one position (think CI)
                if self.numVertiPositions == 1:
                    self.secondPeriod = self.firstPeriod
                    self.secondSlowest = None
                    self.secondMechDifference = None
                    self.secondExtraModPeriods = None
                    self.secondNumFrames = self.firstNumFrames
                else:
                    if self.scanningPattern is "raster":
                        # check horizontal reset time and vertical move time vs modulator move time
                        self.secondMechDifference = (max([self.vertiMoveTime,
                                                          self.horizResetMoveTime])
                                                     - self.firstReqMoveTime)
                        if self.secondMechDifference > 0:
                            if self.vertiMoveTime > self.horizResetMoveTime:
                                self.secondSlowest = "vertical"
                            else:
                                self.secondSlowest = "horizontal reset"
                            # how many modPeriods have to be added
                            self.secondExtraModPeriods = np.ceil(self.secondMechDifference
                                                                 /self.modPeriod)
                            # There is no vertical move in the beginning
                            self.secondPeriod = (self.firstPeriod*self.numVertiPositions +
                                                 self.secondExtraModPeriods*self.modPeriod
                                                 *(self.numVertiPositions-1))
                        else:
                            self.secondSlowest = "first"
                            self.secondExtraModPeriods = 0
                            self.secondPeriod = self.firstPeriod*self.numVertiPositions
                    elif self.scanningPattern is "optimized":
                        # no horizontal move is done after last ramp in first layer
                        # check vertical move time vs modulator move time
                        self.secondMechDifference = self.vertiMoveTime - self.firstReqMoveTime
                        if self.secondMechDifference > 0:
                            self.secondSlowest = "vertical"
                            # how many modPeriods have to be added
                            self.secondExtraModPeriods = np.ceil(self.secondMechDifference
                                                                 /self.modPeriod)
                            # After the last ramp there is no more vertical move
                            self.secondPeriod = (self.firstPeriod*self.numVertiPositions +
                                                 self.secondExtraModPeriods*self.modPeriod
                                                 *(self.numVertiPositions-1))
                        else:
                            self.secondSlowest = "first"
                            self.secondExtraModPeriods = 0
                            self.secondPeriod = self.firstPeriod*self.numVertiPositions
                self.secondRate = 1/self.secondPeriod
                if self.debug:
                    print("\nSecond:")
                    print("mechDiffernece", self.secondMechDifference)
                    print("slowest", self.secondSlowest)
                    print("Period", self.secondPeriod)
                    print("second extra mod periods", self.secondExtraModPeriods)
        #TODO: implement calculation if modulator is out/off
        elif mode is None:
            if self.debug:
                print("modMOveTime", None, "\n")
            # bottom layer for a single camera ramp
            # distinguish between modulator in or out
            if self.modulator is "On":
                self.bottomMoveTimes = np.array([camTime.prepTime + camTime.cameraReadyTime,
                                                 self.modMoveTime])
            else:
                assert(self.modulationSteps == 1), "with modulator out of the beam,\
                     modulationSteps needs to be 1."
                self.bottomMoveTimes = np.array([camTime.prepTime + camTime.cameraReadyTime])
            # decide if camera or modulator defines bottom layer timing
            ind = np.argmax(self.bottomMoveTimes)
            self.bottomReqMoveTime = self.bottomMoveTimes[ind]
            if self.debug:
                print("bottomReqMoveTime", self.bottomReqMoveTime)
            if ind == 0:
                self.bottomSlowest = "camera"
                self.bottomFil = 0
                # modulator dwell time has to be increased to account for fixed move time
                self.modDwellTime = None
                #camTime.rampTime + camTime.cameraReadyTime - self.modMoveTime
            # else:
            #   self.bottomSlowest = "modulator"
            #   self.bottomFil = self.modMoveTime - camTime.prepTime - camTime.cameraReadyTime
            #   # modulator dwells only for the time of the actual exposure
            #   self.modDwellTime = camTime.rampTime - camTime.prepTime

            # independently calculate period and rates to cross check
            self.modPeriod = None#self.modDwellTime + self.modMoveTime
            self.modRate = None #1/self.modPeriod
            self.cameraPeriod = camTime.rampTime + camTime.cameraReadyTime + self.bottomFil
            self.cameraRate = 1/self.cameraPeriod

            if self.debug:
                print("\nBottom layer:")
                print("slowest", self.bottomSlowest)
                print("bottomFil", self.bottomFil)
                print("modDwellTime", self.modDwellTime)
                print("modPeriod", self.modPeriod)
                print("cameraPeriod", self.cameraPeriod)
            # at single slit position ramp can be repeated for a given modulation step
            self.bottomPeriod = self.cameraPeriod*self.modulationSteps*self.numMeasurements
            self.bottomRate = 1/self.bottomPeriod
            self.bottomNumFrames = camTime.ndr*self.modulationSteps*self.numMeasurements
            if self.debug:
                print("bottom period", self.bottomPeriod)


            # first layer Horizontal moves (could be only fixed position)
            if self.numHorizPositions == 1:
                self.firstPeriod = self.bottomPeriod
                self.firstSlowest = None
            else:
                # check if horizontal move time larger than largest bottom move Time
                self.firstMechDifference = self.horizMoveTime - self.bottomReqMoveTime
                if self.firstMechDifference > 0:
                    self.firstSlowest = "horizontal"
                    self.firstReqMoveTime = self.horizMoveTime
                    # how many modPeriods have to be added to cover horizontal move
                    self.firstExtraModPeriods = np.ceil(self.firstMechDifference/self.cameraPeriod)
                    #!!!: There is no horizontal move at beginning
                    self.firstPeriod = (self.bottomPeriod*self.numHorizPositions +
                                        (self.firstExtraModPeriods*self.cameraPeriod
                                         *(self.numHorizPositions-1)))
                else:
                    self.firstSlowest = "bottom"
                    self.firstReqMoveTime = self.bottomReqMoveTime
                    self.firstExtraModPeriods = 0
                    self.firstPeriod = self.bottomPeriod*self.numHorizPositions
            self.firstRate = 1/self.firstPeriod
            self.firstNumFrames = self.bottomNumFrames*self.numHorizPositions
            if self.debug:
                print("\nFirst:")
                print("mechDiffernece", self.firstMechDifference)
                print("slowest", self.firstSlowest)
                print("first period", self.firstPeriod)
                print("first extra mod periods", self.firstExtraModPeriods)

            # second layer
            # factor in scanning
            #TODO: both axis could have only one position (think CI)
            if self.numVertiPositions == 1:
                self.secondPeriod = self.firstPeriod
                self.secondSlowest = None
                self.secondMechDifference = None
                self.secondExtraModPeriods = None
            else:
                if self.scanningPattern is "raster":
                    # check horizontal reset time and vertical move time vs modulator move time
                    self.secondMechDifference = (max([self.vertiMoveTime, self.horizResetMoveTime])
                                                 - self.firstReqMoveTime)
                    if self.secondMechDifference > 0:
                        if self.vertiMoveTime > self.horizResetMoveTime:
                            self.secondSlowest = "vertical"
                        else:
                            self.secondSlowest = "horizontal reset"
                        # how many modPeriods have to be added
                        self.secondExtraModPeriods = np.ceil(self.secondMechDifference
                                                             /self.cameraPeriod)
                        # There is no vertical move in the beginning
                        self.secondPeriod = (self.firstPeriod*self.numVertiPositions +
                                             self.secondExtraModPeriods*self.cameraPeriod
                                             *(self.numVertiPositions-1))
                    else:
                        self.secondSlowest = "first"
                        self.secondExtraModPeriods = 0
                        self.secondPeriod = self.firstPeriod*self.numVertiPositions
                elif self.scanningPattern is "optimized":
                    # no horizontal move is done after last ramp in first layer
                    # check vertical move time vs modulator move time
                    self.secondMechDifference = self.vertiMoveTime - self.firstReqMoveTime
                    if self.secondMechDifference > 0:
                        self.secondSlowest = "vertical"
                        # how many modPeriods have to be added
                        self.secondExtraModPeriods = np.ceil(self.secondMechDifference
                                                             /self.cameraPeriod)
                        # After the last ramp there is no more vertical move
                        self.secondPeriod = (self.firstPeriod*self.numVertiPositions +
                                             self.secondExtraModPeriods*self.cameraPeriod
                                             *(self.numVertiPositions-1))
                    else:
                        self.secondSlowest = "first"
                        self.secondExtraModPeriods = 0
                        self.secondPeriod = self.firstPeriod*self.numVertiPositions
            self.secondRate = 1/self.secondPeriod
            self.secondNumFrames = self.firstNumFrames*self.numVertiPositions
            if self.debug:
                print("\nSecond:")
                print("mechDiffernece", self.secondMechDifference)
                print("slowest", self.secondSlowest)
                print("Period", self.secondPeriod)
                print("second extra mod periods", self.secondExtraModPeriods)
        else:
            if self.debug:
                print("never got into the layer loop \n")

    def update(self, camTime):
        if self.debug:
            print("\nCamera mode", camTime.cameraMode)
            print("Camera frame time", camTime.frameTime)
            print("Camera ramp time", camTime.rampTime)
            print("Camera ndr", camTime.ndr)
            print("Camera dt", camTime.dt)
            print("Camera prep time", camTime.prepTime)
            print("Camera ready time", camTime.cameraReadyTime)
            print("Camera rend", camTime.rend)
            print("duty cycle", camTime.dutyCycle)

        # calculate stage move times
        self.horizMoveTime = fm1Timing(self.horizStepSize, self.horizSpeed, self.horizTa,
                                       self.horizTd, self.horizTsettle)
        self.horizResetMoveTime = fm1Timing(self.horizStepSize*(self.numHorizPositions-1),
                                            self.horizSpeed, self.horizTa, self.horizTd, self.horizTsettle)
        self.vertiMoveTime = fm1Timing(self.vertiStepSize, self.vertiSpeed, self.vertiTa,
                                       self.vertiTd, self.vertiTsettle)

        if self.debug:
            print("\n horizontal move time", self.horizMoveTime)
            print("horizontal reset move time", self.horizResetMoveTime)
            print("vertical move time", self.vertiMoveTime)
            print("modulation mode", self.modulationMode)

        self.layerCalculation(self.modulationMode, camTime)


if __name__ == "__main__":
    modulationSteps = 8
    modulator = "On" # On/Off
    modulationMode = "Discrete"
    modulatorMoveTime = 0.11
    horizSpeed = 1000/(669.12/30)   #1/((mu/arcsec)/(mu/ms))= arcsec/s 1/0.1
    horizTa = 0.05
    horizTd = 0.05
    horizSettle = 0.06
    vertiSpeed = 1000/(669.12/30)   #1/((mu/arcsec)/(mu/ms))= arcsec/s 1/0.1
    vertiTa = 0.05
    vertiTd = 0.05
    vertiSettle = 0.06
    # modulationMode = "Continuous"
    instrument = "SP"

    spCameraMode = "fast"
    spExposureTime = 0.09040206
    spY=0
    spH=2048
    spCoAdds = 1
    spNumberMeasurements = 1
    spScanningPattern = "optimized"
    spHorizPositions = 21
    spHorizStepSize = 0 # arcsec,
    spVertiPositions = 1
    spVertiStepSize = 0

    #calculat camera timing
    spCamTime = cnCameraTiming(spCameraMode, spExposureTime, spY, spH, modulationMode,
                               modulationSteps=modulationSteps)
                
                #  camTime,
                #  modulator,
                #  modulationMode,
                #  modulationSteps,
                #  modulatorMoveTime,
                #  instrument,
                #  cameraMode,
                #  numMeasurements,
                #  scanningPattern,
                #  horizSpeed,
                #  horizTa,
                #  horizTd,
                #  numHorizPositions,
                #  horizStepSize,
                #  vertiSpeed,
                #  vertiTa,
                #  vertiTd,
                #  numVertiPositions,
                #  vertiStepSize,
                #  secCameraMode=None,
                #  secNumMeasurements=None):
    datTime = cnDatTiming(spCamTime, modulator, modulationMode, modulationSteps,
                          modulatorMoveTime, instrument,
                          spCameraMode, spNumberMeasurements, spScanningPattern,
                          horizSpeed, horizTa, horizTd, horizSettle, spHorizPositions, spHorizStepSize,
                          vertiSpeed, vertiTa, vertiTd, vertiSettle, spVertiPositions, spVertiStepSize, debug=True)

    
