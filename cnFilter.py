#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 08:45:00 2018

@author: andreef
"""
import pickle
import numpy as np
# import matplotlib.pyplot as plt
# import os
# import platform


class cnFilter():
    """
    TBD
    """
    def __init__(self, centerWl, bandwidth, instrument, version, baseDir):
        self.centerWl = centerWl
        self.bandwidth = bandwidth
        self.baseDir = baseDir
        self.instrument = instrument
        self.version = version
        self.notInterpolatedWl = None
        self.notInterpolatedTrans = None
        self.notInterpolatedBlocking = None
        self.isInterpolated = None
        self.wl = None
        self.trans = None
        self.blocking = None
        self.blockingWidth = None
        self.wlShift = None
        self.sets = None

    def interpolate(self, start, end, step):
        """
        TBD
        """
        self.notInterpolatedWl = self.wl
        self.notInterpolatedTrans = self.trans
        self.notInterpolatedBlocking = self.blocking
        self.isInterpolated = True

        tempWl = self.wl
        self.wl = np.arange(start, end+step, step)
        tempTrans = self.trans
        tempBlocking = self.blocking
        self.trans = np.zeros((np.size(self.wl), self.sets))
        self.blocking = np.zeros((np.size(self.wl), self.sets))
        for i in range(0, self.sets):
            self.blocking[:, i] = np.interp(self.wl, tempWl, tempBlocking[:, i])
            self.trans[:, i] = np.interp(self.wl, tempWl, tempTrans[:, i])

    def read(self, blockingWidth):
        """
        TBD
        """
        # blocking width is times the bandwidth
        tempData = np.loadtxt(self.baseDir+self.instrument+"_"+
                              str(self.centerWl)+"_"+self.version+".csv",
                              delimiter=",", skiprows=1)
        self.centerWl = self.centerWl/1000.
        self.bandwidth = self.bandwidth/1000.
        self.blockingWidth = blockingWidth
        #if self.instrument is "SP":
        #firs row contains information on wavelength shift
        self.wlShift = tempData[0, 1]/1000.
        # present wavelength in um to be consistent
        self.wl = np.flipud(tempData[1:, 0]/1000.)
        # apply shift in wavelength
        self.wl = self.wl + self.wlShift
        # use only positive values, some are negative in the blocking
        #normalize outputdata to 1
        self.trans = np.abs(np.flipud(tempData[1:, 1:]))/100.

        # blocking
        self.sets = np.shape(self.trans)[1]

        bb = self.blockingWidth
        tmpWl = self.wl[np.abs(self.wl-self.centerWl) > bb*self.bandwidth]
        tmpTrans = self.trans[np.abs(self.wl-self.centerWl) >
                              bb*self.bandwidth, :]
        self.blocking = np.zeros((np.size(self.wl), self.sets))
        for i in range(0, self.sets):
            self.blocking[:, i] = np.abs(np.interp(self.wl, tmpWl, tmpTrans[:, i]))/100.


    def saveObject(self, destination):
        """ save object with pickle

        """
        with open(destination, 'wb') as output:  # Overwrites any existing file.
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

if __name__ == "__main__":


    filterBaseDir = "/home/andre/Dropbox (DKIST)/python/CryoNIRSP/cnIpc/filter_data/"
    # create all CI filter pickles
    a = cnFilter(1049, 1, "CI", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1074, 1, "CI", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1083, 1, "CI", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1250, 160, "CI", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1281, 1, "CI", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1430, 5, "CI", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    # create all SP filter pickles
    a = cnFilter(854, 12, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1077, 20, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1080, 20, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1252, 27, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1282, 28, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(1430, 35, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(2218, 43, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(3028, 70, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(3934, 124, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')

    a = cnFilter(4651, 171, "SP", "delivered", filterBaseDir)
    a.read(2.5)
    a.saveObject('filter_data/'+str(a.instrument)+'_'+str(int(1000.*a.centerWl))+
                 '_'+str(int(1000.*a.bandwidth))+'_'+str((a.blockingWidth))+
                 '_'+str(a.version)+'.pkl')



