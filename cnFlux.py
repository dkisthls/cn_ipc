#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 11:32:04 2018

@author: andreef
"""

import numpy as np

import scipy.constants as pc
from coatings import  coatingAlDkist, coatingFss99
from materials import caf2RefractiveIndex

import commonFunctions as cf
import names
from solarConstants import solarConstants
sc = solarConstants()


class cnFlux():
    """
    parent cnFlux
    """
    # TODO: implement proper transmission/reflection curves
    def __init__(self, cnSim, verbose=False):
        self.lam = cnSim.lam
        # initiate with CN specifics

        # solid angle of the Sun diameter
        self.sunArcsec = sc.solarDiameterArcsec
        self.sunSterad = sc.solarDiameterSterad

        if verbose is True:
            print("solid angle of 1919 arcsec Sun is: ", self.sunSterad, " [st]\n")
        # print("solid angle seen by "+self.instrument+" pixel is: ",
                # self.pixSterad, " [st]\n")

        # all mechanical dim in mm
        self.primaryMirrorSize = cnSim.telescopeDiameter/1000.

        # coating efficiency
        self.aluminum = coatingAlDkist(self.lam).reflectivity
        self.quantum6 = coatingFss99(self.lam, 6).reflectivity
        self.quantum45 = coatingFss99(self.lam, 45).reflectivity

        #telescope efficiency
        m1 = self.aluminum
        m2 = self.quantum6
        m3 = self.quantum6
        m4 = self.quantum6
        m5 = self.quantum6
        m6 = self.quantum6
        m7 = self.quantum6
        m8 = self.quantum45
        m9 = self.quantum45
        m9a = self.quantum45
        self.telescopeReflectivity = (m1*m2*m3*m4*m5*m6*m7*m8*m9*m9a)
        # feed mirror efficiency
        fm1 = self.quantum6
        fm2 = self.quantum6
        self.feedMirrorReflectivity = fm1*fm2
        # rest of feed optics
        self.warmFilterTransmissivity = np.ones(np.size(self.lam))#*cnSim.warmNd
        self.modulatorTransmissivity = np.ones(np.size(self.lam))*cnSim.modulatorTransmission
        # TODO: implement as measured transmission curve for modulator 
        # both instruments have CaF2 window, simple fresnel at 0 deg AOI
        caf2 = caf2RefractiveIndex(self.lam)
        self.windowTransmission = (((1.-((caf2.n-1.)/
                                         (caf2.n+1.))**2.))**2.)

        # thermal emmission of mirrors and propagation
        # spectral radiance / n^2 is invariant for optical system
        # (radiance = radiant flux / Etendue)
        # Etendue G = area entrance pupil A * solid angle of source seen from pupil Omega
        # primary mirror is entrance pupil and pixel solid angle pix_sterad defines angle
        # so all mirrors can be projected back to the Sun and will have the same spectral
        # radiance

        # all warm optics will have room temperature at 293 K
        # emissivity is 1 - reflectivity

        self.blackbody = cf.blackbody(self.lam/1e6, cnSim.temperatureCoude)
        self.blackbodyCold = cf.blackbody(self.lam/1e6, cnSim.temperatureCryostat)
        bm1 = (1.-m1)*self.blackbody
        bm2 = m2*bm1 + (1.-m2)*self.blackbody
        bm3 = m3*bm2 + (1.-m3)*self.blackbody
        bm4 = m4*bm3 + (1.-m4)*self.blackbody
        bm5 = m5*bm4 + (1.-m5)*self.blackbody
        bm6 = m6*bm5 + (1.-m6)*self.blackbody
        bm7 = m7*bm6 + (1.-m7)*self.blackbody
        bm8 = m8*bm7 + (1.-m8)*self.blackbody
        bm9 = m9*bm8 + (1.-m9)*self.blackbody
        bm9a = m9a*bm9 + (1.-m9a)*self.blackbody
        bfm1 = fm1*bm9a + (1.-fm1)*self.blackbody
        bfm2 = fm2*bfm1 + (1.-fm2)*self.blackbody
        bwf = (self.warmFilterTransmissivity*bfm2
               + (1.-self.warmFilterTransmissivity)*self.blackbody)
        self.bmod = (self.modulatorTransmissivity*bwf
                     + (1.-self.modulatorTransmissivity)*self.blackbody)

        ##############################################
        # convert to photons
        ############################################

        # energy per photon at wavelenth
        # planck constant [J s]
        h = pc.h
        # light speed [m s^-1]
        c = pc.c
        self.energyPerPhoton = (h * c) / (self.lam/1e6) # [J]

        #  H2RG QE (electrons per photon)
        self.detectorQe = np.ones(np.size(self.lam))*0.7

class cnFluxCi(cnFlux):
    """
    TBD
    """
    def __init__(self, cnSim, flux, verbose=False):

        self.instrument = "CI"
        cnFlux.__init__(self, cnSim, verbose=verbose)
        # W m^2 m-1 st-1 convertion of 1 flux unit, i.e output
        # is going to be scale factor
        self.inputFlux = flux
        self.update(cnSim, self.inputFlux)

    def update(self, cnSim, flux):
        """
        TBD
        """
        self.inputFlux = flux
        self.pixArcsec = cnSim.spatialSamplingPixelCi
        #solid angle seen by pixel in [st]
        self.pixSterad = cf.arcsec2sterad(self.pixArcsec, self.pixArcsec)
        # print('ci pixsterad old vs new', self.pixSterad, test)

        self.filterTransmission = cnSim.ciFilter.trans[:, 0]
        self.blockingTransmission = cnSim.ciFilter.blocking[:, 0]

        # center wavelength of filters only used for legacy measurements of
        # wiregrid and grating interpolation
        self.lamFilter = np.array([0.53, 0.637, 0.656, 0.789, 1.08, 1.252, 1.43, 2.218,
                                   2.326, 2.58, 3.028, 3.935, 4.651, 5.])
        # Rodney, W. S. and Spinder, R. J., Index of refraction of fused
        # quartz glass for ultraviolet, visible and infrared wavelengths,
        # J. Res. Nat. Bur. Stand. 53, 185 (1954).
        # only reflective losses good to 2.333 um for 1mm thick ohara sk-1300
        # manually adjusted to meet ohara sk-1300 data sheet transmission
        wiregridTransmissivity = np.array([0.93109993, 0.93202998, 0.93215443,
                                           0.93283476, 0.93379867, 0.93426625,
                                           0.93474408, 0.93725542, 0.937676, 0.92, 0.9,
                                           0.875, 0.36, 0.])/2.
        self.wiregridTransmissivity = np.interp(self.lam, self.lamFilter,
                                                wiregridTransmissivity)
        # feed optics
        self.pickoffState = cnSim.pickoffState
        if self.pickoffState == names.PICKOFF_OPEN:
            cm1 = np.zeros(np.size(self.lam))
        elif self.pickoffState == names.PICKOFF_SPLITTER:
            cm1 = np.ones(np.size(self.lam))*cnSim.pellicleReflectivity
        elif self.pickoffState == names.PICKOFF_MIRROR:
            cm1 = self.quantum45
        self.pickoffEfficiency = cm1
        self.feedEfficiency = (self.feedMirrorReflectivity*
                               self.warmFilterTransmissivity*
                               self.modulatorTransmissivity*
                               self.pickoffEfficiency)
        # print('min max feed efficiency', np.min(self.feedEfficiency), np.max(self.feedEfficiency))
        # background flux after feed
        bcm1 = cm1*self.bmod + (1.-cm1)*self.blackbody

        # analyzer in CI
        if cnSim.ciPolarimetry:
            # print('adding analyzer')
            self.analyzerTransmission = self.wiregridTransmissivity
        else:
            self.analyzerTransmission = np.ones_like(self.lam)

        self.ciEfficiency = (self.windowTransmission*self.filterTransmission*
                             self.analyzerTransmission)
        self.ciBlockingEfficiency = (self.windowTransmission*
                                     self.blockingTransmission*
                                     self.analyzerTransmission)

        # combined efficiency
        self.totalEfficiency = (self.telescopeReflectivity*self.feedEfficiency*
                                self.ciEfficiency)
        cnSim.ciTransmission = self.totalEfficiency
        cnSim.ciTransmissionCw = np.interp(cnSim.ciCw,
                                           self.lam,
                                           self.totalEfficiency)
        self.totalBlockingEfficiency = (self.telescopeReflectivity*
                                        self.feedEfficiency*
                                        self.ciBlockingEfficiency)

        # signal photons in [#photons m^-2 m^-1 st^-1 s^-1]
        self.inputPhotons = self.inputFlux / self.energyPerPhoton

        # signal photon flux per ci pixel [#photons pixel^-1 s^-1 m-1]
        # not considering effciency
        detectorPhotonsPerPixel = (self.inputPhotons*(self.primaryMirrorSize/2.)**2.
                                   *np.pi*self.pixSterad*self.totalEfficiency)

        detectorPhotonsPerPixelBlocking = (self.inputPhotons*
                                           (self.primaryMirrorSize/2.)**2.
                                           *np.pi*self.pixSterad*
                                           self.totalBlockingEfficiency)

        # integrate to get total signal
        self.detectorPhotonsPerPixel = np.trapz(detectorPhotonsPerPixel,
                                                self.lam/1e6)

        self.detectorPhotonsPerPixelBlocking = np.trapz(detectorPhotonsPerPixelBlocking,
                                                        self.lam/1e6)
        self.detectorPhotonsPerPixelSignal = (self.detectorPhotonsPerPixel-
                                              self.detectorPhotonsPerPixelBlocking)

        cnSim.detectorPhotonsPerPixelCi = self.detectorPhotonsPerPixel
        cnSim.detectorPhotonsPerPixelBlockingCi = self.detectorPhotonsPerPixelBlocking
        cnSim.detectorPhotonsPerPixelSignalCi = self.detectorPhotonsPerPixelSignal

        self.detectorElectronsPerPixel = np.trapz(detectorPhotonsPerPixel*
                                                  self.detectorQe,
                                                  self.lam/1e6)
        self.detectorElectronsPerPixelBlocking = np.trapz(detectorPhotonsPerPixelBlocking*
                                                          self.detectorQe,
                                                          self.lam/1e6)
        self.detectorElectronsPerPixelSignal = (self.detectorElectronsPerPixel-
                                                self.detectorElectronsPerPixelBlocking)

        cnSim.detectorElectronsPerPixelCi = self.detectorElectronsPerPixel
        cnSim.detectorElectronsPerPixelBlockingCi = self.detectorElectronsPerPixelBlocking
        cnSim.detectorElectronsPerPixelSignalCi = self.detectorElectronsPerPixelSignal



        #    self.windowSterad = (9. / 50.)**2.
        #    self.filterSterad = (1. / 3.)**2.
        #    self.environmentSterad = self.windowSterad #np.pi/8.
        #    print('env, window st', self.environmentSterad, self.windowSterad)
        self.thermalPhotonsCi = ((bcm1*self.windowTransmission*self.filterTransmission*
                                  self.analyzerTransmission*
                                  self.pixSterad*(self.primaryMirrorSize/2.)**2 +
                                  self.blackbody*self.windowTransmission*self.analyzerTransmission*
                                  self.filterTransmission*cnSim.environmentSterad*
                                  (cnSim.pixelSize/1e6)**2. +
                                  self.blackbody*(1.-self.aluminum)*self.filterTransmission*
                                  self.analyzerTransmission*
                                  cnSim.windowSterad*(cnSim.pixelSize/1e6)**2. +
                                  self.blackbodyCold*(1.-self.aluminum)*
                                  cnSim.filterSterad*(cnSim.pixelSize/1e6)**2.)/
                                 self.energyPerPhoton)
        # print('telescope optics',np.trapz(((bcm1*self.windowTransmission*self.filterTransmission*
        #                     self.pixSterad*(self.primaryMirrorSize/2.)**2)/
        #                     self.energyPerPhoton),self.lam/1e6)   )
        # print('environment',np.trapz(((self.blackbody*self.windowTransmission*
        #                     self.filterTransmission*cnSim.environmentSterad*
        #                     (cnSim.pixelSize/1e6)**2.)/
        #                     self.energyPerPhoton),self.lam/1e6)   )
        # print('window emmiss',np.trapz(((self.blackbody*(1.-self.aluminum)*
        #                               self.filterTransmission*
        #                     cnSim.windowSterad*(cnSim.pixelSize/1e6)**2.)/
        #                     self.energyPerPhoton),self.lam/1e6)   )
        # print('filter emmiss',np.trapz(((self.blackbodyCold*(1.-self.aluminum)*
        #                     cnSim.filterSterad*(cnSim.pixelSize/1e6)**2.)/
        #                     self.energyPerPhoton),self.lam/1e6)   )
        self.thermalPhotonsSignalCi = np.trapz(self.thermalPhotonsCi, self.lam/1e6)
        self.thermalElectronsCi = self.thermalPhotonsCi*self.detectorQe
        self.thermalElectronsSignalCi = np.trapz(self.thermalElectronsCi, self.lam/1e6)
        cnSim.thermalPhotonsSignalCi = self.thermalPhotonsSignalCi
        cnSim.thermalElectronsSignalCi = self.thermalElectronsSignalCi

class cnFluxCiMax(cnFlux):
    """
    TBD
    """
    def __init__(self, cnSim, flux, verbose=False):

        self.instrument = "CI"
        cnFlux.__init__(self, cnSim, verbose=verbose)
        # W m^2 m-1 st-1 convertion of 1 flux unit, i.e output
        # is going to be scale factor
        self.inputFlux = flux
        self.update(cnSim, self.inputFlux)

    def update(self, cnSim, flux):
        """
        TBD
        """
        self.inputFlux = flux
        self.pixArcsec = cnSim.spatialSamplingPixelCi
        #solid angle seen by pixel in [st]
        self.pixSterad = cf.arcsec2sterad(self.pixArcsec, self.pixArcsec)

        self.filterTransmission = cnSim.ciFilter.trans[:, 0]
        self.blockingTransmission = cnSim.ciFilter.blocking[:, 0]

        # center wavelength of filters only used for legacy measurements of
        # wiregrid and grating interpolation
        self.lamFilter = np.array([0.53, 0.637, 0.656, 0.789, 1.08, 1.252, 1.43, 2.218,
                                   2.326, 2.58, 3.028, 3.935, 4.651, 5.])
        # Rodney, W. S. and Spinder, R. J., Index of refraction of fused
        # quartz glass for ultraviolet, visible and infrared wavelengths,
        # J. Res. Nat. Bur. Stand. 53, 185 (1954).
        # only reflective losses good to 2.333 um for 1mm thick ohara sk-1300
        # manually adjusted to meet ohara sk-1300 data sheet transmission
        wiregridTransmissivity = np.array([0.93109993, 0.93202998, 0.93215443,
                                           0.93283476, 0.93379867, 0.93426625,
                                           0.93474408, 0.93725542, 0.937676, 0.92, 0.9,
                                           0.875, 0.36, 0.])/2.
        self.wiregridTransmissivity = np.interp(self.lam, self.lamFilter,
                                                wiregridTransmissivity)

        # feed optics
        self.pickoffState = cnSim.pickoffState
        if self.pickoffState == names.PICKOFF_OPEN:
            cm1 = np.zeros(np.size(self.lam))
        elif self.pickoffState == names.PICKOFF_SPLITTER:
            cm1 = np.ones(np.size(self.lam))*cnSim.pellicleReflectivity
        elif self.pickoffState == names.PICKOFF_MIRROR:
            cm1 = self.quantum45
        self.pickoffEfficiency = cm1
        self.feedEfficiency = (self.feedMirrorReflectivity*
                               self.warmFilterTransmissivity*
                               self.modulatorTransmissivity*
                               self.pickoffEfficiency)
        # print('min max feed efficiency', np.min(self.feedEfficiency), np.max(self.feedEfficiency))
        # background flux after feed
        bcm1 = cm1*self.bmod + (1.-cm1)*self.blackbody

        # analyzer in CI
        if cnSim.ciPolarimetry:
            self.analyzerTransmission = self.wiregridTransmissivity
        else:
            self.analyzerTransmission = np.ones_like(self.lam)

        self.ciEfficiency = (self.windowTransmission*self.filterTransmission*
                             self.analyzerTransmission)
        self.ciBlockingEfficiency = (self.windowTransmission*
                                     self.blockingTransmission*
                                     self.analyzerTransmission)
        # combined efficiency
        self.totalEfficiency = (self.telescopeReflectivity*self.feedEfficiency*
                                self.ciEfficiency)
        self.totalBlockingEfficiency = (self.telescopeReflectivity*
                                        self.feedEfficiency*
                                        self.ciBlockingEfficiency)

        # signal photons in [#photons m^-2 m^-1 st^-1 s^-1]
        self.inputPhotons = self.inputFlux / self.energyPerPhoton

        # signal photon flux per ci pixel [#photons pixel^-1 s^-1 m-1]
        # not considering effciency
        detectorPhotonsPerPixel = (self.inputPhotons*(self.primaryMirrorSize/2.)**2.
                                   *np.pi*self.pixSterad*self.totalEfficiency)

        detectorPhotonsPerPixelBlocking = (self.inputPhotons*
                                           (self.primaryMirrorSize/2.)**2.
                                           *np.pi*self.pixSterad*
                                           self.totalBlockingEfficiency)

        # integrate to get total signal
        self.detectorPhotonsPerPixel = np.trapz(detectorPhotonsPerPixel,
                                                self.lam/1e6)
        self.detectorPhotonsPerPixelBlocking = np.trapz(detectorPhotonsPerPixelBlocking,
                                                        self.lam/1e6)
        self.detectorPhotonsPerPixelSignal = (self.detectorPhotonsPerPixel-
                                              self.detectorPhotonsPerPixelBlocking)

        cnSim.detectorPhotonsPerPixelCiMax = self.detectorPhotonsPerPixel
        cnSim.detectorPhotonsPerPixelBlockingCiMax = self.detectorPhotonsPerPixelBlocking
        cnSim.detectorPhotonsPerPixelSignalCiMax = self.detectorPhotonsPerPixelSignal

        self.detectorElectronsPerPixel = np.trapz(detectorPhotonsPerPixel*
                                                  self.detectorQe,
                                                  self.lam/1e6)
        self.detectorElectronsPerPixelBlocking = np.trapz(detectorPhotonsPerPixelBlocking*
                                                          self.detectorQe,
                                                          self.lam/1e6)
        self.detectorElectronsPerPixelSignal = (self.detectorElectronsPerPixel-
                                                self.detectorElectronsPerPixelBlocking)

        cnSim.detectorElectronsPerPixelCiMax = self.detectorElectronsPerPixel
        cnSim.detectorElectronsPerPixelBlockingCiMax = self.detectorElectronsPerPixelBlocking
        cnSim.detectorElectronsPerPixelSignalCiMax = self.detectorElectronsPerPixelSignal



        #    self.windowSterad = (9. / 50.)**2.
        #    self.filterSterad = (1. / 3.)**2.
        #    self.environmentSterad = self.windowSterad #np.pi/8.
        #    print('env, window st', self.environmentSterad, self.windowSterad)
        self.thermalPhotonsCi = ((bcm1*self.windowTransmission*self.filterTransmission*
                                  self.pixSterad*(self.primaryMirrorSize/2.)**2+
                                  self.blackbody*self.windowTransmission*
                                  self.filterTransmission*cnSim.environmentSterad*
                                  (cnSim.pixelSize/1e6)**2. +
                                  self.blackbody*(1.-self.aluminum)*self.filterTransmission*
                                  cnSim.windowSterad*(cnSim.pixelSize/1e6)**2. +
                                  self.blackbodyCold*(1.-self.aluminum)*
                                  cnSim.filterSterad*(cnSim.pixelSize/1e6)**2.)/
                                 self.energyPerPhoton)
        # print('telescope optics',np.trapz(((bcm1*self.windowTransmission*self.filterTransmission*
        #                     self.pixSterad*(self.primaryMirrorSize/2.)**2)/
        #                     self.energyPerPhoton),self.lam/1e6)   )
        # print('environment',np.trapz(((self.blackbody*self.windowTransmission*
        #                     self.filterTransmission*cnSim.environmentSterad*
        #                     (cnSim.pixelSize/1e6)**2.)/
        #                     self.energyPerPhoton),self.lam/1e6)   )
        # print('window emmiss',np.trapz(((self.blackbody*(1.-self.aluminum)*
        # self.filterTransmission*
        #                     cnSim.windowSterad*(cnSim.pixelSize/1e6)**2.)/
        #                     self.energyPerPhoton),self.lam/1e6)   )
        # print('filter emmiss',np.trapz(((self.blackbodyCold*(1.-self.aluminum)*
        #                     cnSim.filterSterad*(cnSim.pixelSize/1e6)**2.)/
        #                     self.energyPerPhoton),self.lam/1e6)   )
        self.thermalPhotonsSignalCi = np.trapz(self.thermalPhotonsCi, self.lam/1e6)
        self.thermalElectronsCi = self.thermalPhotonsCi*self.detectorQe
        self.thermalElectronsSignalCi = np.trapz(self.thermalElectronsCi, self.lam/1e6)
        cnSim.thermalPhotonsSignalCiMax = self.thermalPhotonsSignalCi
        cnSim.thermalElectronsSignalCiMax = self.thermalElectronsSignalCi

class cnFluxSp(cnFlux):
    """
    TBD
    """
    def __init__(self, cnSim, verbose=False):
        self.instrument = "SP"
        cnFlux.__init__(self, cnSim, verbose=verbose)
        self.update(cnSim)

    def update(self, cnSim):
        """
        TBD
        """
        # W m^2 m-1 st-1 convertion of 1 flux unit, i.e output
        # is going to be scale factor
        self.inputFlux = np.ones(np.size(self.lam))
        self.pixArcsec = cnSim.spatialSamplingPixelSp
        self.slitArcsec = cnSim.spatialSamplingSlit
        # print('sp cnsim spatial sampling', self.pixArcsec, self.slitArcsec)
        # solid angle seen by pixel in [st]
        # test = cf.oldarcsec2sterad(self.pixArcsec)
        self.pixSterad = cf.arcsec2sterad(self.pixArcsec, self.slitArcsec)
        # print('pixsterad new vs old', self.pixSterad, test)
        #    print('pixel Sterad', self.pixSterad)

        self.filterTransmission = cnSim.spFilter.trans[:, 0]
        self.blockingTransmission = cnSim.spFilter.blocking[:, 0]

        self.pixelBandwidth = cnSim.pixelBandwidth/1e6 # all wl given in um
        self.instrProf = cnSim.instrumentProfileFwhm/cnSim.pixelSize
        # print('instr profile units?',self.instrProf)
        self.lamCentral = cnSim.spCw/1e6 # all wl given in um convert to m

        # center wavelength of filters only used for legacy measurements of
        # wiregrid and grating interpolation
        self.lamFilter = np.array([0.53, 0.637, 0.656, 0.789, 1.08, 1.252, 1.43, 2.218,
                                   2.326, 2.58, 3.028, 3.935, 4.651, 5.])

        # feed optics
        self.pickoffState = cnSim.pickoffState
        if self.pickoffState == names.PICKOFF_OPEN:
            cm1 = np.ones(np.size(self.lam))
        elif self.pickoffState == names.PICKOFF_SPLITTER:
            cm1 = np.ones(np.size(self.lam))*cnSim.pellicleTransmission
        elif self.pickoffState == names.PICKOFF_MIRROR:
            cm1 = np.zeros(np.size(self.lam))
        self.pickoffEfficiency = cm1
        self.feedEfficiency = (self.feedMirrorReflectivity*
                               self.warmFilterTransmissivity*
                               self.modulatorTransmissivity*
                               self.pickoffEfficiency)
        # background flux after feed and window
        bcm1 = cm1*self.bmod + (1.-cm1)*self.blackbody
        bwin = (self.windowTransmission *bcm1 +
                (1.-self.windowTransmission)*self.blackbody)
        #measured values for grating overall
        # gratingReflectivity = np.array([0.542, 0.56, 0.563, 0.585, 0.62, 0.62, 0.62,
        #                                 0.62, 0.62, 0.62, 0.62, 0.62, 0.62, 0.62])/2.
        # self.gratingEfficiency = np.interp(self.lam, self.lamFilter,
        #                                    gratingReflectivity)
        # use calculated grating efficiency
        self.gratingEfficiency = np.ones(np.size(self.lam))*cnSim.gratingEfficiency*self.aluminum
        # measured reflectivity values
        wiregridReflectivity = np.array([0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.5, 0.68, 0.67,
                                         0.65, 0.55, 0.6, 0.56, 0.56])
        wiregridReflectivity = np.ones(np.size(self.lamFilter))*0.99
        self.wiregridReflectivity = np.interp(self.lam, self.lamFilter,
                                              wiregridReflectivity)
        # Rodney, W. S. and Spinder, R. J., Index of refraction of fused
        # quartz glass for ultraviolet, visible and infrared wavelengths,
        # J. Res. Nat. Bur. Stand. 53, 185 (1954).
        # only reflective losses good to 2.333 um for 1mm thick ohara sk-1300
        # manually adjusted to meet ohara sk-1300 data sheet transmission
        wiregridTransmissivity = np.array([0.93109993, 0.93202998, 0.93215443,
                                           0.93283476, 0.93379867, 0.93426625,
                                           0.93474408, 0.93725542, 0.937676, 0.92, 0.9,
                                           0.875, 0.36, 0.])/2.
        self.wiregridTransmissivity = np.interp(self.lam, self.lamFilter,
                                                wiregridTransmissivity)
        sm2 = self.quantum6
        sm3 = self.quantum6
        sm4 = self.quantum6
        self.sm5 = self.quantum6
        self.spMirrorReflectivity = sm2*sm3*sm4*self.sm5
        sb11 = self.quantum45
        sb12 = self.quantum45
        sb21 = self.quantum45
        prism = self.quantum45
        self.analyzerMirrorP = sb21*prism
        self.analyzerMirrorS = sb11*sb12*prism
        # path for p polarization is refl wg1 -> trans wg2 -> refl SB1 ->refl prism
        self.analyzerPEfficiency = (self.wiregridReflectivity*
                                    self.wiregridTransmissivity*
                                    self.analyzerMirrorP)
        # path for s polarization is trans wg1 -> refl SB2 -> refl SB3 ->refl prism
        self.analyzerSEfficiency = (self.wiregridTransmissivity*
                                    self.analyzerMirrorS)
        # thermal flux through cold sp
        # what is exact emmissivity of interference filters
        bfil = self.filterTransmission*bwin +(1.- self.quantum6)*self.blackbodyCold
        bsm2 = sm2*bfil + (1.-sm2)*self.blackbodyCold
        bsm3 = sm3*bsm2 + (1.-sm3)*self.blackbodyCold
        self.bsm4 = sm4*bsm3 + (1.-sm4)*self.blackbodyCold
        # now sm4 flux is split diffracted by grating, i.e bsm4 flux treated like
        # normal flux and add another component after grating

        bgra = (1.-self.aluminum)*self.blackbodyCold
        bsm5 = self.sm5*bgra + (1.-self.sm5)*self.blackbodyCold
        bwg1 = (self.wiregridTransmissivity*bsm5 + (1.-self.aluminum)*
                self.blackbodyCold)
        bsb11 = sb11*bwg1 + (1.-sb11)*self.blackbodyCold
        bsb12 = sb12*bsb11 + (1.-sb12)*self.blackbodyCold
        self.bpri1 = prism*bsb12 + (1.-prism)*self.blackbodyCold

        bwg2 = (self.wiregridTransmissivity*bwg1 + (1.-self.aluminum)*
                self.blackbodyCold)
        bsb21 = sb21*bwg2 + (1.-sb21)*self.blackbodyCold
        self.bpri2 = prism*bsb21 + (1.-prism)*self.blackbodyCold

        self.spPEfficiency = (self.windowTransmission*self.filterTransmission*
                              self.spMirrorReflectivity*self.gratingEfficiency*
                              self.analyzerPEfficiency)

        self.spSEfficiency = (self.windowTransmission*self.filterTransmission*
                              self.spMirrorReflectivity*self.gratingEfficiency*
                              self.analyzerSEfficiency)

        self.spPBlockingEfficiency = (self.windowTransmission*
                                      self.blockingTransmission*
                                      self.spMirrorReflectivity*
                                      self.gratingEfficiency*
                                      self.analyzerPEfficiency)

        self.spSBlockingEfficiency = (self.windowTransmission*
                                      self.blockingTransmission*
                                      self.spMirrorReflectivity*
                                      self.gratingEfficiency*
                                      self.analyzerSEfficiency)

        # combined efficiencies
        self.totalPEfficiency = (self.telescopeReflectivity*self.feedEfficiency*
                                 self.spPEfficiency)
        self.totalSEfficiency = (self.telescopeReflectivity*self.feedEfficiency*
                                 self.spSEfficiency)
        cnSim.spTransmissionP = self.totalPEfficiency
        cnSim.spTransmissionS = self.totalSEfficiency
        cnSim.spTransmissionPCw = np.interp(self.lamCentral*1e6,
                                            self.lam,
                                            self.totalPEfficiency)
        cnSim.spTransmissionSCw = np.interp(self.lamCentral*1e6,
                                            self.lam,
                                            self.totalSEfficiency)

        self.totalPBlockingEfficiency = (self.telescopeReflectivity*
                                         self.feedEfficiency*
                                         self.spPBlockingEfficiency)
        self.totalSBlockingEfficiency = (self.telescopeReflectivity*
                                         self.feedEfficiency*
                                         self.spSBlockingEfficiency)
        # print('total throughput telescope',np.interp(self.lamCentral,
        # self.lam/1e6,self.totalPEfficiency),
        # np.interp(self.lamCentral,self.lam/1e6,self.totalSEfficiency) )
        # print("spectrograph efficiency", np.interp(self.lamCentral, self.lam/1e6,
                                                #    self.totalPEfficiency))
        # print("qe", np.interp(self.lamCentral, self.lam/1e6, self.detectorQe))

        # signal photons in [#photons m^-2 m^-1 st^-1 s^-1]
        self.inputPhotons = self.inputFlux / self.energyPerPhoton
        # print('photon energy SI',self.lamCentral,
        # np.interp(self.lamCentral,self.lam/1e6,self.energyPerPhoton))

        # signal photon flux per spectrograph pixel [#photons pixel^-1 s^-1 m-1]
        # not considering effciency this is total across slit and thus divided by the amount
        # of pixels over which slit profile spreads this is ideal for emmission lines
        # for continuum we have to multiply with instrument profile to take into account
        # contribution from neigboring wavelength intervals
        # detectorPhotonsPerPixel = (self.inputPhotons*(self.primaryMirrorSize/2.)**2.
        #                           *np.pi*self.pixSterad)
        detectorPhotonsPerPixel = (self.inputPhotons*(self.primaryMirrorSize/2.)**2.
                                   *np.pi*self.pixSterad)/self.instrProf
        # print('detect photons per pixel old', detectorPhotonsPerPixel)
        # print('detect photons per pixel new', (self.inputPhotons*(self.primaryMirrorSize/2.)**2.
        #                                        *np.pi*test)/self.instrProf)
        # print('detector photons',(self.primaryMirrorSize/2.)**2.*np.pi, self.pixSterad,
        # np.interp(self.lamCentral,self.lam/1e6,detectorPhotonsPerPixel))
        # print('photon flux', np.interp(self.lamCentral, self.lam/1e6, detectorPhotonsPerPixel))
        # print('pixelbandwidth', self.pixelBandwidth)
        # signal photon flux per pixel on spectrograph detector !!!! this is only
        # valid for the central pixel with spCw
        # print('spectral pixel bandwidth', self.pixelBandwidth, '\n')
        self.detectorPhotonsPerPixelP = (detectorPhotonsPerPixel*
                                         self.totalPEfficiency*
                                         self.pixelBandwidth)
        self.detectorElectronsPerPixelP = (self.detectorPhotonsPerPixelP*
                                           self.detectorQe)
        #    self.detectorPhotonsPerPixelPBlocking = (detectorPhotonsPerPixel*
        #                                     self.totalPBlockingEfficiency*
        #                                     self.pixelBandwidth)
        self.detectorPhotonsPerPixelS = (detectorPhotonsPerPixel*
                                         self.totalSEfficiency*
                                         self.pixelBandwidth)
        self.detectorElectronsPerPixelS = (self.detectorPhotonsPerPixelS*
                                           self.detectorQe)

        #    self.detectorPhotonsPerPixelSBlocking = (detectorPhotonsPerPixel*
        #                                     self.totalSBlockingEfficiency*
        #                                     self.pixelBandwidth)
        self.detectorPhotonsPerPixelPCentral = np.interp(self.lamCentral,
                                                         self.lam/1e6,
                                                         self.detectorPhotonsPerPixelP)
        self.detectorPhotonsPerPixelSCentral = np.interp(self.lamCentral,
                                                         self.lam/1e6,
                                                         self.detectorPhotonsPerPixelS)
        #    cnSim.detectorPhotonsPerPixelPCentral = self.detectorPhotonsPerPixelPCentral
        #    cnSim.detectorPhotonsPerPixelSCentral = self.detectorPhotonsPerPixelSCentral

        self.detectorElectronsPerPixelPCentral = np.interp(self.lamCentral,
                                                           self.lam/1e6,
                                                           self.detectorElectronsPerPixelP)
        self.detectorElectronsPerPixelSCentral = np.interp(self.lamCentral,
                                                           self.lam/1e6,
                                                           self.detectorElectronsPerPixelS)
        cnSim.detectorElectronsPerPixelPCentral = self.detectorElectronsPerPixelPCentral
        cnSim.detectorElectronsPerPixelSCentral = self.detectorElectronsPerPixelSCentral
        #    self.detectorPhotonsPerPixelPCentralBlocking=np.interp(self.lamCentral,
        #                                                  self.lam,
        #                                                self.detectorPhotonsPerPixelPBlocking)
        #
        # thermal flux needs special treatment
        self.thermalSpFluxS = ((self.bpri1*self.pixSterad)+(self.bsm4*self.gratingEfficiency*
                                                            self.sm5*self.analyzerSEfficiency
                                                            *self.pixelBandwidth))
        self.thermalSpFluxP = ((self.bpri2*self.pixSterad)+(self.bsm4*self.gratingEfficiency*
                                                            self.sm5*self.analyzerPEfficiency
                                                            *self.pixelBandwidth))
        # self.thermalPhotonsS = (self.thermalSpFluxS*(self.primaryMirrorSize/2.)**2.
        #                         *np.pi*self.pixSterad)/self.energyPerPhoton
        # self.thermalPhotonsP = (self.thermalSpFluxP*(self.primaryMirrorSize/2.)**2.
        #                         *np.pi*self.pixSterad)/self.energyPerPhoton
        self.thermalPhotonsS = (self.thermalSpFluxS*(self.primaryMirrorSize/2.)**2.
                                *np.pi*self.pixSterad/self.instrProf)/self.energyPerPhoton
        self.thermalPhotonsP = (self.thermalSpFluxP*(self.primaryMirrorSize/2.)**2.
                                *np.pi*self.pixSterad/self.instrProf)/self.energyPerPhoton
        self.thermalElectronsS = (self.thermalPhotonsS*self.detectorQe)
        self.thermalElectronsP = (self.thermalPhotonsP*self.detectorQe)
        # thermal flux is in SI, lamCentral is in [m] already
        self.thermalElectronsSCentral = np.interp(self.lamCentral,
                                                  self.lam/1e6,
                                                  self.thermalElectronsS)
        self.thermalElectronsPCentral = np.interp(self.lamCentral,
                                                  self.lam/1e6,
                                                  self.thermalElectronsP)
        cnSim.thermalElectronsSCentral = self.thermalElectronsSCentral
        cnSim.thermalElectronsPCentral = self.thermalElectronsPCentral

    def update_cw_only(self, cw, bw, cnSim):
        """
        TBD
        """
        # at this point all wavelengths given in um
        cnSim.spTransmissionPCw = np.interp(cw,
                                            self.lam,
                                            self.totalPEfficiency)
        cnSim.spTransmissionSCw = np.interp(cw,
                                            self.lam,
                                            self.totalSEfficiency)

        # return flux scale at a certain wavelength with a certain pixel bandwidth bw
        bw = bw/1e6 # all wl given in um
        cw = cw/1e6 # all wl given in um convert to m

        # signal photon flux per spectrograph pixel [#photons pixel^-1 s^-1 m-1]
        # not considering effciency
        # detectorPhotonsPerPixel = (self.inputPhotons*(self.primaryMirrorSize/2.)**2.
        #                            *np.pi*self.pixSterad)
        detectorPhotonsPerPixel = (self.inputPhotons*(self.primaryMirrorSize/2.)**2.
                                   *np.pi*self.pixSterad)/self.instrProf
        # signal photon flux per pixel on spectrograph detector !!!! this is only
        # valid for the central pixel with spCw
        detectorPhotonsPerPixelP = (detectorPhotonsPerPixel*self.totalPEfficiency*
                                    bw)
        detectorElectronsPerPixelP = (detectorPhotonsPerPixelP*self.detectorQe)

        detectorPhotonsPerPixelS = (detectorPhotonsPerPixel*self.totalSEfficiency*
                                    bw)
        detectorElectronsPerPixelS = (detectorPhotonsPerPixelS*self.detectorQe)


        detectorElectronsPerPixelPCentral = np.interp(cw, self.lam/1e6,
                                                      detectorElectronsPerPixelP)
        detectorElectronsPerPixelSCentral = np.interp(cw, self.lam/1e6,
                                                      detectorElectronsPerPixelS)

        # thermal flux needs special treatment
        thermalSpFluxS = ((self.bpri1*self.pixSterad)+(self.bsm4*self.gratingEfficiency*
                                                       self.sm5*self.analyzerSEfficiency*bw))
        thermalSpFluxP = ((self.bpri2*self.pixSterad)+(self.bsm4*self.gratingEfficiency*
                                                       self.sm5*self.analyzerPEfficiency*bw))
        # thermalPhotonsS = (thermalSpFluxS*(self.primaryMirrorSize/2.)**2.
        #                         *np.pi*self.pixSterad)/self.energyPerPhoton
        # thermalPhotonsP = (thermalSpFluxP*(self.primaryMirrorSize/2.)**2.
        #                         *np.pi*self.pixSterad)/self.energyPerPhoton
        thermalPhotonsS = (thermalSpFluxS*(self.primaryMirrorSize/2.)**2.
                           *np.pi*self.pixSterad/self.instrProf)/self.energyPerPhoton
        thermalPhotonsP = (thermalSpFluxP*(self.primaryMirrorSize/2.)**2.
                           *np.pi*self.pixSterad/self.instrProf)/self.energyPerPhoton
        thermalElectronsS = (thermalPhotonsS*self.detectorQe)
        thermalElectronsP = (thermalPhotonsP*self.detectorQe)
        thermalElectronsSCentral = np.interp(cw, self.lam/1e6, thermalElectronsS)
        thermalElectronsPCentral = np.interp(cw, self.lam/1e6, thermalElectronsP)
        return (detectorElectronsPerPixelSCentral, detectorElectronsPerPixelPCentral,
                thermalElectronsSCentral, thermalElectronsPCentral)
