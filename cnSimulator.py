# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 22:23:12 2018

@author: Andre

TODO:
    - add polarization information for CI
    - Done provide information on how much of the noise is photon noise
    - Done add expert mode for camera
    - Done allow for import of more accurate models for coronal brightnesses
    - check/make compatible output with FORWard
    - Done check thermal background on CI again
    - Done sensitivity estimates check Matt Penns method
    - selection of input images
    - Done add binning capabilities
    - Done (only easy for line) background/sky scaling should be at center wavelength

"""

import os
import sys
import pickle
import numpy as np
from PyQt5 import  QtWidgets, QtGui
from scipy.interpolate import interp1d
import commonFunctions as cf
from cnSpectrograph import cnSpectrograph
import filters
from cnFlux import cnFluxCi, cnFluxCiMax, cnFluxSp
import names

from solarRadialDependence import solarLimbDarkening


BASEDIR = os.path.dirname(os.path.realpath(sys.argv[0]))

class cnSimulator():
    """
    cnSimulator class
    """
    def __init__(self, fluxModel, fastFrameTimeSp, fastFrameTimeCi):
        self.debug = 0
        if self.debug >= 1:
            print("cnSim initialisation")
        self.lam = fluxModel.lam
        self.bgAtLam = 1.1  # scale BG at this wavelength
        self.radiusCorona = fluxModel.radiusCorona
        self.radiusDisk = fluxModel.radiusDisk
        self.lamIncrement = self.lam[1]-self.lam[0]
        self.radiusIncrement = self.radiusCorona[1]-self.radiusCorona[0]
        self.fastFrameTimeSp = fastFrameTimeSp
        self.fastFrameTimeCi = fastFrameTimeCi
        self.spMaxPixelExposure = 0
        self.ciMaxPixelExposure = 0
        self.testRun = False
        self.plot = False #"synthetic"
        # Things that can be changed from the IPC interface
        #    self.spFilterIndex = "HeI,Fe XIII,Fe"
        self.spFilterIndex = "HeI,Fe XIII 1077-20"
        self.ciFilterIndex = "Fe XIII 1074.7-1"
        self.spMinRadius = 1.
        self.spMaxRadius = 1.2
        self.ciMinRadius = 1.
        self.ciMaxRadius = 1.2
        self.meanRadius = 1.1
        # 0 means no scaling done
        self.bgScaleValue = 0   #rel value of sky brightness from GUI
        self.initialBgScalePass = 0
        self.skyScale = 1   # calculated scale factor
        self.otherBgScale = 1
        self.spCw = 1.080
        self.ciCw = 1.083
        self.warmNd = 1.      # scale factor for neutral density
        self.pickoffState = names.PICKOFF_SPLITTER
        self.ciPolarimetry = False

        self.spIntTime = 0.1
        self.ciIntTime = 0.1
        self.spCoAdds = 1
        self.ciCoAdds = 1
        self.spRepeats = 1
        self.ciRepeats = 1
        self.spBinX = 1
        self.spBinY = 1
        self.ciBinX = 1
        self.ciBinY = 1
        self.nrModStates = 8
        # changes with filter
        self.lowerFilterBoundaryWlSp = 1.065 # lower boundary in um
        self.upperFilterBoundaryWlSp = 1.1  # upper boundary in um
        self.lowerFilterBoundaryWlCi = 1.07
        self.upperFilterBoundaryWlCi = 1.08
        self.slitWidth = 175.
        if self.slitWidth == 52.:
            self.slitHeight = 42.
        elif self.slitWidth == 175.:
            self.slitHeight = 81.
        else:
            raise ValueError("Provide a proper slith width")
        # make selectable in next release
        self.delZannaRegion = "QS"
        self.gratingOrder = None
        self.diskFluxSelector = "Blackbody"
        # other setup parameters hard coded for now
        # 1A resolution spectra for all full spectra
        # self.lamIncrement = 0.0001
        # self.lam = np.arange(0.5,5.,self.lamIncrement)
        self.modulatorTransmission = 0.8
        self.pellicleTransmission = 0.9
        self.pellicleReflectivity = 0.04
        self.temperatureCoude = 293.
        self.temperatureCryostat = 77.
        # descoped CI solid angles
        self.environmentSterad = (40. / 503.)**2.*np.pi#(9. / 50.)**2.#np.pi/4.
        self.windowSterad = (40. / 503.)**2.*np.pi#(9. / 50.)**2.
        self.filterSterad = (30. / 35.)**2.*np.pi#(1. / 3.)**2.
        # self.radiusIncrement = 0.01
        # self.radiusCorona = np.arange(1.,1.6,self.radiusIncrement)
        # self.radiusDisk = np.arange(0.,1.,0.05)
        # DelZanna wavelengths only needed to make relative line widths
        self.zannaWl = np.array([1.0747, 1.0798, 1.4301, 2.2183, 2.5846, 3.0285, 3.9343])
        self.landeZanna = np.array([1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5])
        self.lineWidthZanna = 2e-4*self.zannaWl
        self.heWl = np.array([1.083])
        self.lineWidthHe = np.array([3e-4])
        self.landeHe = np.array([1.25])
        self.lineScaleIndex = 0
        # intialisation only but 0 results in no scaling
        self.lineScaleValue = 0.
        self.gratingEfficiency = [None]
        # self.desiredRelativeLineBrightness = 40. # this is peak spectral not integrated
        #line withdts in x=km/s
        # y = x*lineWidth/3e5
        self.n = 31.6
        self.theta = -5.5
        self.gratingBlaze = 63.9
        self.cameraFocalLength = 932.
        self.collimatorFocalLength = 2096.
        self.pixelSize = 18.
        self.nrSpectralPixel = 1024
        self.telescopeFnumber = 18.
        self.telescopeDiameter = 4000.
        self.gratingWidth = 408.
        self.gratingHeight = 153.
        # initialize attributes that will be overwrittent by cnSpectrograph class
        self.linearDispersion = None
        self.instrumentProfile = None
        self.instrumentProfileFwhm = None
        self.alpha = None
        self.beta = None
        self.instrumentProfileResolution = None
        self.blazeOrder = None
        self.lamMinOnArray = None
        self.lamMaxOnArray = None
        self.spatialSamplingPixelSp = None
        self.spatialSamplingSlit = None

        # initialize attributes that will be written by cnFlux class
        self.detectorElectronsPerPixelSCentral = None
        self.thermalElectronsSCentral = None
        self.detectorElectronsPerPixelCi = [None]
        self.detectorElectronsPerPixelCiMax = [None]
        self.detectorElectronsPerPixelSignalCiMax = [None]
        self.detectorElectronsPerPixelBlockingCiMax = [None]
        self.thermalElectronsSignalCi = None
        self.thermalElectronsSignalCiMax = None
        self.detectorElectronsPerPixelSignalCi = [None]
        self.detectorElectronsPerPixelBlockingCi = [None]
        self.spTransmissionP = None
        self.spTransmissionS = None
        self.spTransmissionPCw = None
        self.spTransmissionSCw = None
        self.ciTransmission = None
        self.ciTransmissionCw = None

        # values needed for first pass thru/initialisatiom make them unrealistic
        self.detectorEperAdu = 0.0005
        self.detectorReadNoiseSp = 50000.  # electrons per pixel
        self.detectorReadNoiseCi = 50000.
        self.detectorFlatFieldNoise = 1.  #percent per pixel
        # dark currents needed for first pass through make them unrealistic
        self.spdetectorDarkCurrent = 1000000. # e- per second
        self.cidetectorDarkCurrent = 1000000. # e- per second
        self.detectorWellSp = 2e9 # e-
        self.detectorWellCi = 2e9 # e-
        self.detectorWellPercentage = 1.
        self.detectorWellCheckSp = True
        self.detectorWellCheckCi = True
        # Thermal blackbody for thermal signal of seen by instrument
        self.thermalBb = cf.blackbody(self.lam/1e6, self.temperatureCoude)
        # limb darkening
        try:
            self.load_object("radialData/limbDarkening.pkl", "limbDarkening", "radialDependence")
        except AttributeError:
            self.diskRadialScale = solarLimbDarkening(self.radiusDisk)
            self.diskRadialScale.saveObject("radialData/limbDarkening.pkl")
        self.update(fluxModel)

        #!!!! flux model calculations/updates are now done in the ipcDriver
        #####################################################################
        # initialisation of simulator done
        #####################################################################

    def get_source_with_path(self, folder, file):
        """
        return complete path of file
        """
        return os.path.join(BASEDIR, folder, file)

    def update_params(self, spFilterIndex, ciFilterIndex, spMinRadius, spMaxRadius,
                      ciMinRadius, ciMaxRadius,
                      meanRadius, bgScale, spCw, ciCw, warmNd, slitWidth, spIntTime,
                      ciIntTime, spCoAdds, ciCoAdds, spRepeats, ciRepeats,
                      autoSatCheckSP, autoSatCheckCI,
                      spBinX, spBinY, ciBinX, ciBinY,
                      spReadNoise, spWell, ciReadNoise, ciWell, numModulationStates,
                      fluxModel, lineScaleIndex, lineScaleValue,
                      detectorWellPercentage, spMaxPixelTime, ciMaxPixelTime,
                      pickoffState, ciPolarimetry):
        """
        Update attributes of cnSimulator if changed in ipcDriver
        """

        # Things that can be changed from the IPC interface
        self.spFilterIndex = spFilterIndex
        self.ciFilterIndex = ciFilterIndex
        self.spMinRadius = spMinRadius
        self.spMaxRadius = spMaxRadius
        self.ciMinRadius = ciMinRadius
        self.ciMaxRadius = ciMaxRadius
        self.meanRadius = meanRadius
        self.lineScaleIndex = lineScaleIndex
        self.lineScaleValue = lineScaleValue
        self.bgScaleValue = bgScale #relative sky brightness
        self.spCw = spCw/1000.   # center wavelength
        self.ciCw = ciCw/1000.
        self.ciPolarimetry = ciPolarimetry

        self.detectorWellCheckSp = autoSatCheckSP
        self.detectorWellCheckCi = autoSatCheckCI

        if warmNd == "2ND":
            self.warmNd = 1/100
        elif warmNd == "3ND":
            self.warmNd = 1/1000
        elif warmNd == "4ND":
            self.warmNd = 1/10000
        elif warmNd == "5ND":
            self.warmNd = 1/100000
        elif warmNd == "Open":
            self.warmNd = 1
        else:
            raise ValueError("Provide a proper warmND %s"%(warmNd))
        # if simul:
        self.pickoffState = pickoffState
        self.spIntTime = spIntTime/1000.
        self.ciIntTime = ciIntTime/1000.
        if self.debug >= 3:
            print("conversion to s", self.spIntTime)

        self.detectorEperAdu = 4.
        self.detectorFlatFieldNoise = 0.#0.01  #percent per pixel
        self.spdetectorDarkCurrent = 0.1 # e- per second
        self.cidetectorDarkCurrent = 0.1 # e- per second
        self.detectorWellPercentage = detectorWellPercentage
        self.detectorReadNoiseSp = spReadNoise
        self.detectorWellSp = spWell
        self.detectorReadNoiseCi = ciReadNoise
        self.detectorWellCi = ciWell

        self.spCoAdds = spCoAdds
        self.ciCoAdds = ciCoAdds
        self.spRepeats = spRepeats
        self.ciRepeats = ciRepeats
        self.nrModStates = numModulationStates
        self.spBinX = spBinX
        self.spBinY = spBinY
        self.ciBinX = ciBinX
        self.ciBinY = ciBinY
        self.spMaxPixelExposure = spMaxPixelTime
        self.ciMaxPixelExposure = ciMaxPixelTime

        # changes with filter
        self.lowerFilterBoundaryWlSp = filters.getFromDict(
            filters.filterDictSP, [spFilterIndex, "min"])/1000
        self.upperFilterBoundaryWlSp = filters.getFromDict(
            filters.filterDictSP, [spFilterIndex, "max"])/1000
        self.lowerFilterBoundaryWlCi = filters.getFromDict(
            filters.filterDictCI, [ciFilterIndex, "min"])/1000
        self.upperFilterBoundaryWlCi = filters.getFromDict(
            filters.filterDictCI, [ciFilterIndex, "max"])/1000
        self.slitWidth = slitWidth
        if self.slitWidth == 52.:
            self.slitHeight = 42.
        elif self.slitWidth == 175.:
            self.slitHeight = 81.
        else:
            raise ValueError("Provide a proper slith width")
        self.update(fluxModel)

        # floating point precision problem
        # round Integration times to ns and to 0.1 resolution
        # print('before return', ciIntTime, self.ciIntTime*1e9, np.round(self.ciIntTime*1e9))
        return  np.around(np.round(self.spIntTime*1e9)*1e-6, 1),\
                np.around(np.round(self.ciIntTime*1e9)*1e-6, 1), self.spCoAdds,\
                         self.ciCoAdds, self.spSaturationLevel, self.ciSaturationLevel,\
                         self.detectorWellCheckSp, self.detectorWellCheckCi

    def update(self, fluxModel):
        """
        methode to update cnSimulator
        """

        # from here things need to go into update

        # decide if coronal or disk case and set other radius to 1
        # also treat min and max radius
        if self.meanRadius >= 1.:
            self.meanRadiusCorona = self.meanRadius
            self.meanRadiusDisk = 1.
            self.minRadiusCoronaSp = max(self.spMinRadius, 1.)
            self.maxRadiusCoronaSp = self.spMaxRadius
            self.minRadiusDiskSp = 1.
            self.maxRadiusDiskSp = 1.
            self.minRadiusCoronaCi = max(self.ciMinRadius, 1.)
            self.maxRadiusCoronaCi = self.ciMaxRadius
            self.minRadiusDiskCi = 1.
            self.maxRadiusDiskCi = 1.
        elif self.meanRadius < 1.:
            self.meanRadiusCorona = 1.
            self.meanRadiusDisk = self.meanRadius
            self.minRadiusDiskSp = self.spMinRadius
            self.maxRadiusDiskSp = min(self.spMaxRadius, 1.)
            self.minRadiusCoronaSp = 1.
            self.maxRadiusCoronaSp = 1.
            self.minRadiusDiskCi = self.ciMinRadius
            self.maxRadiusDiskCi = min(self.ciMaxRadius, 1.)
            self.minRadiusCoronaCi = 1.
            self.maxRadiusCoronaCi = 1.
        if self.debug >= 2:
            print("sp radii", self.spMinRadius, self.spMaxRadius)
            print("ci radii", self.ciMinRadius, self.ciMaxRadius)
            print("sp corona radii", self.minRadiusCoronaSp, self.maxRadiusCoronaSp)
            print("sp disk radii", self.minRadiusDiskSp, self.maxRadiusDiskSp)
            print("ci corona radii", self.minRadiusCoronaCi, self.maxRadiusCoronaCi)
            print("ci disk radii", self.minRadiusDiskCi, self.maxRadiusDiskCi)

        #######################################################################
        # sky and line scaling
        #######################################################################
        # scale background first
        # index for which lam value is closest to bgAtLam in um
        ind = np.where(np.abs(self.lam-self.bgAtLam) < self.lamIncrement/10)[0]
        # alter sky scaling to match desired background specified in GUI
        # sky is relative to value at R=1.1, wl=bgAtLam in um
        # First pass will have None bgScaleValue and we need to set SB value
        modelValue = fluxModel.backgroundModel.totalBgRelativeBrightness
        # this should equal to 1 if no change has been made
        # question is how much to scale sky to get desired relative background
        backgroundScale = self.bgScaleValue/modelValue
        self.skyScale = (1. + (fluxModel.backgroundModel.totalBgRelativeBrightness
                               *(backgroundScale-1.))
                         /fluxModel.backgroundModel.skyRelativeBrightness)
        # for very low values of total BG the skyScale will go negative
        if self.skyScale < 0:
            self.skyScale = 0
            # no the rest of of the components have to be scale
            self.otherBgScale = ((self.bgScaleValue / (fluxModel.backgroundModel.\
                                                           totalBgRelativeBrightness -
                                                       fluxModel.backgroundModel.\
                                                           skyRelativeBrightness)))
        else:
            self.otherBgScale = 1

        if self.debug >= 2:
            print("bgScaleValue, otherScale", self.bgScaleValue, self.otherBgScale)
            print(" skyScale", self.skyScale)

        # line can be scaled (0 value means no scaling)
        # at current radius independent of background
        temp = (1e6*np.interp(fluxModel.coronalLinesWl, self.lam,
                              fluxModel.coronalLineModel.fluxInterpolationFunction(
                                  self.meanRadiusCorona))/
                fluxModel.coronalLineModel.diskCenterAtCoronalWlFlux)
        if self.lineScaleValue > 0:
            self.lineScale = self.lineScaleValue/temp[self.lineScaleIndex]
        else:
            self.lineScale = 1.

        ###########################################################################
        #interpolate all fluxes to mean radius
        ###########################################################################
        # corona
        # TODO: maximum flux is not handled across the limb!
        # maximum flux is at minimum radius
        self.interpolatedScaledLineFluxesMaxSp = (self.lineScale
                                                  *self.warmNd
                                                  *fluxModel.coronalLineModel.\
                                                      fluxInterpolationFunction(\
                                                          self.minRadiusCoronaSp))
        self.interpolatedScaledLineFluxesMaxCi = (self.lineScale
                                                  *self.warmNd
                                                  *fluxModel.coronalLineModel.\
                                                      fluxInterpolationFunction(\
                                                          self.minRadiusCoronaCi))

        self.interpolatedScaledLineFluxes = (self.lineScale
                                             *self.warmNd
                                             *fluxModel.coronalLineModel\
                                                 .fluxInterpolationFunction(self.meanRadiusCorona))

        # for the active line calculations a matrix for all R is required not just the meanRadius
        self.scaledLineFluxes = (self.lineScale*self.warmNd
                                 *fluxModel.coronalLineModel\
                                     .fluxInterpolationFunction(self.radiusCorona))

        self.scaledLineBrightnesses = np.zeros((len(self.radiusCorona),
                                                fluxModel.coronalLinesCount))
        for i in range(len(self.radiusCorona)):
            self.scaledLineBrightnesses[i, :] = np.interp(fluxModel.coronalLinesWl,
                                                          self.lam, self.scaledLineFluxes[:, i])
        self.scaledRelativeLineBrightnesses = (1e6*self.scaledLineBrightnesses
                                               /fluxModel.coronalLineModel\
                                                   .diskCenterAtCoronalWlFlux)

        # disk
        # limb darkening for disk this is a single scale factor for current r
        # also get flux at minimum of field for saturation purposes
        self.diskRadialScale.interpolate(self.minRadiusDiskSp, self.lam)
        self.diskRadialScaleFactorMaxSp = np.squeeze(self.diskRadialScale.radialScaleFactor)
        self.interpolatedScaledDiskFluxMaxSp = (self.warmNd
                                                *self.diskRadialScaleFactorMaxSp
                                                *fluxModel.backgroundModel.diskCenter.flux)
        self.diskRadialScale.interpolate(self.minRadiusDiskCi, self.lam)
        self.diskRadialScaleFactorMaxCi = np.squeeze(self.diskRadialScale.radialScaleFactor)
        self.interpolatedScaledDiskFluxMaxCi = (self.warmNd
                                                *self.diskRadialScaleFactorMaxCi
                                                *fluxModel.backgroundModel.diskCenter.flux)
        # now for center of field
        self.diskRadialScale.interpolate(self.meanRadiusDisk, self.lam)
        self.diskRadialScaleFactor = np.squeeze(self.diskRadialScale.radialScaleFactor)

        self.interpolatedScaledDiskFlux = (self.warmNd
                                           *self.diskRadialScaleFactor
                                           *fluxModel.backgroundModel.diskCenter.flux)

        # thermal
        self.scaledThermalFlux = self.thermalBb*self.warmNd
        self.scaledThermalFluxAtCoronalWl = np.interp(fluxModel.coronalLinesWl,
                                                      self.lam, self.scaledThermalFlux)

        # Background
        # go through all components and also produce sum
        self.interpolatedScaledBgFluxes = []
        self.interpolatedScaledSummedBgFlux = np.zeros(self.lam.shape)
        self.scaledSummedBgFlux = np.zeros((len(self.lam), len(self.radiusCorona)))
        for i in range(fluxModel.backgroundModel.bgCount):
            if fluxModel.backgroundModel.bgNames[i] == "sky":
                temp = (self.warmNd
                        *self.skyScale
                        *(fluxModel.backgroundModel\
                            .fluxInterpolationFunction[i](self.meanRadiusCorona)))
                temp2 = (self.warmNd
                         *self.skyScale
                         *(fluxModel.backgroundModel\
                             .fluxInterpolationFunction[i](self.radiusCorona)))
                self.interpolatedScaledBgFluxes.append(temp)
            else:
                temp = (self.warmNd
                        *self.otherBgScale
                        *(fluxModel.backgroundModel\
                            .fluxInterpolationFunction[i](self.meanRadiusCorona)))
                temp2 = (self.warmNd
                         *self.otherBgScale
                         *(fluxModel.backgroundModel\
                             .fluxInterpolationFunction[i](self.radiusCorona)))
                self.interpolatedScaledBgFluxes.append(temp)
            self.interpolatedScaledSummedBgFlux = temp + self.interpolatedScaledSummedBgFlux

            self.scaledSummedBgFlux = self.scaledSummedBgFlux + temp2

        # bg flux at r min
        interf = interp1d(self.radiusCorona, self.scaledSummedBgFlux)
        self.scaledSummedBgFluxAtRMinSp = interf(self.minRadiusCoronaSp)
        self.scaledSummedBgFluxAtRMinCi = interf(self.minRadiusCoronaCi)

        self.scaledSummedBgFluxAtCoronalWl = np.zeros((len(self.radiusCorona),
                                                       fluxModel.coronalLinesCount))
        for i in range(len(self.radiusCorona)):
            self.scaledSummedBgFluxAtCoronalWl[i, :] = np.interp(fluxModel.coronalLinesWl,
                                                                 self.lam,
                                                                 self.scaledSummedBgFlux[:, i])
        # current background at wl=bgAtLam um
        self.currentBgValue = (self.interpolatedScaledSummedBgFlux[ind]
                               /(1e-6*fluxModel.backgroundModel.diskCenter.flux[ind]))
        self.currentBgValueAtCw = np.interp([self.spCw, self.ciCw],
                                            self.lam,
                                            (self.interpolatedScaledSummedBgFlux
                                             /(1e-6*fluxModel.backgroundModel.diskCenter.flux)))
        # combine for corona
        self.interpolatedCombinedFlux = (self.interpolatedScaledLineFluxes
                                         + self.interpolatedScaledSummedBgFlux)
        self.interpolatedCombinedFluxMaxSp = (self.interpolatedScaledLineFluxesMaxSp
                                              + self.scaledSummedBgFluxAtRMinSp)
        self.interpolatedCombinedFluxMaxCi = (self.interpolatedScaledLineFluxesMaxCi
                                              + self.scaledSummedBgFluxAtRMinCi)

        # scale is needed for plots
        self.plotScale = 1e6/(fluxModel.backgroundModel.diskCenter.flux)

        ##############################################################################
        # Next is the spectrograph as it serves as input for filter and flux calc
        ##############################################################################
        try:
            self.cnSp.update(self)
        except AttributeError:
            self.cnSp = cnSpectrograph(self)
        # call self.cnSp.update(self) for updated
        if self.debug >= 1:
            print("updating spectrograph done")
        # uncomment for filter passband estimates
        # print(self.spCw, self.cnSp.lamMinOnArray, self.cnSp.lamMaxOnArray, self.cnSp.blazeOrder)
        # free, maxundis, passband = self.cnSp.cn_sp_overlaps(self.spCw,
        #                                                     self.cnSp.lamMinOnArray[0],
        #                                                     self.cnSp.lamMaxOnArray[0],
        #                                                     self.cnSp.blazeOrder[0])
        # print("free, maxundisturbed, passband",free, maxundis, passband)

        ###########################################################################
        # loading filter profiles
        ###########################################################################

        # SP first
        atlas = filters.filterDictSP[self.spFilterIndex]['atlas']
        filterData = filters.filterDictSP[self.spFilterIndex]['filterData']
        removeOutliers = filters.filterDictSP[self.spFilterIndex]['removeOutliers']
        self.load_object(self.get_source_with_path("atlas", atlas), "spAtlas", "atlas")
        if removeOutliers is not None:
            self.spAtlas.removeOutliers(removeOutliers[0], removeOutliers[1])
        self.load_object(self.get_source_with_path("filter_data", filterData), "spFilter", "filter")
        # CI
        atlas = filters.filterDictCI[self.ciFilterIndex]['atlas']
        filterData = filters.filterDictCI[self.ciFilterIndex]['filterData']
        removeOutliers = filters.filterDictSP[self.spFilterIndex]['removeOutliers']
        self.load_object(self.get_source_with_path("atlas", atlas), "ciAtlas", "atlas")
        if removeOutliers is not None:
            self.ciAtlas.removeOutliers(removeOutliers[0], removeOutliers[1])
        self.load_object(self.get_source_with_path("filter_data", filterData), "ciFilter", "filter")


        ###########################################################################
        # interpolate atlases and filters
        ###########################################################################
        #linear dispersion has to be in um/um because instrument profile is in um
        # print(self.spAtlas.wl)
        self.spAtlas.interpolate(self.linearDispersion/1000.)

        self.spAtlas.convolve(self.instrumentProfile)
        self.spAtlas.interpolate(self.pixelSize*self.linearDispersion/1000.)

        self.spFilter.interpolate(self.lam[0], self.lam[-1], self.lamIncrement)
        self.ciFilter.interpolate(self.lam[0], self.lam[-1], self.lamIncrement)

        ###########################################################################
        # flux calculations
        ###########################################################################
        if self.debug >= 2:
            print("before cnFluxSP call")
        # SP will return scale factor for detector e-/SI flux and e- for thermal
        # but this is for a monochromatic source and for spectrally extended sources
        # we need to multiply with instrument profile as this will add the contribution
        # from neighboring wavelength intervals
        self.cnFluxSp = cnFluxSp(self)
        #    print("after cnFluxSP call")
        # print('flux output (no update?)',self.detectorElectronsPerPixelSCentral)
        # CI needs flux as input for integration will returen e- per pixel per s
        # this is for the current mean r
        self.cnFluxCi = cnFluxCi(self,
                                 [self.interpolatedScaledDiskFlux,
                                  self.interpolatedCombinedFlux,
                                  self.scaledThermalFlux,
                                  self.interpolatedScaledSummedBgFlux])
        # this is for the minimum radius, i.e. highest flux
        self.cnFluxCiMax = cnFluxCiMax(self,
                                       [self.interpolatedScaledDiskFluxMaxCi,
                                        self.interpolatedCombinedFluxMaxCi,
                                        self.scaledThermalFlux,
                                        self.scaledSummedBgFluxAtRMinCi])

        ###########################################################################
        # calculate counts (ADU) per "pixel"
        ###########################################################################
        #!!!: integration time is in s
        #!!!: spectrograph thermal result is in electrons whereas for detectorElectrons
        #     is scale factor e-/1SI unit of Flux
        ###########################################################################
        # noise and dark electrons ###################################################
        self.spDarkElectrons = (self.spdetectorDarkCurrent*self.spIntTime
                                *self.spCoAdds*self.spBinX*self.spBinY)
        self.ciDarkElectrons = (self.cidetectorDarkCurrent*self.ciIntTime*self.ciCoAdds
                                *self.ciBinX*self.ciBinY)
        # read noise is reduced if several pixels are used or co-adding is performed
        self.spReadNoiseElectrons = (self.detectorReadNoiseSp
                                     *np.sqrt(self.spCoAdds*self.spBinX*self.spBinY))
        self.ciReadNoiseElectrons = (self.detectorReadNoiseCi
                                     *np.sqrt(self.ciCoAdds*self.ciBinX*self.ciBinY))
        ###########################################################################
        # Disk SP electrons ##########################################################
        # output from cnFluxSp is scale factor for flux
        self.spDiskElectronsPerPixel = ((self.detectorElectronsPerPixelSCentral*
                                         self.cnFluxSp.instrProf
                                         *np.interp(self.spCw, self.lam,
                                                    self.interpolatedScaledDiskFlux))
                                        *(self.spIntTime*self.spCoAdds
                                          *self.spBinX*self.spBinY))
        self.spDiskElectronsPerPixelMax = ((self.detectorElectronsPerPixelSCentral*
                                            self.cnFluxSp.instrProf
                                            *np.interp(self.spCw, self.lam,
                                                       self.interpolatedScaledDiskFluxMaxSp))
                                           *(self.spIntTime*self.spCoAdds
                                             *self.spBinX*self.spBinY))
        # output from cnFluxSp is scale factor for thermal
        self.spDiskThermalElectronsPerPixel = ((self.thermalElectronsSCentral*
                                                self.cnFluxSp.instrProf)
                                               *(self.spIntTime*self.spCoAdds
                                                 *self.spBinX*self.spBinY))

        self.spDiskTotalElectrons = (self.spDiskElectronsPerPixel
                                     + self.spDarkElectrons
                                     + self.spDiskThermalElectronsPerPixel)
        self.spDiskTotalElectronsMax = (self.spDiskElectronsPerPixelMax
                                        + self.spDarkElectrons
                                        + self.spDiskThermalElectronsPerPixel)

        ###########################################################################
        # Disk SP S/N #############################################################
        # S/N done in e- per pixel
        # for single beam, single modulation state
        self.spDiskSn = (self.spDiskElectronsPerPixel
                         /(np.sqrt(self.spDiskTotalElectrons + self.spReadNoiseElectrons)))
        # dual beam and multiple modulation states
        self.spDiskSnAllStatesI = np.sqrt(self.nrModStates*2)*self.spDiskSn
        self.spDiskSnAllStatesQUV = self.spDiskSnAllStatesI / np.sqrt(3)
        ###########################################################################
        # Disk CI counts ##########################################################
        # [0] component is for disk signal only
        # [1] component is for the combined flux for corona
        # [2] component is for thermal flux for both
        # [3] component is for the summed coronal background
        self.ciDiskElectronsPerPixel = (self.detectorElectronsPerPixelCi[0]
                                        *(self.ciIntTime*self.ciCoAdds
                                          *self.ciBinX*self.ciBinY))
        self.ciDiskElectronsPerPixelMax = (self.detectorElectronsPerPixelCiMax[0]
                                           *(self.ciIntTime*self.ciCoAdds
                                             *self.ciBinX*self.ciBinY))
        self.ciDiskThermalElectronsPerPixel = ((self.thermalElectronsSignalCi)
                                               *(self.ciIntTime*self.ciCoAdds
                                                 *self.ciBinX*self.ciBinY))
        self.ciDiskTotalElectrons = (self.ciDiskElectronsPerPixel
                                     + self.ciDarkElectrons
                                     + self.ciDiskThermalElectronsPerPixel)
        self.ciDiskTotalElectronsMax = (self.ciDiskElectronsPerPixelMax
                                        + self.ciDarkElectrons
                                        + self.ciDiskThermalElectronsPerPixel)
        ###########################################################################
        # Disk CI S/N #############################################################
        # S/N done in e- per pixel
        # for single exposure
        self.ciDiskSn = (self.ciDiskElectronsPerPixel
                         /(np.sqrt(self.ciDiskTotalElectrons + self.ciReadNoiseElectrons)))
        # multiple modulation states
        self.ciDiskSnAllStatesI = np.sqrt(self.nrModStates)*self.ciDiskSn
        self.ciDiskSnAllStatesQUV = self.ciDiskSnAllStatesI / np.sqrt(3)
        ###########################################################################
        # Corona SP counts ########################################################
        # output from cnFluxSp is scale factor for flux
        self.spCoronaElectronsPerPixel = ((self.detectorElectronsPerPixelSCentral*
                                           self.cnFluxSp.instrProf
                                           *np.interp(self.spCw, self.lam,
                                                      self.interpolatedScaledLineFluxes))
                                          *(self.spIntTime*self.spCoAdds*self.spBinX*self.spBinY))

        self.spCoronaElectronsPerPixelMax = ((self.detectorElectronsPerPixelSCentral*
                                              self.cnFluxSp.instrProf
                                              *np.interp(self.spCw, self.lam,
                                                         self.interpolatedScaledLineFluxesMaxSp))
                                             *(self.spIntTime*self.spCoAdds*self.spBinX
                                               *self.spBinY))

        self.spCoronaBackgroundElectronsPerPixel = ((self.detectorElectronsPerPixelSCentral*
                                                     self.cnFluxSp.instrProf
                                                     *np.interp(self.spCw, self.lam,
                                                                self.interpolatedScaledSummedBgFlux)
                                                    )
                                                    *(self.spIntTime*self.spCoAdds
                                                      *self.spBinX*self.spBinY))

        self.spCoronaBackgroundElectronsPerPixelMax = ((self.detectorElectronsPerPixelSCentral*
                                                        self.cnFluxSp.instrProf
                                                        *np.interp(self.spCw, self.lam,
                                                                   self.scaledSummedBgFluxAtRMinSp))
                                                       *(self.spIntTime*self.spCoAdds
                                                         *self.spBinX*self.spBinY))

        self.spCoronaThermalElectronsPerPixel = ((self.thermalElectronsSCentral*
                                                  self.cnFluxSp.instrProf)
                                                 *(self.spIntTime*self.spCoAdds
                                                   *self.spBinX*self.spBinY))

        self.spCoronaTotalElectrons = (self.spCoronaElectronsPerPixel
                                       + self.spCoronaBackgroundElectronsPerPixel
                                       + self.spCoronaThermalElectronsPerPixel
                                       + self.spDarkElectrons)

        self.spCoronaTotalElectronsMax = (self.spCoronaElectronsPerPixelMax
                                          + self.spCoronaBackgroundElectronsPerPixelMax
                                          + self.spCoronaThermalElectronsPerPixel
                                          + self.spDarkElectrons)

        ###########################################################################
        # Corona SP S/N ########################################################
        # S/N done in e- per pixel
        # single beam and single modulation state
        self.spCoronaSn = (self.spCoronaElectronsPerPixel
                           /(np.sqrt(self.spCoronaTotalElectrons + self.spReadNoiseElectrons)))
        # dual beam and multiple modulation states
        self.spCoronaSnAllStatesI = np.sqrt(self.nrModStates*2)*self.spCoronaSn
        self.spCoronaSnAllStatesQUV = self.spCoronaSnAllStatesI / np.sqrt(3)

        ###########################################################################
        # Corona CI counts ########################################################
        # [0] component is for disk signal only
        # [1] component is for the combined flux for corona
        # [2] component is for thermal flux for both
        # [3] component is for the summed coronal background
        self.ciCoronaElectronsPerPixelAll = ((self.detectorElectronsPerPixelSignalCi[1])
                                             *(self.ciIntTime*self.ciCoAdds
                                               *self.ciBinX*self.ciBinY))
        self.ciCoronaElectronsPerPixelAllMax = ((self.detectorElectronsPerPixelSignalCiMax[1])
                                                *(self.ciIntTime*self.ciCoAdds
                                                  *self.ciBinX*self.ciBinY))

        self.ciCoronaBackgroundElectronsPerPixel = ((self.detectorElectronsPerPixelCi[3])
                                                    *(self.ciIntTime*self.ciCoAdds
                                                      *self.ciBinX*self.ciBinY))
        self.ciCoronaBackgroundElectronsPerPixelMax = ((self.detectorElectronsPerPixelCiMax[3])
                                                       *(self.ciIntTime*self.ciCoAdds
                                                         *self.ciBinX*self.ciBinY))
        # ci has only component for total and background but not signal alone
        self.ciCoronaElectronsPerPixel = (self.ciCoronaElectronsPerPixelAll
                                          - self.ciCoronaBackgroundElectronsPerPixel)
        if self.ciCoronaElectronsPerPixel < 0:
            self.ciCoronaElectronsPerPixel = 0
        self.ciCoronaElectronsPerPixelMax = (self.ciCoronaElectronsPerPixelAllMax
                                             - self.ciCoronaBackgroundElectronsPerPixelMax)
        if self.ciCoronaElectronsPerPixelMax < 0:
            self.ciCoronaElectronsPerPixelMax = 0
        self.ciCoronaBlockingElectronsPerPixel = ((self.detectorElectronsPerPixelBlockingCi[1])
                                                  *(self.ciIntTime*self.ciCoAdds
                                                    *self.ciBinX*self.ciBinY))
        self.ciCoronaBlockingElectronsPerPixelMax = (self.detectorElectronsPerPixelBlockingCiMax[1]
                                                     *(self.ciIntTime*self.ciCoAdds
                                                       *self.ciBinX*self.ciBinY))

        self.ciCoronaThermalElectronsPerPixel = ((self.thermalElectronsSignalCi)
                                                 *(self.ciIntTime*self.ciCoAdds
                                                   *self.ciBinX*self.ciBinY))
        self.ciCoronaThermalElectronsPerPixelMax = ((self.thermalElectronsSignalCiMax)
                                                    *(self.ciIntTime*self.ciCoAdds
                                                      *self.ciBinX*self.ciBinY))

        self.ciCoronaTotalElectrons = (self.ciCoronaElectronsPerPixel
                                       + self.ciCoronaBackgroundElectronsPerPixel
                                       + self.ciCoronaBlockingElectronsPerPixel
                                       + self.ciCoronaThermalElectronsPerPixel
                                       + self.ciDarkElectrons)
        self.ciCoronaTotalElectronsMax = (self.ciCoronaElectronsPerPixelMax
                                          + self.ciCoronaBackgroundElectronsPerPixelMax
                                          + self.ciCoronaBlockingElectronsPerPixelMax
                                          + self.ciCoronaThermalElectronsPerPixelMax
                                          + self.ciDarkElectrons)
        ##########################################################################
        # Corona CI S/N ########################################################
        # single exposure
        self.ciCoronaSn = (self.ciCoronaElectronsPerPixel
                           /(np.sqrt(self.ciCoronaTotalElectrons + self.ciReadNoiseElectrons)))
        # multiple modulation states
        self.ciCoronaSnAllStatesI = np.sqrt(self.nrModStates)*self.ciCoronaSn
        self.ciCoronaSnAllStatesQUV = self.ciCoronaSnAllStatesI / np.sqrt(3)
        if self.debug >= 1:
            print("per pixel calculation done")

    ###########################################################################
    ## calculations for the active lines within the filter passband
    ###########################################################################
        # calculate count values for active emmission line
        # the active line brightness is for peak of line

        # find fluxModel lines that are within passband
        if self.debug >= 2:
            print("sp filter boundaries", self.lowerFilterBoundaryWlSp,
                      self.upperFilterBoundaryWlSp)
            print("sp coronal lines", fluxModel.coronalLinesWl)
        self.activeIndex = np.where((fluxModel.coronalLinesWl >= self.lowerFilterBoundaryWlSp) &
                                    (fluxModel.coronalLinesWl <= self.upperFilterBoundaryWlSp))[0]
        if self.activeIndex.size != 0:
            self.activeLineNames = fluxModel.coronalLinesNames[self.activeIndex]
            self.activeWl = fluxModel.coronalLinesWl[self.activeIndex]
            self.activeLineWidth = fluxModel.coronalLinesWidth[self.activeIndex]
            self.activeLande = fluxModel.coronalLinesLande[self.activeIndex]
            self.activeLineBrightness = self.scaledLineBrightnesses[:, self.activeIndex]
            self.activeRelativeLineBrightness = self.scaledRelativeLineBrightnesses[:,\
                self.activeIndex]
        else:
            self.activeLineNames = None
            self.activeLineBrightness = None
            self.activeRelativeLineBrightness = None
            self.activeWl = None
            self.activeLineWidth = None
            self.activeLande = None
        if (self.activeWl is not None) and (self.pickoffState != names.PICKOFF_MIRROR):
            #interpolate fluxes to active wavelengths

            self.activeBackground = self.scaledSummedBgFluxAtCoronalWl[:, self.activeIndex]
            #!!! smuggle relative acitve background into the next for loop
            self.activeThermalBb = self.scaledThermalFluxAtCoronalWl[self.activeIndex]
            # need to determine the pixel bandwith for all active lines

            res = np.zeros(np.size(self.activeWl))
            sca = np.zeros((np.size(self.activeWl), 4))
            self.activeRelativeBackground = np.zeros(np.size(self.activeWl))

            for i in range(np.size(self.activeWl)):
                self.activeRelativeBackground[i] = np.interp(self.meanRadiusCorona,
                                                             self.radiusCorona,
                                                             self.activeBackground[:, i])
                res[i] = self.cnSp.pixel_bandwidth(self.activeWl[i])
                #sp efficiency at a certain wavelength e-/flux unit
                sca[i, :] = self.cnFluxSp.update_cw_only(self.activeWl[i], res[i], self)
            self.activeRelativeBackground = (1e6*self.activeRelativeBackground
                                             /fluxModel.coronalLineModel\
                                                 .diskCenterAtCoronalWlFlux[self.activeIndex])
            self.activeBandwidth = res
            self.activeFluxScales = sca
            # lineBrightness is spectral
            self.activeLineWidthPixel = self.activeLineWidth/self.activeBandwidth
            self.activeLineConvolution = np.sqrt(self.activeLineWidthPixel**2
                                                 + (self.instrumentProfileFwhm
                                                    /self.pixelSize)**2)
            # print('active line width pixel', self.activeLineWidthPixel)
            # print('active line convolution', self.activeLineConvolution)
            self.activeLineCountsPerPixel = ((self.activeLineBrightness*self.cnFluxSp.instrProf
                                              *self.activeFluxScales[:, 0]
                                              *self.spIntTime*self.spCoAdds
                                              *self.spBinX*self.spBinY))

            self.activeBackgroundCountsPerPixel = ((self.activeBackground*self.cnFluxSp.instrProf
                                                    *self.activeFluxScales[:, 0]
                                                    *self.spIntTime*self.spCoAdds
                                                    *self.spBinX*self.spBinY))
            self.activeThermalCountsPerPixel = ((np.ones((np.size(self.radiusCorona),
                                                          np.size(self.activeWl)))
                                                 *self.cnFluxSp.instrProf
                                                 *self.activeFluxScales[:, 2]
                                                 *self.spIntTime*self.spCoAdds
                                                 *self.spBinX*self.spBinY))
            self.activeDarkCountsPerPixel = ((self.spdetectorDarkCurrent
                                              *self.spIntTime*self.spCoAdds
                                              *self.spBinX*self.spBinY))

            #########################################################################
            # find index for radius closest to min and max radius in the field
            # have to do smaller than increment search otherwise map dragging will cause problems
            # if self.meanRadius >= 1:
            self.activeRadiusInd = np.where(np.abs(self.radiusCorona-self.meanRadiusCorona)
                                            < self.radiusIncrement)[0][0]
            self.activeMinRadiusInd = np.where(np.abs(self.radiusCorona-self.minRadiusCoronaSp)
                                               < self.radiusIncrement)[0][0]
            self.activeMaxRadiusInd = np.where(np.abs(self.radiusCorona-self.maxRadiusCoronaSp)
                                               < self.radiusIncrement)[0][-1]

            # for noise calculation go back to electrons
            self.activeTotalSignal = ((self.activeLineCountsPerPixel
                                       + self.activeBackgroundCountsPerPixel
                                       + self.activeThermalCountsPerPixel
                                       + self.activeDarkCountsPerPixel)
                                      *self.activeLineConvolution)
            self.activeLineSignal = (self.activeLineCountsPerPixel
                                     *self.activeLineConvolution)
            # print('active line signal', self.activeLineSignal[self.activeRadiusInd])
            self.activeSnPhotonOnly = np.sqrt(self.activeLineSignal) # in electrons
            self.activeSnPhotonOnlyAllStatesI = np.sqrt(self.nrModStates*2)*self.activeSnPhotonOnly
            self.activeSnPhotonOnlyAllStatesQUV = self.activeSnPhotonOnlyAllStatesI / np.sqrt(3)
            
            self.activeNoise = (self.activeTotalSignal
                                + self.activeLineConvolution
                                *self.detectorReadNoiseSp**2)

            self.activeSn = (self.activeLineSignal)/np.sqrt(self.activeNoise)
            # dual beam and multiple modulation states
            self.activeSnAllStatesI = np.sqrt(self.nrModStates*2)*self.activeSn
            self.activeSnAllStatesQUV = self.activeSnAllStatesI / np.sqrt(3)
            
            self.activeVelocitySensitivityPhotonOnly = ((1./self.activeSnPhotonOnlyAllStatesI)
                                                        *3e8*self.activeLineWidth
                                                        /(np.sqrt(2)*self.activeWl))
            # 4.7e-9 takes care of fact that wl is in um not nm
            self.activeBSensitivityPhotonOnly = (np.sqrt(2)*self.activeVelocitySensitivityPhotonOnly
                                                 /(self.activeWl*4.7e-9*3e8*self.activeLande))

            self.activeVelocitySensitivity = ((1./self.activeSnAllStatesI)*
                                              3e8*self.activeLineWidth /
                                              (np.sqrt(2)*self.activeWl))

            self.activeBSensitivity = (np.sqrt(2)*self.activeVelocitySensitivity
                                       /(self.activeWl*4.7e-9*3e8*self.activeLande))

            # no sensitivity estimations for some lines
            for i in range(len(self.activeIndex)):
                if not fluxModel.coronalLinesSensitiveEstimations[i]:
                    self.activeBSensitivity[:, i] = np.full(self.activeBSensitivity.shape[0],
                                                            np.nan)
                    self.activeVelocitySensitivity[:, i] = np.full(self.activeBSensitivity\
                        .shape[0], np.nan)
                    self.activeBSensitivityPhotonOnly[:, i] = np.full(self.activeBSensitivity\
                        .shape[0], np.nan)
                    self.activeVelocitySensitivityPhotonOnly[:, i] = np.full(self\
                        .activeBSensitivity.shape[0], np.nan)
            if self.debug >= 2:
                print(self.activeTotalSignal[self.activeRadiusInd])
                print(self.activeNoise[self.activeRadiusInd])

        # region - camera well and saturation check
        ###########################################################################
        ## automatic well check for camera
        ###########################################################################
        #TODO: saturation check is dependent on camera mode!!!!
        spRatio = self.spMaxPixelExposure/self.spIntTime
        ciRatio = self.ciMaxPixelExposure/self.ciIntTime
        # print('sat ratio', spRatio, ciRatio)
        if self.meanRadius < 1:
            # binX and binY contribute to total counts but
            # individual pixels are important for saturation
            self.spSaturationLevel = ((self.spDiskTotalElectronsMax*spRatio
                                       /(self.detectorWellSp*self.detectorWellPercentage))
                                      /(self.spCoAdds*self.spBinX*self.spBinY))
            self.ciSaturationLevel = ((self.ciDiskTotalElectronsMax*ciRatio
                                       /(self.detectorWellCi*self.detectorWellPercentage))
                                      /(self.ciCoAdds*self.ciBinX*self.ciBinY))
            self.spSaturationLevelCenter = ((self.spDiskTotalElectrons*spRatio
                                             /(self.detectorWellSp*self.detectorWellPercentage))
                                            /(self.spCoAdds*self.spBinX*self.spBinY))
            self.ciSaturationLevelCenter = ((self.ciDiskTotalElectrons*ciRatio
                                             /(self.detectorWellCi*self.detectorWellPercentage))
                                            /(self.ciCoAdds*self.ciBinX*self.ciBinY))
        else:
            self.spSaturationLevel = ((self.spCoronaTotalElectronsMax*spRatio
                                       /(self.detectorWellSp*self.detectorWellPercentage))
                                      /(self.spCoAdds*self.spBinX*self.spBinY))
            self.ciSaturationLevel = ((self.ciCoronaTotalElectronsMax*ciRatio
                                       /(self.detectorWellCi*self.detectorWellPercentage))
                                      /(self.ciCoAdds*self.ciBinX*self.ciBinY))
            self.spSaturationLevelCenter = ((self.spCoronaTotalElectrons*spRatio
                                             /(self.detectorWellSp*self.detectorWellPercentage))
                                            /(self.spCoAdds*self.spBinX*self.spBinY))
            self.ciSaturationLevelCenter = ((self.ciCoronaTotalElectrons*ciRatio
                                             /(self.detectorWellCi*self.detectorWellPercentage))
                                            /(self.ciCoAdds*self.ciBinX*self.ciBinY))
        if self.debug >= 2:
            print("sp sat levels", self.spSaturationLevel, self.spSaturationLevelCenter)
            print("ci sat levels", self.ciSaturationLevel, self.ciSaturationLevelCenter)
        if self.detectorWellCheckSp:
            if self.spSaturationLevel > 1:
                # if saturated drop integration time und increase co adds
                self.spIntTime = (self.spIntTime/(np.floor(self.spSaturationLevel)+1))
                self.spCoAdds = self.spCoAdds*(np.floor(self.spSaturationLevel)+1)
                # disable saturation check if minimum exposure time for fast mode is reached
                if self.spIntTime < self.fastFrameTimeSp:
                    self.spIntTime = self.fastFrameTimeSp
                    self.spCoAdds = 1
                    self.detectorWellCheckSp = False

        if self.detectorWellCheckCi:
            if self.ciSaturationLevel > 1:
                # if saturated drop integration time und increase co adds
                self.ciIntTime = (self.ciIntTime/(np.floor(self.ciSaturationLevel)+1))
                self.ciCoAdds = self.ciCoAdds*(np.floor(self.ciSaturationLevel)+1)
                # disable saturation check if minimum exposure time for fast mode is reached
                if self.ciIntTime < self.fastFrameTimeCi:
                    self.ciIntTime = self.fastFrameTimeCi
                    self.ciCoAdds = 1
                    self.detectorWellCheckCi = False
        # endregion

    def load_object(self, source, what, cat):
        """
        loading stored pickel objects
        """
        if cat == "atlas":
            if what == "spAtlas":
        #        f=open(source, "rb")
                with open(source, "rb") as myFile:
                    self.spAtlas = pickle.load(myFile)
            if what == "ciAtlas":
                with open(source, "rb") as myFile:
                    self.ciAtlas = pickle.load(myFile)
        elif cat == "filter":
            if what == "spFilter":
                with open(source, "rb") as myFile:
                    self.spFilter = pickle.load(myFile)
            elif what == "ciFilter":
                with open(source, "rb") as myFile:
                    self.ciFilter = pickle.load(myFile)
        elif cat == "flux":
            if what == "zannaAr":
                with open(source, "rb") as myFile:
                    self.zannaAr = pickle.load(myFile)
            elif what == "zannaQr":
                with open(source, "rb") as myFile:
                    self.zannaQr = pickle.load(myFile)
            elif what == "zannaQs":
                with open(source, "rb") as myFile:
                    self.zannaQs = pickle.load(myFile)
        elif cat == "radialDependence":
            if what == "limbDarkening":
                with open(source, "rb") as myFile:
                    self.diskRadialScale = pickle.load(myFile)
        else:
            print("Unknown object type!")

    def saveObject(self, destination):
        """ save object with pickle

        """
        with open(destination, 'wb') as output:  # Overwrites any existing file.
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

    def add_row_to_tw(self, tw, i, param, val):
        """
        insert a row to existing table
        """
        tw.insertRow(i)
        tw.setItem(i, 0, QtWidgets.QTableWidgetItem(param))
        tw.setItem(i, 1, QtWidgets.QTableWidgetItem(val))

    def update_tw_ci(self, tw, i):
        """
        insert ouputs to ci GUI table
        """
        #self.add_row_to_tw(i,"grating_order",self.blazeOrder)
        #i += 1
        #cnSpectrograph(self
        font = QtGui.QFont()
        font.setBold(True)
        # self.ui.tableWidget.item(row_index, col_index).setFont(font)

        if self.meanRadius < 1.:
            self.add_row_to_tw(tw, i, "total efficiency [%]",
                               str((self.ciTransmissionCw*100).round(2)))
            i += 1
            self.add_row_to_tw(tw, i, "Pixel @ selected wavelength",
                               "single beam, single exposure")
            tw.item(i, 0).setFont(font)
            tw.item(i, 1).setFont(font)
            i += 1
            self.add_row_to_tw(tw, i, "Disk: S/N I",
                               str(format_e(self.ciDiskSn)))
            i += 1
            self.add_row_to_tw(tw, i, "Disk: total counts [e-]",
                               str(format_e(self.ciDiskTotalElectrons)))
            # there is no background signal for this high disk flux
            i += 1
            self.add_row_to_tw(tw, i, "Disk: Continuum Signal [e-]",
                               str(format_e(self.ciDiskTotalElectrons
                                            - self.ciDarkElectrons
                                            - self.ciDiskThermalElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Disk: Thermal Background [e-]",
                               str(format_e(self.ciDiskThermalElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Camera Dark Signal [e-]",
                               str(format_e(self.ciDarkElectrons)))
            # modulation
            if self.nrModStates > 1 and self.ciPolarimetry:
                i += 1
                self.add_row_to_tw(tw, i, "Pixel @ selected wavelength",
                                   "single beam, full modulation cycle")
                tw.item(i, 0).setFont(font)
                tw.item(i, 1).setFont(font)
                i += 1
                self.add_row_to_tw(tw, i, "Disk: S/N I",
                                   str(format_e(self.ciDiskSnAllStatesI)))
                i += 1
                self.add_row_to_tw(tw, i, "Disk: S/N QUV",
                                   str(format_e(self.ciDiskSnAllStatesQUV)))
        else:
            # coronal case
            self.add_row_to_tw(tw, i, "total effciency [%]",
                               str((self.ciTransmissionCw*100).round(2)))
            i += 1
            self.add_row_to_tw(tw, i, "Pixel @ selected wavelength",
                               "single beam, single exposure")
            tw.item(i, 0).setFont(font)
            tw.item(i, 1).setFont(font)
            if np.isclose(self.ciCoronaSn, 0.0, atol=0.01): 
                i += 1
                self.add_row_to_tw(tw, i, "Maybe no emission line", 'in filter bandpass')
            i += 1
            self.add_row_to_tw(tw, i, "Corona: S/N I",
                               str(format_e(self.ciCoronaSn)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: total counts [e-]",
                               str(format_e(self.ciCoronaTotalElectrons)))
            # What would e corona be here
            i += 1
            self.add_row_to_tw(tw, i, "Corona: E corona Signal [e-]",
                               str(format_e(self.ciCoronaElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: total background [e-]",
                               str(format_e(self.ciCoronaBackgroundElectronsPerPixel
                                            + self.ciCoronaBlockingElectronsPerPixel
                                            + self.ciCoronaThermalElectronsPerPixel
                                            + self.ciDarkElectrons)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: Model background [e-]",
                               str(format_e(self.ciCoronaBackgroundElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: Through blocking of filter [e-]",
                               str(format_e(self.ciCoronaBlockingElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: Thermal background [e-]",
                               str(format_e(self.ciCoronaThermalElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Camera Dark Signal [e-]",
                               str(format_e(self.ciDarkElectrons)))
            # modulation
            if self.nrModStates > 1 and self.ciPolarimetry:
                i += 1
                self.add_row_to_tw(tw, i, "Pixel @ selected wavelength",
                                   "single beam, full modulation cycle")
                tw.item(i, 0).setFont(font)
                tw.item(i, 1).setFont(font)
                i += 1
                self.add_row_to_tw(tw, i, "Corona: S/N I",
                                   str(format_e(self.ciCoronaSnAllStatesI)))
                # i += 1
                # self.add_row_to_tw(tw, i, "Corona: S/N QUV",
                #                    str(format_e(self.ciCoronaSnAllStatesQUV)))

    def update_tw_sp(self, tw, i):
        """
        insert ouputs to SP GUI table
        """
        font = QtGui.QFont()
        font.setBold(True)

        self.add_row_to_tw(tw, i, "grating incidence angle [degrees]", str(self.alpha.round(2)))
        i += 1
        self.add_row_to_tw(tw, i, "grating diffracted angle [degrees]", str(self.beta.round(2)))
        i += 1
        self.add_row_to_tw(tw, i, "linear dispersion [nm/um]",
                           (format_e(self.linearDispersion)))
        i += 1
        self.add_row_to_tw(tw, i, "pixel bandwidth [nm]",
                           str(format_e(self.linearDispersion*self.pixelSize)))
        i += 1
        self.add_row_to_tw(tw, i, "min on array [um] ", str(self.lamMinOnArray.round(4)))
        i += 1
        self.add_row_to_tw(tw, i, "max on array [um]", str(self.lamMaxOnArray.round(4)))
        i += 1
        self.add_row_to_tw(tw, i, "instrument profile FWHM [pixel]",
                           str((self.instrumentProfileFwhm/self.pixelSize).round(2)))
        i += 1
        self.add_row_to_tw(tw, i, "resolution",
                           str(self.instrumentProfileResolution.round(0)))
        i += 1
        self.add_row_to_tw(tw, i, "grating blaze order", str(self.blazeOrder.round(2)))
        i += 1
        self.add_row_to_tw(tw, i, "estimated grating efficiency [%]"
                           , str((self.gratingEfficiency[0]*100).round(2)))
        i += 1
        self.add_row_to_tw(tw, i, "spatial slit sampling [arcsec]",
                           str(round(self.spatialSamplingSlit, 2)))
        i += 1
        self.add_row_to_tw(tw, i, "spatial pixel sampling [arcsec]",
                           str(round(self.spatialSamplingPixelSp, 2)))
        i += 1
        self.add_row_to_tw(tw, i, "binned pixel [nm x arcsec]",
                           str(round(self.spBinX*self.linearDispersion*self.pixelSize, 4))+" x "
                           + str(round(self.spBinY*self.spatialSamplingPixelSp, 2)))
        i += 1
        self.add_row_to_tw(tw, i, "total single beam efficiency [%]",
                           str((self.spTransmissionSCw*100).round(2)))
        if self.meanRadius < 1.:
            # disk case
            i += 1
            self.add_row_to_tw(tw, i, "Pixel @ center wavelength", "single beam, single exposure")
            tw.item(i, 0).setFont(font)
            tw.item(i, 1).setFont(font)
            i += 1
            self.add_row_to_tw(tw, i, "Disk: S/N I",
                               str(format_e(self.spDiskSn)))
            i += 1
            self.add_row_to_tw(tw, i, "Disk: Total counts [e-]",
                               str(format_e(self.spDiskTotalElectrons)))
            i += 1
            self.add_row_to_tw(tw, i, "Disk: Thermal Background [e-]",
                               str(format_e(self.spDiskThermalElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Camera Dark Signal [e-]",
                               str(format_e(self.spDarkElectrons)))
            # modulation
            if self.nrModStates > 1:
                i += 1
                self.add_row_to_tw(tw, i, "Pixel @ center wavelength",
                                   "dual beam, full modulation cycle")
                tw.item(i, 0).setFont(font)
                tw.item(i, 1).setFont(font)
                i += 1
                self.add_row_to_tw(tw, i, "Disk: S/N I",
                                   str(format_e(self.spDiskSnAllStatesI)))
                i += 1
                self.add_row_to_tw(tw, i, "Disk: S/N QUV",
                                   str(format_e(self.spDiskSnAllStatesQUV)))
        else:
            # sp coronal case
            i += 1
            self.add_row_to_tw(tw, i, "Pixel @ center wavelength", 
                               "single beam, single exposure")
            tw.item(i, 0).setFont(font)
            tw.item(i, 1).setFont(font)
            if np.isclose(self.spCoronaSn, 0.0, atol=0.01): 
                i += 1
                self.add_row_to_tw(tw, i, "SP might not be centered", 'on emission line')
            i += 1
            self.add_row_to_tw(tw, i, "Corona: S/N I",
                               str(format_e(self.spCoronaSn)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: Total counts [e-/pixel]",
                               str(format_e(self.spCoronaTotalElectrons)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: E corona signal [e-/pixel]",
                               str(format_e(self.spCoronaElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: Total background [e-/pixel]",
                               str(format_e(self.spCoronaBackgroundElectronsPerPixel
                                            + self.spCoronaThermalElectronsPerPixel
                                            + self.spDarkElectrons)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: Model background [e-/pixel]",
                               str(format_e(self.spCoronaBackgroundElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Corona: Thermal background [e-/pixel]",
                               str(format_e(self.spCoronaThermalElectronsPerPixel)))
            i += 1
            self.add_row_to_tw(tw, i, "Camera Dark Signal [e-/pixel]",
                               str(format_e(self.spDarkElectrons)))
            # modulation
            if self.nrModStates > 1:
                i += 1
                self.add_row_to_tw(tw, i, "Pixel @ center wavelength",
                                   "dual beam, full modulation cycle")
                tw.item(i, 0).setFont(font)
                tw.item(i, 1).setFont(font)
                i += 1
                self.add_row_to_tw(tw, i, "Corona: S/N I",
                                   str(format_e(self.spCoronaSnAllStatesI)))
            #     i += 1
            #     self.add_row_to_tw(tw, i, "Corona: S/N QUV",
            #                        str(format_e(self.spCoronaSnAllStatesQUV)))
            # active line calculations
            if ((self.activeWl is not None) and (self.pickoffState != names.PICKOFF_MIRROR) and
               (self.nrModStates > 1)):
                i += 1
                self.add_row_to_tw(tw, i, "Metrics for lines in passband",
                                   "dual beam, full modulation cycle")
                tw.item(i, 0).setFont(font)
                tw.item(i, 1).setFont(font)
                i += 1
                self.add_row_to_tw(tw, i, "Active line wavelengths [um]",
                                   (format_e(self.activeWl, 4)))
                i += 1
                self.add_row_to_tw(tw, i, "Active line rel. brightness [B_disk]",
                                   (format_e(self\
                                       .activeRelativeLineBrightness[self.activeRadiusInd], 1)))
                i += 1
                if self.lineScaleValue > 0:
                    answer = "yes"
                else:
                    answer = "no"
                self.add_row_to_tw(tw, i, "Active lines are scaled", answer)
                i += 1
                self.add_row_to_tw(tw, i, "Active line background [B_disk]",
                                   (format_e(self.activeRelativeBackground, 1)))
                i += 1
                self.add_row_to_tw(tw, i, "Active line SN (shotnoise only)",
                                   (format_e(self.activeSnPhotonOnly[self.activeRadiusInd])))
                i += 1
                self.add_row_to_tw(tw, i, "Active line SN",
                                   (format_e(self.activeSn[self.activeRadiusInd])))
                i += 1
                if self.debug >= 2:
                    print("activeRadiusInd", self.activeRadiusInd)
                vec = self.activeVelocitySensitivity[self.activeRadiusInd]
                self.add_row_to_tw(tw, i, "Active line v sensitivity [m/s]",
                                   (format_e(vec)))
                i += 1
                vec = self.activeBSensitivity[self.activeRadiusInd]
                self.add_row_to_tw(tw, i, 'Active line B_V sensitivity [Gauss]',
                                   (format_e(vec)))
                i += 1
                self.add_row_to_tw(tw, i, "sensitivity field dependence", "")
                i += 1
                if self.debug >= 2:
                    print("activeMinRadiusInd", self.activeMinRadiusInd)
                vec = self.activeVelocitySensitivity[self.activeMinRadiusInd]
                self.add_row_to_tw(tw, i, "Active line max v sensitivity [m/s]",
                                   (format_e(vec)))
                i += 1
                vec = self.activeVelocitySensitivity[self.activeMaxRadiusInd]
                self.add_row_to_tw(tw, i, "Active line min v sensitivity [m/s]",
                                   (format_e(vec)))
                i += 1
                vec = self.activeBSensitivity[self.activeMinRadiusInd]
                self.add_row_to_tw(tw, i, "Active line max B_V sensitivity [Gauss]",
                                   (format_e(vec)))
                i += 1
                vec = self.activeBSensitivity[self.activeMaxRadiusInd]
                self.add_row_to_tw(tw, i, "Active line min B_V sensitivity [Gauss]",
                                   (format_e(vec)))
            else:
                i += 1
                self.add_row_to_tw(tw, i, "Active line message:",
                                   "no active line in filter passband")
                i += 1
                self.add_row_to_tw(tw, i, "",
                                   "or modulator off")
                i += 1
                self.add_row_to_tw(tw, i, "",
                                   "or pickoff mirror in beam")
def format_e(n, p=2):
    """
    scientific formatin of string with desired precision
    """
    try:
        many = len(n)
        string = "{:."+str(int(p))+"E}, "
        a = "["
        for i in range(many):
            if i == many-1:
                string = "{:."+str(int(p))+"E}"
            if not np.isfinite(n[i]):
                if i == many-1:
                    a = a + "NaN"
                else:
                    a = a + "NaN, "
            else:
                l = string.format(n[i])
                tester = l.split("E")[1].replace("0", "").replace(",", "").strip()

                if tester == "+":
                    # print("single digit",n[i])
                    if i == many-1:
                        a = a + l.split("E")[0]
                    else:
                        a = a + l.split("E")[0] + ", "
                else:
                    a = a + l.split("E")[0]+"E"+l.split("E")[1].replace("0", "")

        a = a + "]"
    except TypeError:
        # single number
        if not np.isfinite(n):
            a = "NaN"
        else:
            string = "{:."+str(int(p))+"E}"
            l = string.format(n)
            tester = l.split("E")[1].replace("0", "")
            if tester == "+":
                a = l.split("E")[0]
            else:
                a = l.split("E")[0]+"E"+l.split("E")[1].replace("0", "")

    return a
