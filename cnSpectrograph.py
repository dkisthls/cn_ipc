# -*- coding: utf-8 -*-
"""
Created on Tue Aug 22 08:17:02 2017

@author: Andre
"""
import numpy as np
# import matplotlib.pyplot as plt

class cnSpectrograph():
    def __init__(self, cnSim):
        # the cnSim object contains all the information for the spectrograph
        #    lam,n,theta,blaze,camfocal,colfocal,pixelSize,
        #                 nrSpectralPixel,slitWidth,slitHeight,telescopeFnumber,
        #                 telescopeDiameter,gratingWidth,gratingHeight):
        # fixed by design
        self.n = cnSim.n
        self.theta = cnSim.theta
        self.gratingBlaze = cnSim.gratingBlaze
        self.cameraFocalLength = cnSim.cameraFocalLength
        self.collimatorFocalLength = cnSim.collimatorFocalLength
        self.pixelSize = cnSim.pixelSize
        self.nrSpectralPixel = cnSim.nrSpectralPixel
        self.slitWidth = cnSim.slitWidth

        self.telescopeFnumber = cnSim.telescopeFnumber
        self.telescopeDiameter = cnSim.telescopeDiameter
        self.gratingWidth = cnSim.gratingWidth
        self.gratingHeight = cnSim.gratingHeight

        self.plateScale = self.plate_scale("arcsec/mm",self.telescopeDiameter,
                                           self.telescopeFnumber)
        cnSim.plateScale = self.plateScale
        cnSim.plateScaleUnit = "arcsec/mm"
        self.spatialSamplingSlit = self.plateScale*self.slitWidth/1000.
        cnSim.spatialSamplingSlit = self.spatialSamplingSlit
        self.spatialSamplingPixelSp = (self.plateScale/1000.*self.pixelSize*
                                       self.collimatorFocalLength/
                                       self.cameraFocalLength)
        cnSim.spatialSamplingPixelSp = self.spatialSamplingPixelSp
        self.spatialSamplingPixelCi = (self.plateScale/1000.*self.pixelSize)
        cnSim.spatialSamplingPixelCi = self.spatialSamplingPixelCi

        # things update from here

        self.update(cnSim)

    def update(self, cnSim):
        # update what might have changed from parent
        self.lam = cnSim.spCw
        self.slitWidth = cnSim.slitWidth
        self.slitHeight = cnSim.slitHeight

        self.spatialSamplingSlit = self.plateScale*self.slitWidth/1000.
        cnSim.spatialSamplingSlit = self.spatialSamplingSlit
        self.spatialSamplingPixelSp = (self.plateScale/1000.*self.pixelSize*
                                       self.collimatorFocalLength/
                                       self.cameraFocalLength)
        cnSim.spatialSamplingPixelSp = self.spatialSamplingPixelSp
        # print('cnspectrograph slit sampling', self.spatialSamplingSlit)
        self.blazeFunction = True
        self.blazeOrder, self.blazeEfficiency = self.blaze_order(self.lam, self.n,
                                                                 self.gratingBlaze,
                                                                 self.theta,
                                                                 self.blazeFunction)
        cnSim.blazeOrder = self.blazeOrder[0]
        cnSim.blazeEfficiency = self.blazeEfficiency[0]
        # how to handle if user chooses different order
        self.gratingOrder = self.blazeOrder
        self.gratingEfficiency = self.blazeEfficiency
        cnSim.gratingOrder = self.gratingOrder
        cnSim.gratingEfficiency = self.blazeEfficiency

        self.alpha, self.beta = self.alphaBeta(self.gratingOrder,self.n,self.lam,
                                               self.theta)
        cnSim.alpha = self.alpha[0]
        cnSim.beta = self.beta[0]
        self.anamorphicFactor = self.anamorphic_factor(self.alpha,self.beta)
        cnSim.anamorphicFactor = self.anamorphicFactor
        self.angularDispersion = self.angular_dispersion(self.beta,
                                                        self.gratingOrder,self.n)
        cnSim.angularDispersion = self.angularDispersion
        self.linearDispersion = self.linear_dispersion(self.n,self.gratingOrder,
                                                      self.beta,
                                                      self.cameraFocalLength)
        cnSim.linearDispersion = self.linearDispersion[0]
        self.pixelBandwidth = self.linearDispersion*self.pixelSize/1000. #in um
        cnSim.pixelBandwidth = self.pixelBandwidth[0]
        self.lamMinOnArray, self.lamMaxOnArray = self.edgeLam(self.lam,
                                                              self.linearDispersion,
                                                              self.pixelSize,
                                                              self.nrSpectralPixel)
        cnSim.lamMinOnArray = self.lamMinOnArray[0]
        cnSim.lamMaxOnArray = self.lamMaxOnArray[0]
#        self.entranceSlitDiffraction("fnumber")
#        cnSim.slitDiffraction = self.slitDiffraction
#        cnSim.slitDiffractionUnit = "fnumber"


        (self.instrumentProfileFwhm, self.instrumentProfile,
         self.instrumentProfileDimension, self.instrumentProfileX,
         self.instrumentProfileY, self.instrumentProfileResolution,
         self.instrumentProfileLinearSeparation)=self.spectrograph_profile(self.lam,
                                                                           self.slitWidth,
                                                                           self.slitHeight,
                                                                           self.gratingWidth,
                                                                           self.gratingHeight,
                                                                           self.collimatorFocalLength,
                                                                           self.cameraFocalLength,
                                                                           self.pixelSize,
                                                                           self.angularDispersion,
                                                                           self.anamorphicFactor)

        cnSim.instrumentProfileFwhm = self.instrumentProfileFwhm
        cnSim.instrumentProfile = self.instrumentProfile
        cnSim.instrumentProfileDimension = self.instrumentProfileDimension
        cnSim.instrumentProfileX = self.instrumentProfileX
        cnSim.instrumentProfileY = self.instrumentProfileY
        cnSim.instrumentProfileResolution = self.instrumentProfileResolution[0]
        cnSim.instrumentProfileLinearSeparation = self.instrumentProfileLinearSeparation

    def pixel_bandwidth(self, lam):
        # update what might have changed from parent

        # blazeFunction = True
        blazeOrder, _ = self.blaze_order(lam, self.n, self.gratingBlaze,
                                         self.theta, self.blazeFunction)

        # how to handle if user chooses different order
        gratingOrder = blazeOrder

        _, beta = self.alphaBeta(gratingOrder, self.n, lam, self.theta)
        linearDispersion = self.linear_dispersion(self.n, gratingOrder, beta,
                                                  self.cameraFocalLength)
        pixelBandwidth = linearDispersion*self.pixelSize/1000. #in um

        return pixelBandwidth

    def alphaBeta(self, k, n, lam, theta):
        '''solve spectrograph equation to calculate alpha and beta'''
        # input:  - diffraction order k
        #         - grating ruling density n [groves/mm]
        #         - wavelength lam [um]
        #         - Littorw angle theta [degrees]
        #
        # output: - angle of incidence alpha [degrees]
        #         - angle of diffraction beta [degrees]

        beta = (np.degrees(np.arcsin((1e-3*k*n*lam) /
                                      (2. * np.cos(-1.*np.radians(theta))))) - theta)
        alpha = (np.degrees(np.arcsin((1e-3*k*n*lam) -
                                       (np.sin(np.radians(beta))))))
        return alpha, beta

    def anamorphic_factor(self,alpha,beta):
        '''calculate anamorphic factor'''
        # input:  - angle of incidence alpha [degrees]
        #         - angle of diffraction beta [degrees]
        # output: - anamorphic factor

        return ((np.cos(np.radians(alpha)) / np.cos(np.radians(beta))))

    def angular_dispersion(self, beta, k, n):
        '''calculate the angular dispersion of the grating'''
        # input:  - angle of diffraction beta [degrees]
        #         - diffraction order k
        #         - grating ruling density n [groves/mm]
        #
        # output: - angular dispersion dbdlam [rad/um]

        return (((1e-3*k*n) / np.cos(np.radians(beta))))
        #elf.angularDispersionUnits = "rad/um"

    def approximateBandpass(self, slit_image, pixel, linear_dispersion):
        '''
        approximate spectrograph bandpass

        Horiba.com
        In general, most spectrometers are not routinely used at the limit
        of their resolution so the influence of the slits may dominate the
        line profile. From Figure 18 the FWHM, due to the slits, is
        determined by either the image of the entrance slit or the exit slit,
        whichever is greater. If the two slits are perfectly matched and
        aberrations minimal compared to the effect of the slits, then the
        FWHM will be half the width at the base of the peak. (Aberrations
        may, however, still produce broadening of the base). Bandpass (BP)
        is then given by:
        BP = FWHM ~ linear dispersion x (exit slit width or the image of
        the entrance slit, whichever is greater).
        '''
        # input:  - slit image width [um]
        #         - pixel size [um]
        #         - linear dispersion [pm/um]
        # output: - approximated bandpass [um]

        bandpass = np.max([slit_image, pixel]) * linear_dispersion

        return bandpass

    def betaAngle(self,k,n,lam,alpha):
        '''solve spectrograph equation to calculate beta'''
        # input:  - diffraction order k
        #         - grating ruling density n [groves/mm]
        #         - wavelength lam [um]
        #         - angle of incidence alpha [degrees]
        # output: - angle of diffraction beta [degrees]

        beta = np.degrees(np.arcsin((1e-3*k*n*lam) - np.sin(np.radians(alpha))))
        return beta

    def gratingEfficiencyTheoretical(self, lam, n, k, gratingBlaze, alpha, beta):
        '''calculate theoretical grating efficiency'''
        # !function is only working for scalars

        # input:  - grating ruling density n [groves/mm]
        #         - wavelength lam [um]
        #         - grating order k
        #         - blaze angle t of the grating [degrees]
        #         - incidence angle alpha [degrees]
        #
        #
        # output: - estimated efficiency of order
        # d = 1.d /n # in mm
        # Don Mickey
        # gam = ((n*np.pi*np.cos(np.deg2rad(gratingBlaze))/lam) *
        #         (np.sin(np.deg2rad(beta-gratingBlaze)) +
        #         np.sin(np.deg2rad(alpha-gratingBlaze))))
        # res = np.square(np.sin(gam)/gam)
        # Casini
        a = np.deg2rad(alpha)
        b = np.deg2rad(beta)
        f = np.deg2rad(gratingBlaze)
        argument = (k*np.pi*(np.cos(a)/np.cos(a-f))*(np.cos(f) - np.sin(f)/np.tan((a+b)/2.)))
        resCas = np.square(np.sin(argument)/argument)
        r = np.cos(a)/np.cos(b)
        # print('shadow coefficient', 1./r)
        s = (np.cos(a - f)) / (r * np.cos(b - f))
        # catch alpha > beta case and set factor to 1
        fak = np.nanmin([s, 1.])
        if alpha + beta == 0 or alpha + beta == np.nan:
            res = 0
        else:
            res = fak * resCas

        return res


    def blaze_order(self, lam, n, gratingBlaze, theta, blazeFunction=False):
        '''calculate theoretical spectrograph blaze order'''
        # input:  - grating ruling density n [groves/mm]
        #         - wavelength lam [um]
        #         - blaze angle t of the grating [degrees]
        #         - optional blaze_function keyword
        #         - optional littrow angle theta [degrees]
        #
        # output: - blaze order
        #         - estimated max efficiency of order

        blazeOrder = (np.around(np.sin(np.radians(gratingBlaze))*2./(lam*n*1e-3)))
        blazeEfficiency = 1.
        if blazeFunction == True:
            test = int(blazeOrder)+2
            res = np.zeros((2, test))
            for k in range(1, test):
                alpha, beta = self.alphaBeta(k, n, lam, theta)
                # print('order, alpha, beta', k, alpha, beta)

                res[0, k] = k
                res[1, k] = self.gratingEfficiencyTheoretical(lam, n, k, gratingBlaze, alpha, beta)

            blazeOrder = res[0, res[1, :] == np.nanmax(res[1, :])]
            blazeEfficiency = res[1, res[1, :] == np.nanmax(res[1, :])]

        return blazeOrder, blazeEfficiency

    def spectrographCc(self, cc, pixel_wav, q):
        '''suport function for the instrument profile function'''
        # proposed functional form of dlam_cc
        al = np.log(4.0/3.0)
        dlam_cc = pixel_wav * np.sqrt(cc**2.0 + 4.0*np.exp(-1.0*al*cc**q))
        return dlam_cc

    def edgeLam(self, lam, linearDispersion, pixelSize, nrSpectralPixel):
        '''calculate minimum and maximum wavelength on detector'''
        # input:  - wavelength lam in [um]
        #         - linear dispersion [um/mm] == [nm/um]
        #         - number of pixels in spectral direction
        # output: - minimal wavelength [um]
        #         - maximal wavelength [um]

        lamMinOnArray = (lam - (nrSpectralPixel/2. * (linearDispersion*pixelSize))/1000.)
        lamMaxOnArray = (lam + (nrSpectralPixel/2. * (linearDispersion*pixelSize))/1000.)
        return lamMinOnArray, lamMaxOnArray

    def entranceSlitDiffraction(self, unit):
        '''how big is the diffraction caused by the entrance slit'''
        # input:  - wavelength lam [um]
        #         - slit width [um]
        #         - desired unit "rad" or "fnumber"
        #
        # output: - diffraction value in radians or fnumber

        if unit == "rad":
            self.slitDiffraction = (self.lam)/self.slitWidth
        elif unit == "fnumber":
            self.slitDiffraction = self.slitWidth/(self.lam)

    def plate_scale(self, unit, telescopeDiameter, telescopeFnumber):
        '''calculate the plate scale for telescope'''
        # input:    - fnumber of the beam
        #           - aperture diameter of the telescope [mm]
        #
        # output:   - plate scale in arcsec/mm or um/arcsec
        if unit == "arcsec/mm":
            plateScale = (206265./(telescopeDiameter*telescopeFnumber))
        elif unit == "um/arcsec":
            plateScale = (1000.*(telescopeDiameter*telescopeFnumber)/206265.)

        return plateScale


    def entranceSlitOnSky(self, w):
        '''hat needs the slit with to be in order to provide
        the desired spatial sampling on the object (SUN)'''
        # input:  - fnumber of the input beam
        #         - aperture diameter of the telescope [mm]
        #         - slit width w in [um]
        #
        # output: - sampling on sky in [arcsec]

        scale = self.plateScale(self.telescopeFnumber, self.telescopeDiameter,
                                unit="um/arcsec")
        self.spatialSamplingOnSky = w/scale

    #
    ##-------------------------------------------------------
    ## what needs the slit with to be in order to provide
    ## the desired spatial sampling on the object (SUN)
    ##-------------------------------------------------------
    #def cn_sp_entrance_slit_width, fnumber, tel_aperture, spatial_sampling_object
    #  # input:  - fnumber of the input beam
    #  #         - aperture diameter of the telescope [mm]
    #  #         - desired sampling on sky [arcsec]
    #  #
    #  # output: - entrance slit width in [um]
    #
    #  scale = plate_scale(fnumber, tel_aperture, unit="um/arcsec")
    #  sw = scale*spatial_sampling_object
    #  return, sw
    #end
    #
    ##-------------------------------------------------------
    ## calculate projected slit dimensons on detector
    ##-------------------------------------------------------
    #def cn_sp_entrance_slit_morph, alpha, beta, La, Lb, slit_w, slit_h, pixel
    #  # input:  - angle of incidence alpha [degrees]
    #  #         - angle of diffraction beta [degrees]
    #  #         - collimator focal length La [mm]
    #  #         - camera focal length Lb
    #  #         - slit width slit_w [um]
    #  #         - slit height slit_h [um]
    #  #         - pixel size pixel [um]
    #  #
    #  # output: - projected slit with and slit height [um]
    #  #         - optimal slith with to achieve Nynquist sampling in [um]
    #  #         - optimal slith with to achieve point sampling across prjected slit [um]
    #
    #  slit_w_dash = slit_w * (np.cos(!DTOR*alpha) / np.cos(!DTOR*beta)) * (Lb / La)
    #  slit_h_dash = slit_h * (Lb / La)
    #
    #  sw_nyn = 2.d*pixel / (np.cos(!DTOR*alpha) / np.cos(!DTOR*beta)) / (Lb / La)
    #  sw_4 = 4.d*pixel / (np.cos(!DTOR*alpha) / np.cos(!DTOR*beta)) / (Lb / La)
    #
    #  return, [[slit_w_dash],[slit_h_dash],[sw_nyn],[sw_4]]
    #end
    #
    ##-------------------------------------------------------
    ## calculate effectiv spectrograph fnumbers
    ##-------------------------------------------------------
    #def cn_sp_fnumber, La, Lb, W, H, alpha, beta
    #  # input:  - collimator focal length La [mm]
    #  #         - camera focal length Lb[mm]
    #  #         - grating width W [mm]
    #  #         - grating heigt H [mm]
    #  #         - angle of incidence alpha [degrees]
    #  #         - angle of diffraction beta [degrees]
    #  #
    #  # output: - effective input fnumber
    #  #         - effective output fnumber
    #
    #  fin = La / (2.d*sqrt((W*H*np.cos(!DTOR*alpha))/(np.pi)))
    #  fout = Lb / (2.d*sqrt((W*H*np.cos(!DTOR*beta))/(np.pi)))
    #  return, [[fin],[fout]]
    #end
    #
    #

    def linear_dispersion(self, n, order, beta, cameraFocalLength, inclinationAngle=None,
                          lh=None, angularDispersion=None):
        '''calculate the linear dispersion'''
        # input:  - grating ruling density n [groves/mm]
        #         - diffraction order k
        #         - angle of diffraction beta [degrees]
        #         - camera focal length Lb [mm]
        #   optional  - inclination angle in [degrees]
        #             - Perpendicular distance from the spectral plane to grating
        #               Lh [mm]
        #             - angular dispersion dbdlam [rad/um]
        # output: - linear dispersion dlamdx [um/mm]

        if inclinationAngle is not None:
            linearDispersion = ((1e3*np.cos(np.radians(beta))*
                                 (np.cos(np.radians(inclinationAngle))**2.))/ (order*n*lh))
        else:
            if angularDispersion is not None:
                linearDispersion = (1. /(angularDispersion) / cameraFocalLength)
            else:
                linearDispersion = ((1e3*np.cos(np.radians(beta)))/ (order*n*cameraFocalLength))
        return linearDispersion

        #
        ##-------------------------------------------------------
        ## calculate the linear dispersion at the slit
        ##-------------------------------------------------------
        #def cn_sp_linear_dispersion_slit, alpha, beta, La, angular_dispersion
        #  # input:  - angle of incidence alpha [degrees]
        #  #         - angle of diffraction beta [degrees]
        #  #         - collimator focal length La [mm]
        #  #         - angular dispersion [rad/um]
        #
        #  # output: - linear dispersion dlamdx [um/mm]
        #  dlamdx = (angular_dispersion*np.cos(!DTOR*beta) / np.cos(!DTOR*alpha)) * La
        #  return, 1.d/dlamdx
        #end
        #

    def si(self, x):

        # si(-x) = -si(x)
        my = 1.
        if x < 0:
            x = -1.*x
            my = -1.

        if x > 1.:
            #x > 1
            a1 = 38.027264
            a2 = 265.187033
            a3 = 335.677320
            a4 = 38.102495
            b1 = 40.021433
            b2 = 322.624911
            b3 = 570.236280
            b4 = 157.105423
            # error f < 5e-7
            f = ((1./x)*(x**8.+a1*x**6.+a2*x**4.+a3*x**2.+a4) /
                (x**8.+b1*x**6.+b2*x**4.+b3*x**2.+b4))

            a1 = 42.242855
            a2 = 302.757865
            a3 = 352.018498
            a4 = 21.821899
            b1 = 48.196927
            b2 = 482.485984
            b3 = 1114.978885
            b4 = 449.690326
            # error g < 3e-7
            g = ((1./x**2.)*(x**8.+a1*x**6.+a2*x**4.+a3*x**2.+a4) /
                (x**8.+b1*x**6.+b2*x**4.+b3*x**2.+b4))

            result = 0.5*np.pi - f*np.cos(x) - g*np.sin(x)
        else:
            result = (x*(1. - (x**2./6.)*((1./3.) - (x**2./20.)*((1./5.) - (x**2./294.)))))
        return my*result


    def spectrograph_profile(self, lam, slitWidth, slitHeight, gratingWidth,
                             gratingHeight, collimatorFocalLength,
                             cameraFocalLength, pixelSize, angularDispersion,
                             anamorphicFactor,
                             x=np.arange(-100., 101), y=np.arange(-100., 101),
                             coherence="incoherent", dimension="1D"):
        '''follows the paper by Casini and de Wijn, On the Instrument Profile of Slit Spectrographs
        Analytic expression for sliti image at detector provides accurate estimation for
        normalized FWHM of the instrument profile delta x dash / fcamera'''

        # input:    - x spatial in um
        #           - y spatial in um
        #           - wavelength lam [um]
        #           - slit width sw [um]
        #           - slit height sh [um]
        #           - grating width gw [um]
        #           - grating height gh [um]
        #           - focal length of collimator fcol [um]
        #           - focal length of camera fcam [um]
        #           - anamorphic factor r
        #           - pixel size [um]
        #           - angular dispersion [rad/um]
        #           - coherence optional
        #           - dimension optional, "1D" or "2D"
        # output:   - instrument profile of spectrograph
        #           - FWHM of instrument profile
        #           - resolution of sperctrograph

        sw = slitWidth
        sh = slitHeight*1000.    # all dimensions in um
        gw = gratingWidth*1000.
        gh = gratingHeight*1000.
        fcol = collimatorFocalLength*1000.
        fcam = cameraFocalLength*1000.
        pixel = pixelSize

        r = anamorphicFactor

        gs = r*sw / fcol    # angular spread of slit width as seen by collimator
        gg = r*lam / gw     # FWHM of diffraction profile correspinding to the
                            # aperture of the grating
        ghs = sh / fcol
        ghg = lam / gh

        txp = np.pi*0.5 * (gs + 2.*x/fcam) / gg
        txm = np.pi*0.5 * (gs - 2.*x/fcam) / gg
        sizeX = np.size(txp)

        typp = np.pi*0.5 * (ghs + 2.*y/fcam) / ghg
        tym = np.pi*0.5 * (ghs - 2.*y/fcam) / ghg
        sizeY = np.size(typp)

        if coherence == "coherent":
            ux = np.zeros(sizeX)
            uxy = np.zeros((sizeX, sizeY))
            for i in range(0, sizeX-1):
                if dimension == "1D":
                    ux[i] = (1./(np.pi))*(self.si(txm[i]) + self.si(txp[i]))
                elif dimension == "2D":
                    for s in range(0, sizeY-1):
                        uxy[i, s] = ((1./(np.pi)**2.)*(self.si(txm[i]) + self.si(txp[i]))*
                                     (self.si(tym[s])+self.si(typp[s])))
            if dimension == "1D":
                profile = np.square(ux)
            if dimension == "2D":
                profile = np.square(uxy)
        elif coherence == "incoherent":
            igx = np.zeros(sizeX)
            igxy = np.zeros((sizeX, sizeY))
            for i in range(0, sizeX-1):
                if dimension == "1D":
                    igx[i] = ((1./(np.pi))*(self.si(2.*txm[i])+self.si(2.*txp[i])
                                            - ((np.sin(txm[i]))**2./txm[i])
                                            - ((np.sin(txp[i]))**2./txp[i])))
                elif dimension == "2D":
                    for s in range(0, sizeY-1):
                        igxy[i, s] = ((1./(np.pi)**2.)
                                      *(self.si(2.*txm[i]) + self.si(2.*txp[i])
                                        - ((np.sin(txm[i]))**2./txm[i])
                                        - ((np.sin(txp[i]))**2./txp[i]))
                                      *(self.si(2.*tym[s]) + self.si(2.*typp[s])
                                        - ((np.sin(tym[s]))**2./tym[s])
                                        - ((np.sin(typp[s]))**2./typp[s])))
            if dimension == "1D":
                profile = igx
            if dimension == "2D":
                profile = igxy

        profile = profile / np.max(profile)
        # find FWHM of profile
        fwhm = 2.*np.abs(np.interp(0.5, profile[:int((sizeX-1)/2)], x[:int((sizeX-1)/2)]))

        # express pixel in wavelength units
        # linear dispersion to angular by dividing through fcam
        # angular to wavelength units by multiplying witn angular dispersion and
        # divide by 1000 sinc my angular dispersion is [rad/nm]
        pixel_wav = (pixel / fcam) / (angularDispersion)
        #fwhm_wav = (fwhm / fcam) / (self.angularDispersion)

        # line profile FWHM to pixel ratio
        cc = fwhm / pixel

        # dlam_cc is minimum resolvable spectral interval
        # if cc small (lim 0) then dlam_cc = 2*pixel
        # if cc large (lim inf) then dlam_cc = fwhm
        # if cc about 1 (pixel matching) then dlam_cc = 2*fwhm = 2*pixel

        # good choice of q: 2.5 <= q <= 3
        q = 2.75
        dlam_cc = self.spectrographCc(cc, pixel_wav, q)
        resolution = lam / dlam_cc

        # convert dlam_cc to linear separation
        dlam_cc_lin = dlam_cc * angularDispersion * fcam
        instrumentProfileFwhm = fwhm
        if dimension == "1D":
            instrumentProfile = profile
            instrumentProfileDimension = 1
            instrumentProfileX = x
            instrumentProfileY = None
        elif dimension == "2":
            instrumentProfile = profile
            instrumentProfileDimension = 2
            instrumentProfileX = x
            instrumentProfileY = y
        instrumentProfileResolution = resolution
        instrumentProfileLinearSeparation = dlam_cc_lin

        return (instrumentProfileFwhm, instrumentProfile,
                instrumentProfileDimension, instrumentProfileX,
                instrumentProfileY, instrumentProfileResolution,
                instrumentProfileLinearSeparation)

    def cn_sp_overlaps(self, lam, lmin, lmax, order):
        '''calculate spectral overlaps'''
        # input:  - wavelength lam [um]
        #         - minimal wavelength lmin [um]
        #         - maximal wavelength lmax [um]
        #         - grating order
        #
        # output: - free spectral range [um]
        #         - maximum undisturbed wavelength [um]
        #         - passband such that no other orther conatminates spectrum [um]

        free_spectral_range = lmin/order
        max_undisturbed = lmin + free_spectral_range

        c_const = order*lam
        min_const = order*lmin
        max_const = order*lmax

        # limiting case is next higher order delta_lam = lam_center / (2*k +1)
        # which gives a slightly narrower wavelength range compared to the next lower
        # order delta_lam = lam_center / (2*k -1)
        # Thus the filter passband should be lower than 2*delta_lam
        #
        passband = 2.*lam / (2*order + 1)

        return free_spectral_range, max_undisturbed, passband


        ##-------------------------------------------------------
        ## calculate theoretical grating only resolving power R
        ##-------------------------------------------------------
        #def cn_sp_resolving_power, w, alpha, beta, lam
        #  # input:  - grating width w [mm]
        #  #         - angle of incidence alpha [degrees]
        #  #         - angle of diffraction beta [degrees]
        #  #         - wavelength lam [um]
        #  #
        #  # output: - theoretical resolving power R
        #  #         - theoretical resolution [um]
        #
        #  R = w*(np.sin(!DTOR*alpha)+np.sin(!DTOR*beta)) / (1e-3*lam)
        #  resolution = lam / R
        #  return, [[R],[resolution]]
        #end

if __name__ == "__main__":
    pass

    # lam = 1.083
    # n = 31.6
    # theta = -5.5
    # blaze = 63.9
    # camfocal = 932.
    # colfocal = 2096.
    # pixelSize = 18.
    # nrSpectralPixel=1024
    # slitWidth = 52.
    # slitHeight = 42.
    # telescopeFnumber = 72.
    # telescopeDiameter = 4200.
    # gratingWidth = 408.
    # gratingHeight = 153.

    # a=cnSpectrograph(lam, n, theta, blaze, camfocal, colfocal, pixelSize, nrSpectralPixel,
    #                  slitWidth, slitHeight, telescopeFnumber, telescopeDiameter,
    #                  gratingWidth, gratingHeight)

    # # free, maxundis, passband = a.cn_sp_overlaps(lam, a.lamMinOnArray, a.lamMaxOnArray, a.blazeOrder)
    # # print('free, maxundisturbed, passband',free, maxundis, passband)
    # print(a.blazeOrder)
    # print(a.alpha, a.beta)
    # print(a.anamorphicFactor)
    # print(a.angularDispersion)
    # print("linear dispersion", a.linearDispersion, "nm/um")   # [um/mm] or [nm/um]
    # print("pixel contains :",a.linearDispersion*a.pixelSize,"nm of spectrum")
    # print(a.lamMinOnArray, a.lamMaxOnArray)
    # print(a.slitDiffraction)    # fnumber
    # print(a.plateScale) # arcsec/mm
    # print(a.instrumentProfileFwhm, a.instrumentProfileResolution)

    # aa=a.instrumentProfile
    # bb = aa[0][0]
    # cc = aa[1][0]
    # e = cc[:100]
    # f = bb[:100]
    # plt.plot(bb,cc)
    # print(np.interp(0.5,e,f))



