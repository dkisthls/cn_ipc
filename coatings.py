# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 22:04:28 2018

@author: Andre
"""
import numpy as np
# import matplotlib.pyplot as plt
import os
# import platform
import sys
baseDir=os.path.dirname(os.path.realpath(sys.argv[0]))

class coating():
  def __init__(self,lam):
    self.lam = lam
    
  def emissivityF(self,reflectivity):
    self.emissivity = 1.-reflectivity
    
  def restore(self):
    pass
  def save(self):
    pass
    
class coatingAlDkist(coating):

  def __init__(self,lam):

    coating.__init__(self,lam)
    self.source = "DKIST"
    l = np.array([393.33,396.80,430.50,450.40,460.73,486.13,530.30,
                         555.00,587.60,589.00,617.33,630.20,637.00,656.30,
                         668.40,705.40,789.00,819.48,854.20,1074.60,1083.00,
                         1430.,1565.,1920.,2326.,2580.,2855.,3027.,3935.])/1e3
    # M1 has aluminum coating from DKIST flux budget
    # reflectivity normalized to 1
    r = np.array([91.78,91.78,91.79,91.82,91.84,91.81,91.55,91.35,91.08,
                     91.07,90.74,90.60,90.52,90.38,90.30,89.86,86.51,86.25,
                     86.76,94.75,94.82,96.76,97.04,97.33,97.57,97.73,97.96,
                     98.13,98.53])/100.
    self.reflectivity = np.interp(lam,l,r)
    self.emissivityF(self.reflectivity)

class coatingFss99(coating):
  def __init__(self, lam, aoi):
    coating.__init__(self,lam)
    self.aoi = aoi
    self.read()
    # return fss99-500 reflectance interpolated to lam values

  def read(self):
    if self.aoi == 6:
      # vis data is inverted
      tmp = np.loadtxt(os.path.join(baseDir,'materialData',
                                    '6DEGAOI-FSS99-500_Runs9LB05and06_vis.txt'))
      t = np.flipud(tmp)
      
      # nir data is inverted
      tmp = np.loadtxt(os.path.join(baseDir,'materialData',
                                    '6DEGAOI-FSS99-500_Runs9LB05and06_nir.txt'))
      t = np.append(t,np.flipud(tmp),axis=0)
      
      # ir data is not inverted
      tmp = np.loadtxt(os.path.join(baseDir,'materialData',
                                    '6DEGAOI-FSS99-500_Runs9LB05and06_ir.txt'))
      t = np.append(t,tmp,axis=0)
      
      t = np.unique(t,axis=0)
    elif self.aoi == 45:
      # none of the data files are inverted
      t = np.loadtxt(os.path.join(baseDir,'materialData',
                                  '45DEGAOI-FSS99-500_RunJ9B03AGG_vis.txt'))
      tmp = np.loadtxt(os.path.join(baseDir,'materialData',
                                    '45DEGAOI-FSS99-500_RunJ9B03AGG_nir.txt'))
      t = np.append(t,tmp,axis=0)
      tmp = np.loadtxt(os.path.join(baseDir,'materialData',
                                    '45DEGAOI-FSS99-500_RunJ9B03AGG_ir.txt'))
      t = np.append(t,tmp,axis=0) 
      t = np.unique(t,axis=0)
    else: print("this angle of incidence is not supported at the moment")

    # data uses wavelength in nm and normalize to 1
    self.reflectivity = np.interp(self.lam,t[:,0]/1000.,t[:,1]/100.)
    self.emissivityF(self.reflectivity)
