#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 09:07:23 2018

@author: andreef
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import platform
import scipy.constants as pc

def blackbody(lam, temp, cgs=False, exitance=False):
    # calculates black body spectral radiance in SI units (W m^-2 ster^-1 m^-1),
    # i.e. radiant flux per unit solid angle per unit projected area
    # (directional)
    # or
    # calcualtes black body spectral exitance (W m^-2 m^-1), i.e. radiant flux
    # per surface unit area exitance = pi*radiance
    #
    # luminance, i.e. Luminous power per unit solid angle per unit projected source area (cd/m^2)
    # in photometry (weighted for sensitivity of human eye) is equivalent to radiance, i.e.
    # Radiant flux emitted, reflected, transmitted
    # or received by a surface, per unit solid angle per unit projected area (W m^-2 ster^-1 m^-1)
    # in radiometry
    #
    # lambda in m
    # temp in K
    # keyword cgs returns blackbody radiance in CGS units (erg cm^-2 ster^-1 cm^-1)


    # boltzmann constant
    k = pc.k
    # planck constant
    h = pc.h
    # speed of light
    c = pc.c

    # blackbody
    b = (2. * h * c**2.) / (lam**5. * (np.exp((h*c)/(lam*k*temp))-1.))

    if exitance:
        b = np.py * b

    # 1 erg = 1e-7 J
    # 1 W = 1 J/s
    # => 1 W = 1e7 erg / s
    # 1 m = 100 cm
    if cgs:
        b = 10. * b
    return b

def photonEnergy(lam):
    # lambda in m
    h = pc.h
    # light speed [m s^-1]
    c = pc.c
    en = (h * c) / (lam) # [J]
    return en


def integratedLineIntensity2spectralIntensity(intFlux, cw, lineWidth, lam, returnAmplitude=False):
    # gaussian =a*exp(-(x-b)^2/(2*c^2)) where b=cw and c=lineWidth
    c = lineWidth/(2.*np.sqrt(2.*np.log(2.)))
    a = intFlux/(c*np.sqrt(2*np.pi))
    e = a*np.exp(-1.*(np.square(lam-cw))/(2.*c**2.))
    if returnAmplitude:
        return a
    else:
        return e

def gaussianLineSpectrum(lam, amp, cw, lineWidth):
    c = lineWidth/(2.*np.sqrt(2.*np.log(2.)))
    e = amp*np.exp(-1.*(np.square(lam-cw))/(2.*c**2.))
    return e

def oldarcsec2sterad(arcsec):
    rad = np.deg2rad(arcsec / 3600.)
    sterad = 2.*np.pi* (1. - np.cos(rad/2.))
    return sterad

def arcsec2sterad(m, n):
    radM = np.deg2rad(m / 3600.)
    radN = np.deg2rad(n / 3600.)

    tM = (np.tan(radM/2))
    tN = (np.tan(radN/2))
    # tM = (np.tan(radM/2))**2
    # tN = (np.tan(radN/2))**2
    # sterad = 4.*np.arccos(np.sqrt((1. + tM + tN)/((1. + tM)*(1. + tN))))

    # for precision issue need to do taylor expansion
    sterad = (4.*tM*tN - 2*tM*tN*(tM**2.+tN**2.) + 1.5*tM*tN*(tM**4.+tN**4.)
              + 5./3.*(tM*tN)**3. - 1.25*tM*tN*(tM**6.+tN**6.) - 7./4.*(tM*tN)**3. *(tM**2 + tN**2))

    return sterad

def sterad2arcsec(sterad):
    rad = 2.*np.arccos(1.-(sterad/(2.*np.pi)))
    arcsec = np.rad2deg(rad)*3600.
    return arcsec


def wnum2ang(wavenum, help=False):
    """
    NAME:
    		WNUM2ANG
    PURPOSE:
    		Convert input wavenumber in cm^-1 (vacuum) to air Angstroms
    		Routine first computes refractive index of air using a
    		standard formulation (Coleman, Bozman and Meggers# 1960#
    		"Table of Wavenumbers"# NBS Monograph 3).
    		Then computes wavelength using...
    			angstroms=1./(n*wavenum).*1.e8
    CATEGORY:
    		Mathematical
    CALLING SEQUENCE:
    		angstrom=wnum2ang(wavenum)
    INPUTS:
    		wavenum - floating point wavenumber in Kaysers
    			(cm^-1 in vacuum)
    KEYWORD PARAMETERS:
    		None
    OUTPUTS:
    		angstrom - wavelength in Angstroms computed for air
    COMMON BLOCKS:
    NOTES:
    		MJP 29 Dec 93 - IDL version implemented
    MODIFICATION HISTORY:

    -"""

    if help is True:
        print('Convert input wavenumber in cm^-1 (vacuum) to air Angstroms')
        print('angstrom=wnum2ang(wavenum)')
        print('wavenum - floating point wavenumber in Kaysers')
        print('(cm^-1 in vacuum)')
        print('angstrom - wavelength in Angstroms computed for air')
        angstrom = 0.
        return angstrom


    #Compute index of refraction from NBS formulation
    a = 1.+6432.8e-8
    b = 2949810.0
    c = 146.0e8
    d = 25540.0
    e = 41.0e8
    n = a + (b/(c-wavenum**2.)) + (d/(e-wavenum**2.))

    angstrom = 1./(n*wavenum)*1.e8
    return angstrom

if __name__ == "__main__":
    
    #%%

    lam = np.array([1074.7, 1079.8, 1282.16, 1430.5, 2218.3, 2584.6, 3028.5, 3934.3])
    # convert to wavnumber
    wn = 10000000./lam
    ang = wnum2ang(wn)
    print(lam)
    print(ang/10)
