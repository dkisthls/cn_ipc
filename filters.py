# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 08:55:28 2018

@author: cindy
"""

from functools import reduce  # forward compatibility for Python 3
import operator
# import numpy as np

filterDictSP = {
    "Ca II 854-12":{
        "wl": 854,
        "cw": 854.2,
        "fwhm": 12,
        "min": 842,
        "max": 866,
        "atlas": 'SP854.pkl',
        "filterData": "SP_854_12_2.5_delivered.pkl",
        "removeOutliers": [0, 1.05]
        },
    # "HeI,Fe XIII 1080-20":{
    #     "wl": 1080,
    #     "cw": 1083.00,
    #     "fwhm": 20,
    #     "min": 1060,
    #     "max":1100
    #     },
    "HeI,Fe XIII 1077-20":{
        "wl": 1077,
        "cw": 1083.00,
        "fwhm": 20,
        "min": 1057,
        "max":1097,
        "atlas": 'SP1077.pkl',
        "filterData": "SP_1077_20_2.5_delivered.pkl",
        "removeOutliers": [0, 1.05]
        },
    "S IX 1252-27":{
        "wl":1252,
        "cw": 1252.0,
        "fwhm": 27,
        "min": 1225,
        "max":1279,
        "atlas": 'CI1250.pkl',
        "filterData": "SP_1252_27_2.5_delivered.pkl",
        "removeOutliers": [0, 1.05]
        },
    "HpaschB 1282-28":{
        "wl":1282,
        "cw": 1281.8,
        "fwhm": 28,
        "min": 1254,
        "max":1310,
        "atlas": 'CI1250.pkl',
        "filterData": "SP_1282_28_2.5_delivered.pkl",
        "removeOutliers": [0, 1.05]
        },
    "Si X 1430-35":{
        "wl":1430,
        "cw": 1430.10,
        "fwhm": 35,
        "min": 1395,
        "max":1465,
        "atlas": 'SP1430.pkl',
        "filterData": "SP_1430_35_2.5_delivered.pkl",
        "removeOutliers": None
        },
    "Fe IX 2218-43":{
        "wl":2218,
        "cw": 2217.7,
        "fwhm": 43,
        "min": 2175,
        "max":2261,
        "atlas": 'SP2218.pkl',
        "filterData": "SP_2218_43_2.5_delivered.pkl",
        "removeOutliers": None
        },
    "Mg VIII 3028-70":{
        "wl":3028,
        "cw": 3027.7,
        "fwhm": 70,
        "min": 2958,
        "max":3098,
        "atlas": 'SP3028.pkl',
        "filterData": "SP_3028_70_2.5_delivered.pkl",
        "removeOutliers": None
        },
    "Si IX 3934-124":{
        "wl":3934,
        "cw": 3934.3,
        "fwhm": 124,
        "min": 3810,
        "max":4058,
        "atlas": 'SP3934.pkl',
        "filterData": "SP_3934_124_2.5_delivered.pkl",
        "removeOutliers": None
        },
    "CO 4651-171":{
        "wl":4651,
        "cw": 4666.0,
        "fwhm": 171,
        "min": 4480.0,
        "max":4822.0,
        "atlas": 'SP4651.pkl',
        "filterData": "SP_4651_171_2.5_delivered.pkl",
        "removeOutliers": None
        }
    }

filterDictCI = {
    "1um cont. 1049.5-1":{
        "wl": 1049.50,
        "cw": 1049.50,
        "fwhm": 1,
        "min": 1048.5,
        "max":1050.5,
        "atlas": 'CI1049.pkl',
        "filterData": "CI_1049_1_2.5_delivered.pkl",
        "removeOutliers": [0, 1.05]
        },
    "Fe XIII 1074.7-1":{
        "wl": 1074.70,
        "cw": 1074.70,
        "fwhm": 1,
        "min": 1073.7,
        "max":1075.7,
        "atlas": 'SP1080.pkl',
        "filterData": "CI_1074_1_2.5_delivered.pkl",
        "removeOutliers": [0, 1.05]
        },
    # "Fe XIII 1079.8-1":{
    #     "wl": 1079.80,
    #     "cw": 1079.80,
    #     "fwhm": 1,
    #     "min": 1078.8,
    #     "max":1080.8,
    #     "atlas": 'SP1080.pkl',
    #     "filterData": "CI_1079_1_2.5_delivered.pkl",
    #     "removeOutliers": [0, 1.05]
    #     },
    "He I 1083.0-1":{
        "wl": 1083.00,
        "cw": 1083.00,
        "fwhm": 1,
        "min": 1082.0,
        "max":1084.0,
        "atlas": 'SP1080.pkl',
        "filterData": "CI_1083_1_2.5_delivered.pkl",
        "removeOutliers": [0, 1.05]
        },
    "J Band 1250-160":{
        "wl":1250.00,
        "cw": 1250.00,
        "fwhm": 160,
        "min": 1090.,
        "max":1410.,
        "atlas": 'CI1250.pkl',
        "filterData": "CI_1250_160_2.5_delivered.pkl",
        "removeOutliers": None
        },
    "HpaschB 1281.8-1":{
        "wl":1281.80,
        "cw": 1281.80,
        "fwhm": 1,
        "min": 1280.8,
        "max":1282.8,
        "atlas": 'CI1250.pkl',
        "filterData": "CI_1281_1_2.5_delivered.pkl",
        "removeOutliers": None
        },
    "Si X 1430.0-5":{
        "wl":1430.10,
        "cw": 1430.10,
        "fwhm": 5,
        "min": 1425.0,
        "max":1435.0,
        "atlas": 'SP1430.pkl',
        "filterData": "CI_1430_5_2.5_delivered.pkl",
        "removeOutliers": None
        }
    }


def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)

def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value

'''
spectralLines=np.empty( (0,len(filterDict)) )
for key in filterDict:
    print (key, getFromDict(filterDict, [key, "wl"] ))
    spectralLines=np.append(spectralLines,getFromDict(filterDict, [key, "wl"]))

print (spectralLines)
'''