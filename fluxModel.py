#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 29 12:34:52 2019

@author: Andre Fehlmann (afehlmann@nso.edu)
"""

import numpy as np
import matplotlib.pyplot as plt

from solarConstants import solarConstants
sc = solarConstants()

import solarFlux
from fluxModelParser import fluxModelParser

class fluxModel():
    '''class to contain a custom flux model'''
    def __init__(self,
                 modelName,
                 lam,
                 radCorona,
                 radDisk,
                 parseFromFile=False,
                 modelPath=None,
                 debug=False):
        #TODO: Allow for atmospheric transmission profile to be used to scale line fluxes
        self.lam = lam
        self.radiusCorona = radCorona
        self.radiusDisk = radDisk

        if parseFromFile:
            #!!! loading should be done outside in ipcDriver
            self.modelPath = modelPath
            self.modelName = modelName
            #parse model
            self.solarFluxModel, self.parsedDic, self.parsedBgDic = fluxModelParser(self.modelPath)
            if debug:
                print(self.parsedDic)
                print('done parsing from file')

            self.coronalLinesCount = len(self.parsedDic['lineWavelengths'])
            self.coronalLinesNames = np.array(self.parsedDic['lineNames'])
            self.coronalLinesWl = np.array(self.parsedDic['lineWavelengths'])
            self.coronalLinesLande = np.array(self.parsedDic['lineLandes'])
            self.coronalLinesWidth = np.array(self.parsedDic['lineWidths'])
            self.coronalLinesSensitiveEstimations = np.array(self.parsedDic[\
                'lineSenstitivityEstimations'])
            self.coronalLinesBrightnesses = np.array(self.parsedDic['lineBrightnesses'])
            self.coronalLinesBrightnessUnit = np.array(self.parsedDic['lineBrightnessUnit'])
            self.coronalLinesBrightnessesAreAbsolute = np.array(self.parsedDic[\
                'lineBrightnessesAreAbsolute'])
            self.coronalLinesOriginalRadii = np.array(self.parsedDic['lineOriginalRadii'])
            self.coronalLinesRadiusDependencesAreAbsolute = np.array(self\
                .parsedDic['lineRadiusDependencesAreAbsolute'])
            #!!! not all radial dependences might have same length
            # count = self.findMaxLengthOfList(self.parsedDic['lineRadii'])
            self.coronalLinesRadii = self.parsedDic['lineRadii']
            self.coronalLinesRadiusDependences = self.parsedDic['lineRadiusDependences']

            self.bgComponentsCount = len(self.parsedBgDic['bgNames'])
            self.bgComponentsNames = self.parsedBgDic['bgNames']
            self.bgComponentsOriginalRadii = self.parsedBgDic['bgOriginalRadii']
            self.bgComponentsOriginalWavelengths = self.parsedBgDic['bgOriginalWavelengths']
            self.bgComponentsBrightnesses = self.parsedBgDic['bgBrightnesses']
            self.bgComponentsBrightnessUnit = self.parsedBgDic['bgBrightnessUnit']
            self.bgComponentsBrightnessesAreAbsolute = self.parsedBgDic['bgBrightnessesAreAbsolute']
            self.bgComponentsRadii = self.parsedBgDic['bgRadii']
            self.bgComponentsRadiusDependences = self.parsedBgDic['bgRadiusDependences']
            self.bgComponentsRadiusDependencesAreAbsolute = self\
                .parsedBgDic['bgRadiusDependencesAreAbsolute']
            self.bgComponentsWavelengths = self.parsedBgDic['bgWavelengths']
            self.bgComponentsWavelengthDependences = self.parsedBgDic['bgWavelengthDependences']
            self.bgComponentsWavelengthDependencesAreAbsolute = self\
                .parsedBgDic['bgWavelengthDependencesAreAbsolute']


            # disk center flux is needed in the line model
            validList = ['Blackbody', 'Allen', 'DKIST']
            assert(self.solarFluxModel in validList),\
                'No or unknown solarFluxModel specified. Valid values are: blackbody, Allen, DKIST'
            if self.solarFluxModel == 'Blackbody':
                self.diskCenterAtCoronalWlFlux = solarFlux\
                    .solarFluxBlackbody(self.coronalLinesWl).flux
                self.diskCenter = solarFlux.solarFluxBlackbody(self.lam)
            elif self.solarFluxModel == 'Allen':
                self.diskCenterAtCoronalWlFlux = solarFlux.solarFluxAllen(self.coronalLinesWl).flux
                self.diskCenter = solarFlux.solarFluxAllen(self.lam)
            elif self.solarFluxModel == 'DKIST':
                self.diskCenterAtCoronalWlFlux = solarFlux.solarFluxDkist(self.coronalLinesWl).flux
                self.diskCenter = solarFlux.solarFluxDkist(self.lam)


            self.coronalLineModel = solarFlux.solarFluxCustomLines(\
                self.lam,
                self.coronalLinesCount,
                self.coronalLinesNames,
                self.coronalLinesWidth,
                self.coronalLinesWl,
                self.coronalLinesOriginalRadii,
                self.coronalLinesBrightnesses,
                self.coronalLinesBrightnessUnit,
                self.coronalLinesBrightnessesAreAbsolute,
                self.coronalLinesRadii,
                self.coronalLinesRadiusDependences,
                self.coronalLinesRadiusDependencesAreAbsolute,
                self.diskCenterAtCoronalWlFlux,
                self.radiusCorona)

            self.backgroundModel = solarFlux.solarFluxCustomBgComponent(\
                self.lam,
                self.bgComponentsCount,
                self.bgComponentsNames,
                self.bgComponentsOriginalRadii,
                self.bgComponentsOriginalWavelengths,
                self.bgComponentsBrightnesses,
                self.bgComponentsBrightnessUnit,
                self.bgComponentsBrightnessesAreAbsolute,
                self.bgComponentsRadii,
                self.bgComponentsRadiusDependences,
                self.bgComponentsRadiusDependencesAreAbsolute,
                self.bgComponentsWavelengths,
                self.bgComponentsWavelengthDependences,
                self.bgComponentsWavelengthDependencesAreAbsolute,
                self.diskCenter,
                self.radiusCorona)
        else:
            if debug:
                print('load from pickle file')

        if debug:
            print('finished flux model initialisation')

if __name__ == "__main__":
    lamIncrement = 0.0001
    docLam = np.arange(0.5, 5.5, lamIncrement)
    radiusIncrement = 0.05
    radiusCorona = np.arange(1., 1.6, radiusIncrement)
    radiusDisk = np.arange(0., 1., radiusIncrement)
    a = fluxModel('penn2004', docLam, radiusCorona, radiusDisk,
                  parseFromFile=True, modelPath='fluxObjects/Penn2004.txt')

    print('flux shape shape', a.backgroundModel.totalBgRelativeBrightness)
    a.coronalLineModel.myflux = a.coronalLineModel.fluxInterpolationFunction([1., 1.1, 1.3, 1.5])

    for i in range(a.coronalLinesCount):
        fig, ax = plt.subplots()
        test = a.coronalLineModel.fluxInterpolationFunction(1.)/a.diskCenterAtCoronalWlFlux[i]*1e6
        ax.plot(docLam, a.coronalLineModel.myflux/a.diskCenterAtCoronalWlFlux[i]*1e6)
        ax.set_xlim([a.coronalLinesWl[i]-0.001, a.coronalLinesWl[i]+0.001])
        ax.set_ylim([0, max(test)])
        ax.set_ylabel('relative Brightness [$B_{disk}$]')
        ax.set_xlabel(r'wavelength [$\mu$m]')
        plt.title('Relative line brightness with Radius ('+a.modelName+','+
                  a.coronalLinesNames[i]+')')
        plt.legend(['1.0', '1.1', '1.3', '1.5'])
        plt.savefig('documentationGraphs/radialBrightnessAndLineProfile-'+a.modelName+'-'+
                    a.coronalLinesNames[i]+'-'+a.solarFluxModel+'.png',
                    dpi=150, bbox_inches='tight')
        plt.close(fig)

    # bg components with radius
    for k in range(a.bgComponentsCount):
        fig, ax = plt.subplots()
        flux = np.squeeze(a.backgroundModel.fluxInterpolationFunction[k]([1., 1.1, 1.3, 1.5]))
        print('flux shape', flux.shape)

        for i in range(4):
            ax.plot(docLam, flux[:, i]/a.backgroundModel.diskCenter.flux*1e6)
        # ax.set_yscale('log')
        ax.set_ylabel('relative Brightness [$B_{disk}$]')
        ax.set_xlabel(r'wavelength [$\mu$m]')
        plt.title('Background component brightness with Radius ('+a.bgComponentsNames[k]+')')
        plt.legend(['1.0', '1.1', '1.3', '1.5'])
        plt.savefig('documentationGraphs/radialBrightnessBgComponent-'+a.modelName+'-'+
                    a.bgComponentsNames[k]+'-'+a.solarFluxModel+'.png',
                    dpi=150, bbox_inches='tight')
        plt.close(fig)


    #spectra
    mymax = 0
    #  flux = np.squeeze(a.coronalLineModel.fluxInterpolationFunction([1.1],docLam))
    flux = np.squeeze(a.coronalLineModel.fluxInterpolationFunction([1.1]))
    if max(flux/a.backgroundModel.diskCenter.flux*1e6) > mymax:
        mymax = max(flux/a.backgroundModel.diskCenter.flux*1e6)
    fig, ax = plt.subplots()
    ax.plot(docLam, flux/a.backgroundModel.diskCenter.flux*1e6)
    for k in range(a.bgComponentsCount):
        flux = np.squeeze(a.backgroundModel.fluxInterpolationFunction[k]([1.1]))
    #    flux = np.squeeze(a.backgroundModel.fluxInterpolationFunction[k]([1.1], docLam))
        ax.plot(docLam, flux/a.backgroundModel.diskCenter.flux*1e6)
        if max(flux/a.backgroundModel.diskCenter.flux*1e6) > mymax:
            mymax = max(flux/a.backgroundModel.diskCenter.flux*1e6)
    ax.set_ylim([1e-2, mymax])
    ax.set_yscale('log')
    ax.set_ylabel('relative Brightness [$B_{disk}$]')
    ax.set_xlabel(r'wavelength [$\mu$m]')
    plt.legend([a.modelName]+a.bgComponentsNames)
    plt.title('coronal spectra (R=1.1, '+a.modelName+')')
    plt.savefig('documentationGraphs/individualRelativeCoronalSpectrum-'+a.modelName+'-'+
                a.bgComponentsNames[k]+'-'+a.solarFluxModel+'.png', dpi=150, bbox_inches='tight')
    plt.close(fig)

    # combined spectra
    #  flux = np.squeeze(a.coronalLineModel.fluxInterpolationFunction([1.1],docLam))
    flux = np.squeeze(a.coronalLineModel.fluxInterpolationFunction([1.1]))
    for k in range(a.bgComponentsCount):
        flux = flux+np.squeeze(a.backgroundModel.fluxInterpolationFunction[k]([1.1]))
    #    flux = flux+np.squeeze(a.backgroundModel.fluxInterpolationFunction[k]([1.1], docLam))

    fig, ax = plt.subplots()
    ax.plot(docLam, flux/a.backgroundModel.diskCenter.flux*1e6)
    ax.set_yscale('log')
    ax.set_ylabel('relative Brightness [$B_{disk}$]')
    ax.set_xlabel(r'wavelength [$\mu$m]')
    plt.title('coronal Spectrum (R=1.1, '+a.modelName+')')
    plt.savefig('documentationGraphs/relativeCoronalSpectrum-'+a.modelName+'-'+
                a.bgComponentsNames[k]+'-'+a.solarFluxModel+'.png', dpi=150, bbox_inches='tight')
    plt.close(fig)

    fig, ax = plt.subplots()
    ax.plot(docLam, flux)
    ax.set_yscale('log')
    ax.set_ylabel('absolute Flux [W m-2 m-1 st-1]')
    ax.set_xlabel(r'wavelength [$\mu$m]')
    plt.title('coronal Spectrum (R=1.1, '+a.modelName+')')
    plt.savefig('documentationGraphs/absoluteSiCoronalSpectrum-'+a.modelName+'-'+
                a.bgComponentsNames[k]+'-'+a.solarFluxModel+'.png', dpi=150, bbox_inches='tight')
    plt.close(fig)
