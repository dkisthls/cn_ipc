#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 14:50:21 2019

@author: Andre Fehlmann (afehlmann@nso.edu)
"""
import numpy as np
debug=False
if debug:
  filename='/home/andreef/Dropbox (DKIST)/python/CryoNIRSP/cnIpc/fluxModelTemplate.txt'

def fluxModelParser(filename):
  if debug:
    print(filename)
  lineNames=[]
  lineWavelengths=[]
  lineBrightnesses=[]
  lineBrightnessUnit=[]
  lineBrightnessesAreAbsolute=[]
  lineWidths=[]
  lineLandes=[]
  lineSenstitivityEstimations=[]
  lineOriginalRadii=[]
  lineRadii=[]
  lineRadiusDependences=[]
  lineRadiusDependencesAreAbsolute=[]
  bgNames=[]
  bgOriginalRadii=[]
  bgOriginalWavelengths=[]
  bgBrightnesses=[]
  bgBrightnessUnit=[]
  bgBrightnessesAreAbsolute=[]
  bgRadii=[]
  bgRadiusDependences=[]
  bgRadiusDependencesAreAbsolute=[]
  bgWavelengths=[]
  bgWavelengthDependences=[]
  bgWavelengthDependencesAreAbsolute=[]
  
  with open(filename, 'r') as file:
    line = file.readline()
    while line:
      # otherwise end of line caracters in there
      line = line.strip()
      # ignore empty lines
      if line != "":
        # decide if comment or not
        if line[0] != "#":
          # separate at the =
          pre, aft = line.split("=")
          #remove unwanted white spaces
          pre = pre.strip()
          aft = aft.strip()
          if pre == "solarFluxModel":
            solarFluxModel=aft
          elif pre == "lineName":
            lineNames.append(aft)
          elif pre == "lineWavelength":
            lineWavelengths.append(float(aft))
          elif pre == "lineBrightness":
            lineBrightnesses.append(float(aft))
          elif pre == "lineBrightnessUnit":
            lineBrightnessUnit.append((aft))
          elif pre == "lineBrightnessIsAbsolute":
            if aft == "True":
              lineBrightnessesAreAbsolute.append(bool(1))
            else:
              lineBrightnessesAreAbsolute.append(bool(0))
          elif pre == "lineWidth":
            lineWidths.append(float(aft))
          elif pre == "lineLande":
            lineLandes.append(float(aft))
          elif pre == "lineSenstitivityEstimation":
            if aft == "True":
              lineSenstitivityEstimations.append(bool(1))
            else:
              lineSenstitivityEstimations.append(bool(0))
          elif pre == "lineOriginalRadius":
            lineOriginalRadii.append(float(aft))
          elif pre == "lineRadius":
            #strip the brackets
            aft = aft.strip("][")
            aft = aft.split(",")
            temp =[]
            for i in aft:
              temp.append(float(i))  
            lineRadii.append(temp)
          elif pre == "lineRadiusDependence":
            if aft[0] == '[':
              #strip the brackets
              aft = aft.strip("][")
              aft = aft.split(",")
              temp =[]
              for i in aft:
                temp.append(float(i))  
              lineRadiusDependences.append(temp)
            else:
              lineRadiusDependences.append(aft)
          elif pre == "lineRadiusDependenceIsAbsolute":
            if aft == "True":
              lineRadiusDependencesAreAbsolute.append(bool(1))
            else:
              lineRadiusDependencesAreAbsolute.append(bool(0))
          # background components
          elif pre == "bgName":
            bgNames.append(aft)
          elif pre == "bgOriginalRadius":
            bgOriginalRadii.append(float(aft))
          elif pre == "bgOriginalWavelength":
            bgOriginalWavelengths.append(float(aft))
          elif pre == "bgBrightness":
            bgBrightnesses.append(float(aft))
          elif pre == "bgBrightnessUnit":
            bgBrightnessUnit.append((aft))
          elif pre == "bgBrightnessIsAbsolute":
            if aft == "True":
              bgBrightnessesAreAbsolute.append(bool(1))
            else:
              bgBrightnessesAreAbsolute.append(bool(0))
          elif pre == "bgRadius":
            #strip the brackets
            aft = aft.strip("][")
            aft = aft.split(",")
            temp =[]
            for i in aft:
              temp.append(float(i))  
            bgRadii.append(temp)
          elif pre == "bgRadiusDependence":
            if aft[0] == '[':
              #strip the brackets
              aft = aft.strip("][")
              aft = aft.split(",")
              temp =[]
              for i in aft:
                temp.append(float(i))  
              bgRadiusDependences.append(temp)
            else:
              lineRadiusDependences.append(aft)
          elif pre == "bgRadiusDependenceIsAbsolute":
            if aft == "True":
              bgRadiusDependencesAreAbsolute.append(bool(1))
            else:
              bgRadiusDependencesAreAbsolute.append(bool(0))
          elif pre == "bgWavelength":
            #strip the brackets
            aft = aft.strip("][")
            aft = aft.split(",")
            temp =[]
            for i in aft:
              temp.append(float(i))  
            bgWavelengths.append(temp)
          elif pre == "bgWavelengthDependence":
            if aft[0] == '[':
              #strip the brackets
              aft = aft.strip("][")
              aft = aft.split(",")
              temp =[]
              for i in aft:
                temp.append(float(i))  
              bgWavelengthDependences.append(temp)
            else:
              lineRadiusDependences.append(aft)
          elif pre == "bgWavelengthDependenceIsAbsolute":
            if aft == "True":
              bgWavelengthDependencesAreAbsolute.append(bool(1))
            else:
              bgWavelengthDependencesAreAbsolute.append(bool(0))
          
          
          
      line = file.readline()
    #make sanity check on lengths of list
    components = ['lineNames','lineWavelengths','lineBrightnesses','lineBrightnessUnit','lineBrightnessesAreAbsolute',
               'lineWidths','lineLandes','lineSenstitivityEstimations','lineOriginalRadii',
               'lineRadii','lineRadiusDependences','lineRadiusDependencesAreAbsolute']
    my_list = [lineNames,lineWavelengths,lineBrightnesses,lineBrightnessUnit,lineBrightnessesAreAbsolute,
               lineWidths,lineLandes,lineSenstitivityEstimations,lineOriginalRadii,
               lineRadii,lineRadiusDependences,lineRadiusDependencesAreAbsolute]
    
    bgComponents = ['bgNames','bgOriginalRadii','bgOriginalWavelengths','bgBrightnesses','bgBrightnessUnit',
              'bgBrightnessesAreAbsolute','bgRadii','bgRadiusDependences','bgRadiusDependencesAreAbsolute',
                    'bgWavelengths','bgWavelengthDependences','bgWavelengthDependencesAreAbsolute']
    bgList = [bgNames,bgOriginalRadii,bgOriginalWavelengths,bgBrightnesses,bgBrightnessUnit,
              bgBrightnessesAreAbsolute,bgRadii,bgRadiusDependences,bgRadiusDependencesAreAbsolute,
                    bgWavelengths,bgWavelengthDependences,bgWavelengthDependencesAreAbsolute]
    #TODO: assert that matching radii/dependence and wl/dependence have same length
    for j in range(len(bgNames)):
      assert(len(bgRadii[j]) == len(bgRadiusDependences[j])),'radius and radius dependence vector have not the same length.'
      assert(len(bgWavelengths[j]) == len(bgWavelengthDependences[j])),'wavelength and wavelength dependence vector have not the same length.'
    
    n = len(my_list[0])
    if debug:
      print('my_list[0]',len(my_list[0]))
    if all(len(x) == n for x in my_list):
      if debug:
        print("all good")
    n = len(bgList[0])
    if debug:
      print('bgList[0]',len(bgList[0]))
    if all(len(x) == n for x in bgList):
      if debug:
        print("bg all good")
    my_dic = {}
    for i in range(len(components)):
      my_dic[components[i]] = my_list[i]
    bgDic = {}
    for i in range(len(bgComponents)):
      bgDic[bgComponents[i]] = bgList[i]
    return solarFluxModel, my_dic, bgDic
  # convert to numpy array
  # radii = np.array(radii)
if debug:
  res = fluxModelParser(filename)