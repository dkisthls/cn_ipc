# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ipcAdvGUI.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CameraWindow(object):
    def setupUi(self, CameraWindow):
        CameraWindow.setObjectName("CameraWindow")
        CameraWindow.resize(501, 743)
        font = QtGui.QFont()
        font.setPointSize(10)
        CameraWindow.setFont(font)
        CameraWindow.setDocumentMode(False)
        CameraWindow.setDockNestingEnabled(False)
        self.centralwidget = QtWidgets.QWidget(CameraWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.tab_4)
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_5 = QtWidgets.QLabel(self.tab_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setObjectName("label_5")
        self.gridLayout_3.addWidget(self.label_5, 1, 0, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.tab_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy)
        self.label_7.setObjectName("label_7")
        self.gridLayout_3.addWidget(self.label_7, 0, 0, 1, 1)
        self.advModTypeCB = QtWidgets.QComboBox(self.tab_4)
        self.advModTypeCB.setObjectName("advModTypeCB")
        self.advModTypeCB.addItem("")
        self.advModTypeCB.addItem("")
        self.gridLayout_3.addWidget(self.advModTypeCB, 1, 1, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.tab_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)
        self.label_6.setObjectName("label_6")
        self.gridLayout_3.addWidget(self.label_6, 2, 0, 1, 1)
        self.advNumModulationStepsSB = QtWidgets.QSpinBox(self.tab_4)
        self.advNumModulationStepsSB.setMinimum(4)
        self.advNumModulationStepsSB.setObjectName("advNumModulationStepsSB")
        self.gridLayout_3.addWidget(self.advNumModulationStepsSB, 2, 1, 1, 1)
        self.advModStateCB = QtWidgets.QComboBox(self.tab_4)
        self.advModStateCB.setObjectName("advModStateCB")
        self.advModStateCB.addItem("")
        self.advModStateCB.addItem("")
        self.advModStateCB.addItem("")
        self.gridLayout_3.addWidget(self.advModStateCB, 0, 1, 1, 1)
        self.verticalLayout_6.addLayout(self.gridLayout_3)
        self.advModReset = QtWidgets.QPushButton(self.tab_4)
        self.advModReset.setObjectName("advModReset")
        self.verticalLayout_6.addWidget(self.advModReset)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_6.addItem(spacerItem)
        self.tabWidget.addTab(self.tab_4, "")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.tab)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.advExposureSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.advExposureSB_SP.sizePolicy().hasHeightForWidth())
        self.advExposureSB_SP.setSizePolicy(sizePolicy)
        self.advExposureSB_SP.setDecimals(1)
        self.advExposureSB_SP.setMaximum(1000000.0)
        self.advExposureSB_SP.setSingleStep(1.0)
        self.advExposureSB_SP.setProperty("value", 1000.0)
        self.advExposureSB_SP.setObjectName("advExposureSB_SP")
        self.gridLayout_2.addWidget(self.advExposureSB_SP, 3, 1, 1, 1)
        self.advModPeriodL = QtWidgets.QLabel(self.tab)
        self.advModPeriodL.setObjectName("advModPeriodL")
        self.gridLayout_2.addWidget(self.advModPeriodL, 10, 1, 1, 1)
        self.advCoaddSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        self.advCoaddSB_SP.setDecimals(0)
        self.advCoaddSB_SP.setMinimum(1.0)
        self.advCoaddSB_SP.setMaximum(1000.0)
        self.advCoaddSB_SP.setObjectName("advCoaddSB_SP")
        self.gridLayout_2.addWidget(self.advCoaddSB_SP, 7, 1, 1, 1)
        self.label_23 = QtWidgets.QLabel(self.tab)
        self.label_23.setObjectName("label_23")
        self.gridLayout_2.addWidget(self.label_23, 0, 0, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.tab)
        self.label_13.setObjectName("label_13")
        self.gridLayout_2.addWidget(self.label_13, 19, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 5, 0, 1, 1)
        self.advDutyCycleL_SP = QtWidgets.QLabel(self.tab)
        self.advDutyCycleL_SP.setObjectName("advDutyCycleL_SP")
        self.gridLayout_2.addWidget(self.advDutyCycleL_SP, 18, 1, 1, 1)
        self.advDtL_SP = QtWidgets.QLabel(self.tab)
        self.advDtL_SP.setObjectName("advDtL_SP")
        self.gridLayout_2.addWidget(self.advDtL_SP, 15, 1, 1, 1)
        self.advBottomLayerPeriodL_SP = QtWidgets.QLabel(self.tab)
        self.advBottomLayerPeriodL_SP.setObjectName("advBottomLayerPeriodL_SP")
        self.gridLayout_2.addWidget(self.advBottomLayerPeriodL_SP, 19, 1, 1, 1)
        self.advBinXSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        self.advBinXSB_SP.setDecimals(0)
        self.advBinXSB_SP.setMinimum(1.0)
        self.advBinXSB_SP.setObjectName("advBinXSB_SP")
        self.gridLayout_2.addWidget(self.advBinXSB_SP, 8, 1, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_12.sizePolicy().hasHeightForWidth())
        self.label_12.setSizePolicy(sizePolicy)
        self.label_12.setObjectName("label_12")
        self.gridLayout_2.addWidget(self.label_12, 17, 0, 1, 1)
        self.advRampTimeL_SP = QtWidgets.QLabel(self.tab)
        self.advRampTimeL_SP.setObjectName("advRampTimeL_SP")
        self.gridLayout_2.addWidget(self.advRampTimeL_SP, 17, 1, 1, 1)
        self.advFirstLayerPeriodL_SP = QtWidgets.QLabel(self.tab)
        self.advFirstLayerPeriodL_SP.setObjectName("advFirstLayerPeriodL_SP")
        self.gridLayout_2.addWidget(self.advFirstLayerPeriodL_SP, 20, 1, 1, 1)
        self.advBinYSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        self.advBinYSB_SP.setDecimals(0)
        self.advBinYSB_SP.setMinimum(1.0)
        self.advBinYSB_SP.setMaximum(100.0)
        self.advBinYSB_SP.setObjectName("advBinYSB_SP")
        self.gridLayout_2.addWidget(self.advBinYSB_SP, 9, 1, 1, 1)
        self.label_17 = QtWidgets.QLabel(self.tab)
        self.label_17.setObjectName("label_17")
        self.gridLayout_2.addWidget(self.label_17, 12, 0, 1, 1)
        self.label_14 = QtWidgets.QLabel(self.tab)
        self.label_14.setObjectName("label_14")
        self.gridLayout_2.addWidget(self.label_14, 20, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 2, 0, 1, 1)
        self.advPixelSaturationLevelL_SP = QtWidgets.QLabel(self.tab)
        self.advPixelSaturationLevelL_SP.setObjectName("advPixelSaturationLevelL_SP")
        self.gridLayout_2.addWidget(self.advPixelSaturationLevelL_SP, 13, 1, 1, 1)
        self.advNumMeasuresSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        self.advNumMeasuresSB_SP.setDecimals(0)
        self.advNumMeasuresSB_SP.setMinimum(1.0)
        self.advNumMeasuresSB_SP.setMaximum(1000.0)
        self.advNumMeasuresSB_SP.setObjectName("advNumMeasuresSB_SP")
        self.gridLayout_2.addWidget(self.advNumMeasuresSB_SP, 6, 1, 1, 1)
        self.label_19 = QtWidgets.QLabel(self.tab)
        self.label_19.setObjectName("label_19")
        self.gridLayout_2.addWidget(self.label_19, 14, 0, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy)
        self.label_9.setObjectName("label_9")
        self.gridLayout_2.addWidget(self.label_9, 7, 0, 1, 1)
        self.label_32 = QtWidgets.QLabel(self.tab)
        self.label_32.setObjectName("label_32")
        self.gridLayout_2.addWidget(self.label_32, 11, 0, 1, 1)
        self.advFrameRateL_SP = QtWidgets.QLabel(self.tab)
        self.advFrameRateL_SP.setObjectName("advFrameRateL_SP")
        self.gridLayout_2.addWidget(self.advFrameRateL_SP, 16, 1, 1, 1)
        self.advHSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        self.advHSB_SP.setDecimals(0)
        self.advHSB_SP.setMinimum(1.0)
        self.advHSB_SP.setMaximum(2048.0)
        self.advHSB_SP.setProperty("value", 2048.0)
        self.advHSB_SP.setObjectName("advHSB_SP")
        self.gridLayout_2.addWidget(self.advHSB_SP, 5, 1, 1, 1)
        self.label_16 = QtWidgets.QLabel(self.tab)
        self.label_16.setObjectName("label_16")
        self.gridLayout_2.addWidget(self.label_16, 10, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 3, 0, 1, 1)
        self.advYSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        self.advYSB_SP.setDecimals(0)
        self.advYSB_SP.setMaximum(2047.0)
        self.advYSB_SP.setSingleStep(1.0)
        self.advYSB_SP.setObjectName("advYSB_SP")
        self.gridLayout_2.addWidget(self.advYSB_SP, 4, 1, 1, 1)
        self.advCameraModeCB_SP = QtWidgets.QComboBox(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.advCameraModeCB_SP.sizePolicy().hasHeightForWidth())
        self.advCameraModeCB_SP.setSizePolicy(sizePolicy)
        self.advCameraModeCB_SP.setObjectName("advCameraModeCB_SP")
        self.advCameraModeCB_SP.addItem("")
        self.advCameraModeCB_SP.addItem("")
        self.advCameraModeCB_SP.addItem("")
        self.advCameraModeCB_SP.addItem("")
        self.advCameraModeCB_SP.addItem("")
        self.advCameraModeCB_SP.addItem("")
        self.gridLayout_2.addWidget(self.advCameraModeCB_SP, 2, 1, 1, 1)
        self.label_22 = QtWidgets.QLabel(self.tab)
        self.label_22.setObjectName("label_22")
        self.gridLayout_2.addWidget(self.label_22, 13, 0, 1, 1)
        self.label_21 = QtWidgets.QLabel(self.tab)
        self.label_21.setObjectName("label_21")
        self.gridLayout_2.addWidget(self.label_21, 16, 0, 1, 1)
        self.advCameraModeL_SP = QtWidgets.QLabel(self.tab)
        self.advCameraModeL_SP.setText("")
        self.advCameraModeL_SP.setObjectName("advCameraModeL_SP")
        self.gridLayout_2.addWidget(self.advCameraModeL_SP, 11, 1, 1, 1)
        self.advMaxPixelExposureL_SP = QtWidgets.QLabel(self.tab)
        self.advMaxPixelExposureL_SP.setObjectName("advMaxPixelExposureL_SP")
        self.gridLayout_2.addWidget(self.advMaxPixelExposureL_SP, 12, 1, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_11.sizePolicy().hasHeightForWidth())
        self.label_11.setSizePolicy(sizePolicy)
        self.label_11.setObjectName("label_11")
        self.gridLayout_2.addWidget(self.label_11, 9, 0, 1, 1)
        self.advNdrL_SP = QtWidgets.QLabel(self.tab)
        self.advNdrL_SP.setObjectName("advNdrL_SP")
        self.gridLayout_2.addWidget(self.advNdrL_SP, 14, 1, 1, 1)
        self.label_20 = QtWidgets.QLabel(self.tab)
        self.label_20.setObjectName("label_20")
        self.gridLayout_2.addWidget(self.label_20, 15, 0, 1, 1)
        self.label_18 = QtWidgets.QLabel(self.tab)
        self.label_18.setObjectName("label_18")
        self.gridLayout_2.addWidget(self.label_18, 18, 0, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy)
        self.label_8.setObjectName("label_8")
        self.gridLayout_2.addWidget(self.label_8, 6, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 4, 0, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_10.sizePolicy().hasHeightForWidth())
        self.label_10.setSizePolicy(sizePolicy)
        self.label_10.setObjectName("label_10")
        self.gridLayout_2.addWidget(self.label_10, 8, 0, 1, 1)
        self.label_15 = QtWidgets.QLabel(self.tab)
        self.label_15.setObjectName("label_15")
        self.gridLayout_2.addWidget(self.label_15, 21, 0, 1, 1)
        self.advSecondLayerPeriodL_SP = QtWidgets.QLabel(self.tab)
        self.advSecondLayerPeriodL_SP.setObjectName("advSecondLayerPeriodL_SP")
        self.gridLayout_2.addWidget(self.advSecondLayerPeriodL_SP, 21, 1, 1, 1)
        self.advH2rgReadNoiseSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        self.advH2rgReadNoiseSB_SP.setMinimum(0.1)
        self.advH2rgReadNoiseSB_SP.setMaximum(200.0)
        self.advH2rgReadNoiseSB_SP.setObjectName("advH2rgReadNoiseSB_SP")
        self.gridLayout_2.addWidget(self.advH2rgReadNoiseSB_SP, 0, 1, 1, 1)
        self.label_24 = QtWidgets.QLabel(self.tab)
        self.label_24.setObjectName("label_24")
        self.gridLayout_2.addWidget(self.label_24, 1, 0, 1, 1)
        self.advFullWellSB_SP = QtWidgets.QDoubleSpinBox(self.tab)
        self.advFullWellSB_SP.setDecimals(0)
        self.advFullWellSB_SP.setMinimum(1.0)
        self.advFullWellSB_SP.setMaximum(1000000.0)
        self.advFullWellSB_SP.setObjectName("advFullWellSB_SP")
        self.gridLayout_2.addWidget(self.advFullWellSB_SP, 1, 1, 1, 1)
        self.verticalLayout_4.addLayout(self.gridLayout_2)
        self.advSpReset = QtWidgets.QPushButton(self.tab)
        self.advSpReset.setObjectName("advSpReset")
        self.verticalLayout_4.addWidget(self.advSpReset)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab_2)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label_87 = QtWidgets.QLabel(self.tab_2)
        self.label_87.setObjectName("label_87")
        self.gridLayout.addWidget(self.label_87, 18, 0, 1, 1)
        self.label_65 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_65.sizePolicy().hasHeightForWidth())
        self.label_65.setSizePolicy(sizePolicy)
        self.label_65.setObjectName("label_65")
        self.gridLayout.addWidget(self.label_65, 2, 0, 1, 1)
        self.advCameraModeCB_CI = QtWidgets.QComboBox(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.advCameraModeCB_CI.sizePolicy().hasHeightForWidth())
        self.advCameraModeCB_CI.setSizePolicy(sizePolicy)
        self.advCameraModeCB_CI.setObjectName("advCameraModeCB_CI")
        self.advCameraModeCB_CI.addItem("")
        self.advCameraModeCB_CI.addItem("")
        self.advCameraModeCB_CI.addItem("")
        self.advCameraModeCB_CI.addItem("")
        self.advCameraModeCB_CI.addItem("")
        self.advCameraModeCB_CI.addItem("")
        self.gridLayout.addWidget(self.advCameraModeCB_CI, 2, 1, 1, 1)
        self.label_67 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_67.sizePolicy().hasHeightForWidth())
        self.label_67.setSizePolicy(sizePolicy)
        self.label_67.setObjectName("label_67")
        self.gridLayout.addWidget(self.label_67, 3, 0, 1, 1)
        self.advExposureSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.advExposureSB_CI.sizePolicy().hasHeightForWidth())
        self.advExposureSB_CI.setSizePolicy(sizePolicy)
        self.advExposureSB_CI.setDecimals(1)
        self.advExposureSB_CI.setMaximum(1000000.0)
        self.advExposureSB_CI.setSingleStep(1.0)
        self.advExposureSB_CI.setProperty("value", 1000.0)
        self.advExposureSB_CI.setObjectName("advExposureSB_CI")
        self.gridLayout.addWidget(self.advExposureSB_CI, 3, 1, 1, 1)
        self.label_75 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_75.sizePolicy().hasHeightForWidth())
        self.label_75.setSizePolicy(sizePolicy)
        self.label_75.setObjectName("label_75")
        self.gridLayout.addWidget(self.label_75, 4, 0, 1, 1)
        self.advYSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        self.advYSB_CI.setDecimals(0)
        self.advYSB_CI.setMaximum(2047.0)
        self.advYSB_CI.setSingleStep(1.0)
        self.advYSB_CI.setObjectName("advYSB_CI")
        self.gridLayout.addWidget(self.advYSB_CI, 4, 1, 1, 1)
        self.label_73 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_73.sizePolicy().hasHeightForWidth())
        self.label_73.setSizePolicy(sizePolicy)
        self.label_73.setObjectName("label_73")
        self.gridLayout.addWidget(self.label_73, 5, 0, 1, 1)
        self.advHSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        self.advHSB_CI.setDecimals(0)
        self.advHSB_CI.setMinimum(1.0)
        self.advHSB_CI.setMaximum(2048.0)
        self.advHSB_CI.setProperty("value", 2048.0)
        self.advHSB_CI.setObjectName("advHSB_CI")
        self.gridLayout.addWidget(self.advHSB_CI, 5, 1, 1, 1)
        self.label_71 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_71.sizePolicy().hasHeightForWidth())
        self.label_71.setSizePolicy(sizePolicy)
        self.label_71.setObjectName("label_71")
        self.gridLayout.addWidget(self.label_71, 6, 0, 1, 1)
        self.advNumMeasuresSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        self.advNumMeasuresSB_CI.setDecimals(0)
        self.advNumMeasuresSB_CI.setMinimum(1.0)
        self.advNumMeasuresSB_CI.setMaximum(1000.0)
        self.advNumMeasuresSB_CI.setObjectName("advNumMeasuresSB_CI")
        self.gridLayout.addWidget(self.advNumMeasuresSB_CI, 6, 1, 1, 1)
        self.label_83 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_83.sizePolicy().hasHeightForWidth())
        self.label_83.setSizePolicy(sizePolicy)
        self.label_83.setObjectName("label_83")
        self.gridLayout.addWidget(self.label_83, 7, 0, 1, 1)
        self.advCoaddSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        self.advCoaddSB_CI.setDecimals(0)
        self.advCoaddSB_CI.setMinimum(1.0)
        self.advCoaddSB_CI.setMaximum(1000.0)
        self.advCoaddSB_CI.setObjectName("advCoaddSB_CI")
        self.gridLayout.addWidget(self.advCoaddSB_CI, 7, 1, 1, 1)
        self.label_78 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_78.sizePolicy().hasHeightForWidth())
        self.label_78.setSizePolicy(sizePolicy)
        self.label_78.setObjectName("label_78")
        self.gridLayout.addWidget(self.label_78, 8, 0, 1, 1)
        self.advBinXSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        self.advBinXSB_CI.setDecimals(0)
        self.advBinXSB_CI.setMinimum(1.0)
        self.advBinXSB_CI.setObjectName("advBinXSB_CI")
        self.gridLayout.addWidget(self.advBinXSB_CI, 8, 1, 1, 1)
        self.label_66 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_66.sizePolicy().hasHeightForWidth())
        self.label_66.setSizePolicy(sizePolicy)
        self.label_66.setObjectName("label_66")
        self.gridLayout.addWidget(self.label_66, 9, 0, 1, 1)
        self.advBinYSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        self.advBinYSB_CI.setDecimals(0)
        self.advBinYSB_CI.setMinimum(1.0)
        self.advBinYSB_CI.setMaximum(100.0)
        self.advBinYSB_CI.setObjectName("advBinYSB_CI")
        self.gridLayout.addWidget(self.advBinYSB_CI, 9, 1, 1, 1)
        self.label_84 = QtWidgets.QLabel(self.tab_2)
        self.label_84.setObjectName("label_84")
        self.gridLayout.addWidget(self.label_84, 10, 0, 1, 1)
        self.advModPeriodL_CI = QtWidgets.QLabel(self.tab_2)
        self.advModPeriodL_CI.setObjectName("advModPeriodL_CI")
        self.gridLayout.addWidget(self.advModPeriodL_CI, 10, 1, 1, 1)
        self.label_74 = QtWidgets.QLabel(self.tab_2)
        self.label_74.setObjectName("label_74")
        self.gridLayout.addWidget(self.label_74, 11, 0, 1, 1)
        self.advCameraModeL_CI = QtWidgets.QLabel(self.tab_2)
        self.advCameraModeL_CI.setText("")
        self.advCameraModeL_CI.setObjectName("advCameraModeL_CI")
        self.gridLayout.addWidget(self.advCameraModeL_CI, 11, 1, 1, 1)
        self.label_76 = QtWidgets.QLabel(self.tab_2)
        self.label_76.setObjectName("label_76")
        self.gridLayout.addWidget(self.label_76, 12, 0, 1, 1)
        self.advMaxPixelExposureL_CI = QtWidgets.QLabel(self.tab_2)
        self.advMaxPixelExposureL_CI.setObjectName("advMaxPixelExposureL_CI")
        self.gridLayout.addWidget(self.advMaxPixelExposureL_CI, 12, 1, 1, 1)
        self.label_85 = QtWidgets.QLabel(self.tab_2)
        self.label_85.setObjectName("label_85")
        self.gridLayout.addWidget(self.label_85, 13, 0, 1, 1)
        self.advPixelSaturationLevelL_CI = QtWidgets.QLabel(self.tab_2)
        self.advPixelSaturationLevelL_CI.setObjectName("advPixelSaturationLevelL_CI")
        self.gridLayout.addWidget(self.advPixelSaturationLevelL_CI, 13, 1, 1, 1)
        self.label_86 = QtWidgets.QLabel(self.tab_2)
        self.label_86.setObjectName("label_86")
        self.gridLayout.addWidget(self.label_86, 14, 0, 1, 1)
        self.advNdrL_CI = QtWidgets.QLabel(self.tab_2)
        self.advNdrL_CI.setObjectName("advNdrL_CI")
        self.gridLayout.addWidget(self.advNdrL_CI, 14, 1, 1, 1)
        self.label_82 = QtWidgets.QLabel(self.tab_2)
        self.label_82.setObjectName("label_82")
        self.gridLayout.addWidget(self.label_82, 15, 0, 1, 1)
        self.advDtL_CI = QtWidgets.QLabel(self.tab_2)
        self.advDtL_CI.setObjectName("advDtL_CI")
        self.gridLayout.addWidget(self.advDtL_CI, 15, 1, 1, 1)
        self.label_81 = QtWidgets.QLabel(self.tab_2)
        self.label_81.setObjectName("label_81")
        self.gridLayout.addWidget(self.label_81, 16, 0, 1, 1)
        self.advFrameRateL_CI = QtWidgets.QLabel(self.tab_2)
        self.advFrameRateL_CI.setObjectName("advFrameRateL_CI")
        self.gridLayout.addWidget(self.advFrameRateL_CI, 16, 1, 1, 1)
        self.label_77 = QtWidgets.QLabel(self.tab_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_77.sizePolicy().hasHeightForWidth())
        self.label_77.setSizePolicy(sizePolicy)
        self.label_77.setObjectName("label_77")
        self.gridLayout.addWidget(self.label_77, 17, 0, 1, 1)
        self.advRampTimeL_CI = QtWidgets.QLabel(self.tab_2)
        self.advRampTimeL_CI.setObjectName("advRampTimeL_CI")
        self.gridLayout.addWidget(self.advRampTimeL_CI, 17, 1, 1, 1)
        self.advDutyCycleL_CI = QtWidgets.QLabel(self.tab_2)
        self.advDutyCycleL_CI.setObjectName("advDutyCycleL_CI")
        self.gridLayout.addWidget(self.advDutyCycleL_CI, 18, 1, 1, 1)
        self.label_70 = QtWidgets.QLabel(self.tab_2)
        self.label_70.setObjectName("label_70")
        self.gridLayout.addWidget(self.label_70, 19, 0, 1, 1)
        self.advBottomLayerPeriodL_CI = QtWidgets.QLabel(self.tab_2)
        self.advBottomLayerPeriodL_CI.setObjectName("advBottomLayerPeriodL_CI")
        self.gridLayout.addWidget(self.advBottomLayerPeriodL_CI, 19, 1, 1, 1)
        self.label_68 = QtWidgets.QLabel(self.tab_2)
        self.label_68.setObjectName("label_68")
        self.gridLayout.addWidget(self.label_68, 20, 0, 1, 1)
        self.advFirstLayerPeriodL_CI = QtWidgets.QLabel(self.tab_2)
        self.advFirstLayerPeriodL_CI.setObjectName("advFirstLayerPeriodL_CI")
        self.gridLayout.addWidget(self.advFirstLayerPeriodL_CI, 20, 1, 1, 1)
        self.label_79 = QtWidgets.QLabel(self.tab_2)
        self.label_79.setObjectName("label_79")
        self.gridLayout.addWidget(self.label_79, 21, 0, 1, 1)
        self.advSecondLayerPeriodL_CI = QtWidgets.QLabel(self.tab_2)
        self.advSecondLayerPeriodL_CI.setObjectName("advSecondLayerPeriodL_CI")
        self.gridLayout.addWidget(self.advSecondLayerPeriodL_CI, 21, 1, 1, 1)
        self.advH2rgReadNoiseSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        self.advH2rgReadNoiseSB_CI.setDecimals(1)
        self.advH2rgReadNoiseSB_CI.setMinimum(0.1)
        self.advH2rgReadNoiseSB_CI.setMaximum(200.0)
        self.advH2rgReadNoiseSB_CI.setObjectName("advH2rgReadNoiseSB_CI")
        self.gridLayout.addWidget(self.advH2rgReadNoiseSB_CI, 0, 1, 1, 1)
        self.label_30 = QtWidgets.QLabel(self.tab_2)
        self.label_30.setObjectName("label_30")
        self.gridLayout.addWidget(self.label_30, 0, 0, 1, 1)
        self.label_31 = QtWidgets.QLabel(self.tab_2)
        self.label_31.setObjectName("label_31")
        self.gridLayout.addWidget(self.label_31, 1, 0, 1, 1)
        self.advFullWellSB_CI = QtWidgets.QDoubleSpinBox(self.tab_2)
        self.advFullWellSB_CI.setDecimals(0)
        self.advFullWellSB_CI.setMinimum(1.0)
        self.advFullWellSB_CI.setMaximum(1000000.0)
        self.advFullWellSB_CI.setObjectName("advFullWellSB_CI")
        self.gridLayout.addWidget(self.advFullWellSB_CI, 1, 1, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout)
        self.advCiReset = QtWidgets.QPushButton(self.tab_2)
        self.advCiReset.setObjectName("advCiReset")
        self.verticalLayout_2.addWidget(self.advCiReset)
        self.tabWidget.addTab(self.tab_2, "")
        self.verticalLayout.addWidget(self.tabWidget)
        self.verticalLayout_3.addLayout(self.verticalLayout)
        CameraWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(CameraWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 501, 21))
        self.menubar.setObjectName("menubar")
        CameraWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(CameraWindow)
        self.statusbar.setObjectName("statusbar")
        CameraWindow.setStatusBar(self.statusbar)

        self.retranslateUi(CameraWindow)
        self.tabWidget.setCurrentIndex(0)
        self.advModTypeCB.setCurrentIndex(0)
        self.advCameraModeCB_SP.setCurrentIndex(0)
        self.advCameraModeCB_CI.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(CameraWindow)

    def retranslateUi(self, CameraWindow):
        _translate = QtCore.QCoreApplication.translate
        CameraWindow.setWindowTitle(_translate("CameraWindow", "Advanced Camera Settings"))
        self.label_5.setText(_translate("CameraWindow", "Modulation mode"))
        self.label_7.setText(_translate("CameraWindow", "Modulator State"))
        self.advModTypeCB.setItemText(0, _translate("CameraWindow", "discrete"))
        self.advModTypeCB.setItemText(1, _translate("CameraWindow", "continuous"))
        self.label_6.setText(_translate("CameraWindow", "# Modulation steps"))
        self.advModStateCB.setItemText(0, _translate("CameraWindow", "On"))
        self.advModStateCB.setItemText(1, _translate("CameraWindow", "Off"))
        self.advModStateCB.setItemText(2, _translate("CameraWindow", "Out"))
        self.advModReset.setText(_translate("CameraWindow", "Reset Modulator Settings"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), _translate("CameraWindow", "Modulator"))
        self.advModPeriodL.setText(_translate("CameraWindow", "0"))
        self.label_23.setText(_translate("CameraWindow", "Read Noise [e-]"))
        self.label_13.setText(_translate("CameraWindow", "single slit position [s]"))
        self.label_4.setText(_translate("CameraWindow", "# of rows"))
        self.advDutyCycleL_SP.setText(_translate("CameraWindow", "0"))
        self.advDtL_SP.setText(_translate("CameraWindow", "0"))
        self.advBottomLayerPeriodL_SP.setText(_translate("CameraWindow", "0"))
        self.label_12.setText(_translate("CameraWindow", "ramp Time [s]"))
        self.advRampTimeL_SP.setText(_translate("CameraWindow", "0"))
        self.advFirstLayerPeriodL_SP.setText(_translate("CameraWindow", "0"))
        self.label_17.setText(_translate("CameraWindow", "max pixel exposure [s]"))
        self.label_14.setText(_translate("CameraWindow", "single horizontal scan [s]"))
        self.label_3.setText(_translate("CameraWindow", "Camera Mode"))
        self.advPixelSaturationLevelL_SP.setText(_translate("CameraWindow", "0"))
        self.label_19.setText(_translate("CameraWindow", "# NDR"))
        self.label_9.setText(_translate("CameraWindow", "# co-adds"))
        self.label_32.setText(_translate("CameraWindow", "camera mode"))
        self.advFrameRateL_SP.setText(_translate("CameraWindow", "0"))
        self.label_16.setText(_translate("CameraWindow", "modulator period [s]"))
        self.label.setText(_translate("CameraWindow", "Exposure time [ms]"))
        self.advCameraModeCB_SP.setItemText(0, _translate("CameraWindow", "automatic"))
        self.advCameraModeCB_SP.setItemText(1, _translate("CameraWindow", "slow"))
        self.advCameraModeCB_SP.setItemText(2, _translate("CameraWindow", "fast"))
        self.advCameraModeCB_SP.setItemText(3, _translate("CameraWindow", "lineByLine"))
        self.advCameraModeCB_SP.setItemText(4, _translate("CameraWindow", "idealH2RG"))
        self.advCameraModeCB_SP.setItemText(5, _translate("CameraWindow", "dreamH2RG"))
        self.label_22.setText(_translate("CameraWindow", "pixel staturation level [%]"))
        self.label_21.setText(_translate("CameraWindow", "frame rate [Hz]"))
        self.advMaxPixelExposureL_SP.setText(_translate("CameraWindow", "0"))
        self.label_11.setText(_translate("CameraWindow", "bin Y"))
        self.advNdrL_SP.setText(_translate("CameraWindow", "0"))
        self.label_20.setText(_translate("CameraWindow", "dt [s]"))
        self.label_18.setText(_translate("CameraWindow", "duty cycle [%]"))
        self.label_8.setText(_translate("CameraWindow", "# measurements"))
        self.label_2.setText(_translate("CameraWindow", "Start row"))
        self.label_10.setText(_translate("CameraWindow", "bin X"))
        self.label_15.setText(_translate("CameraWindow", "full scan [s]"))
        self.advSecondLayerPeriodL_SP.setText(_translate("CameraWindow", "0"))
        self.label_24.setText(_translate("CameraWindow", "full well [e-]"))
        self.advSpReset.setText(_translate("CameraWindow", "Reset Spectrograph Camera"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("CameraWindow", "Spectrograph"))
        self.label_87.setText(_translate("CameraWindow", "duty cycle [%]"))
        self.label_65.setText(_translate("CameraWindow", "Camera Mode"))
        self.advCameraModeCB_CI.setItemText(0, _translate("CameraWindow", "automatic"))
        self.advCameraModeCB_CI.setItemText(1, _translate("CameraWindow", "slow"))
        self.advCameraModeCB_CI.setItemText(2, _translate("CameraWindow", "fast"))
        self.advCameraModeCB_CI.setItemText(3, _translate("CameraWindow", "lineByLine"))
        self.advCameraModeCB_CI.setItemText(4, _translate("CameraWindow", "idealH2RG"))
        self.advCameraModeCB_CI.setItemText(5, _translate("CameraWindow", "dreamH2RG"))
        self.label_67.setText(_translate("CameraWindow", "Exposure time [ms]"))
        self.label_75.setText(_translate("CameraWindow", "Start row"))
        self.label_73.setText(_translate("CameraWindow", "# of rows"))
        self.label_71.setText(_translate("CameraWindow", "# measurements"))
        self.label_83.setText(_translate("CameraWindow", "# co-adds"))
        self.label_78.setText(_translate("CameraWindow", "bin X"))
        self.label_66.setText(_translate("CameraWindow", "bin Y"))
        self.label_84.setText(_translate("CameraWindow", "modulator period [s]"))
        self.advModPeriodL_CI.setText(_translate("CameraWindow", "0"))
        self.label_74.setText(_translate("CameraWindow", "camera mode"))
        self.label_76.setText(_translate("CameraWindow", "max pixel exposure [s]"))
        self.advMaxPixelExposureL_CI.setText(_translate("CameraWindow", "0"))
        self.label_85.setText(_translate("CameraWindow", "pixel staturation level [%]"))
        self.advPixelSaturationLevelL_CI.setText(_translate("CameraWindow", "0"))
        self.label_86.setText(_translate("CameraWindow", "# NDR"))
        self.advNdrL_CI.setText(_translate("CameraWindow", "0"))
        self.label_82.setText(_translate("CameraWindow", "dt [s]"))
        self.advDtL_CI.setText(_translate("CameraWindow", "0"))
        self.label_81.setText(_translate("CameraWindow", "frame rate [Hz]"))
        self.advFrameRateL_CI.setText(_translate("CameraWindow", "0"))
        self.label_77.setText(_translate("CameraWindow", "ramp Time [s]"))
        self.advRampTimeL_CI.setText(_translate("CameraWindow", "0"))
        self.advDutyCycleL_CI.setText(_translate("CameraWindow", "0"))
        self.label_70.setText(_translate("CameraWindow", "single slit position [s]"))
        self.advBottomLayerPeriodL_CI.setText(_translate("CameraWindow", "0"))
        self.label_68.setText(_translate("CameraWindow", "single horizontal scan [s]"))
        self.advFirstLayerPeriodL_CI.setText(_translate("CameraWindow", "0"))
        self.label_79.setText(_translate("CameraWindow", "full scan [s]"))
        self.advSecondLayerPeriodL_CI.setText(_translate("CameraWindow", "0"))
        self.label_30.setText(_translate("CameraWindow", "Read Noise [e-]"))
        self.label_31.setText(_translate("CameraWindow", "full well [e-]"))
        self.advCiReset.setText(_translate("CameraWindow", "Reset Context Imager Camera"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("CameraWindow", "Context Imager"))

