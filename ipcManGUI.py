# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ipcManGUI.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_modelManipulationWindow(object):
    def setupUi(self, modelManipulationWindow):
        modelManipulationWindow.setObjectName("modelManipulationWindow")
        modelManipulationWindow.resize(438, 238)
        font = QtGui.QFont()
        font.setPointSize(10)
        modelManipulationWindow.setFont(font)
        self.centralwidget = QtWidgets.QWidget(modelManipulationWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.label_2)
        self.manCoronalLineSelectorCB = QtWidgets.QComboBox(self.centralwidget)
        self.manCoronalLineSelectorCB.setObjectName("manCoronalLineSelectorCB")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.manCoronalLineSelectorCB)
        self.manCoronalLineScaleSB = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.manCoronalLineScaleSB.setObjectName("manCoronalLineScaleSB")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.manCoronalLineScaleSB)
        self.verticalLayout.addLayout(self.formLayout)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        modelManipulationWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(modelManipulationWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 438, 21))
        self.menubar.setObjectName("menubar")
        modelManipulationWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(modelManipulationWindow)
        self.statusbar.setObjectName("statusbar")
        modelManipulationWindow.setStatusBar(self.statusbar)

        self.retranslateUi(modelManipulationWindow)
        QtCore.QMetaObject.connectSlotsByName(modelManipulationWindow)

    def retranslateUi(self, modelManipulationWindow):
        _translate = QtCore.QCoreApplication.translate
        modelManipulationWindow.setWindowTitle(_translate("modelManipulationWindow", "Model Manipulation"))
        self.label.setText(_translate("modelManipulationWindow", "Line"))
        self.label_2.setText(_translate("modelManipulationWindow", "relative amplitude"))
        self.label_3.setText(_translate("modelManipulationWindow", "Instructions:\n"
"Select the line you want to scale. All other lines will be scaled by the same factor.\n"
"The relative amplitude defines the emission line amplitude above the background, i.e. in the spectral plots the line peak will be at the specified value plus the background. A value of zero means no scaling is done and the default flux model value is used."))

