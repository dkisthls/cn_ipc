# -*- coding: utf-8 -*-
"""
Created on Tue May 29 10:48:19 2018

@author: cindy
"""
import names as names
FOV = ['2.8', '5.0']
OCCULTER = ['On', 'Off']
ND_FILTER = ['Open', '2ND', '3ND', '4ND', '5ND']
MOD_TYPE = [names.DISCRETE, names.CONTINIOUS]
MOD_POS = ['0', '1', '45', '90', '180', '225', '270']
MOD_STATE = [names.ON, names.OFF, names.OUT]
SP_SLIT = ["120\" long - 0.15\" wide", "233\" long - 0.5\" wide"]
PICKOFF = [names.PICKOFF_SPLITTER, names.PICKOFF_OPEN, names.PICKOFF_MIRROR]

OCCULTER_FOV_28 = [1.054, 1.064]
OCCULTER_FOV_5 = [1.080, 1.091]
DEFAULT_R_28 = 1.059
DEFAULT_R_5 = 1.086

DEFAULT_R = 1.5
