#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 08:52:55 2018

@author: andreef
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 08:45:04 2018

@author: andreef
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import platform
import scipy.constants as pc

class refractiveIndex():
  def __init__(self,lam,temp=None):
      self.lam = lam
      self.temp = None

  def sellmayer(self,a1=None,a2=None,a3=None,l1=None,l2=None,l3=None):
    self.type = "sellmayer"
    self.a1 = a1
    self.a2 = a2
    self.a3 = a3
    self.l1 = l1
    self.l2 = l2
    self.l3 = l3
    self.n = np.sqrt((self.a1*np.square(self.lam))/
                     (np.square(self.lam)-self.l1**2.)+
                     (self.a2*np.square(self.lam))/
                     (np.square(self.lam)-self.l2**2.)+
                     (self.a3*np.square(self.lam))/
                     (np.square(self.lam)-self.l3**2.)+1.)
    
class caf2RefractiveIndex(refractiveIndex):
  def __init__(self,lam, temp=None):
    refractiveIndex.__init__(self,lam,temp)
    # Data from I. H. Malitson. A Redetermination of Some Optical Properties of
    # Calcium Fluoride, Appl. Opt. 2, 1103-1107 (1963)
    # condition: 24 degree Celcius
    # sellmayer coefficients
    a1 = 0.5675888
    a2 = 0.4710914
    a3 = 3.8484723
    l1 = 0.050263605
    l2 = 0.1003909
    l3 = 34.649040
    self.temp = "24 degree C"
    self.sellmayer(a1,a2,a3,l1,l2,l3)

       

    
#lam = np.linspace(0.5,5.,1000.)
#a=caf2RefractiveIndex(lam)
#plt.plot(lam,a.n)