#THIS IS THE CUSTOM WIDGET
# Python Qt4 bindings for GUI objects
from PyQt5 import QtWidgets
# import the Qt4Agg FigureCanvas object, that binds Figure to
# Qt4Agg backend. It also inherits from QWidget
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
# Matplotlib Figure object
from matplotlib.figure import Figure
from matplotlib import rcParams

class MplCanvas(FigureCanvas):
    #Class to represent the FigureCanvas widget
    def __init__(self):

 # setup Matplotlib Figure and Axis
        self.fig = Figure(figsize =(5.41,3.21),dpi=100,tight_layout=True)
        
        rcParams.update({'figure.autolayout': True})
        self.ax = self.fig.add_subplot(111)
        self.ax1 = self.ax.twinx()#self.fig.add_subplot(111)
#        self.ax2 = self.fig.add_subplot()
#        self.ax3 = self.fig.add_subplot()

 # initialization of the canvas
        FigureCanvas.__init__(self, self.fig)
       # FigureCanvas.setGeometry(self,0,0,500,500)
 # we define the widget as expandable
    '''
        FigureCanvas.setSizePolicy(self,
                               QtWidgets.QSizePolicy.Expanding,
                               QtWidgets.QSizePolicy.Expanding)
    '''
 # notify the system of updated policy
        
class MplWidget(QtWidgets.QWidget):
     #Widget defined in Qt Designer"""

    def __init__(self, parent = None):
     
 # initialization of Qt MainWindow widget
         QtWidgets.QWidget.__init__(self, parent)
 # set the canvas to the Matplotlib widget
         self.canvas = MplCanvas()

 # create a vertical box layout
         self.vbl = QtWidgets.QVBoxLayout()
 # add mpl widget to vertical box
         self.vbl.addWidget(self.canvas)
 # set the layout to th vertical box
         self.setLayout(self.vbl)