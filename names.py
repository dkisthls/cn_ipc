
# -*- coding: utf-8 -*-
"""
Created on Wed May 23 11:48:01 2018

@author: cindy
"""
FOV = 'GOS FOV [arcmin]'
R = 'Radius [solar radii]'
T = 'T [degrees]'
ROTATION = 'field rotation [degrees]'
OCCULTER = 'Occulter'
MOD_STATE = 'Modulator State'
ON = 'On'
OFF = 'Off'
OUT = 'Out'
MOD_TYPE = 'Modulator Type'
MOD_POS = 'Modulator position'
SKY_BRIGHT = 'Background brightness [millionths of disk]'
ND_FILTER = 'Neutral Density Filter'
PICKOFF_STATE = 'Pickoff stage position'
PICKOFF_OPEN = 'open'
PICKOFF_SPLITTER = 'splitter'
PICKOFF_MIRROR = 'mirror'
SP_SLIT = 'SP slit length and width [arcsec]'
SP_FILTER = 'SP filter'
SP_CENTER_WL = 'SP center wavelength [nm]'
SP_EXP_TIME = 'SP exposure time [ms]'
SP_CAMERA_MODE = 'SP camera mode'
SP_CO_ADD = 'SP number of coadds'
SP_NUM_REPEATS = 'SP number of repeats'
SP_STEP_SIZE = 'SP step size [arcsec]'
SP_NUM_STEPS = 'SP number of steps'
SP_SECS = 'SP duration [HH:MM:SS]'
SP_DV = 'SP data volume [MB]'
SP_BIN_X = 'SP X binning'
SP_BIN_Y = 'SP Y binning'

CI_FILTER = 'CI filter'
CI_CENTER_WL = 'CI center wavelength [nm]'
CI_EXP_TIME = 'CI exposure time [ms]'
CI_CAMERA_MODE = 'CI camera mode'
CI_CO_ADD = 'CI number of coadds'
CI_NUM_POSITIONS = 'CI number of positions'
CI_NUM_REPEATS = 'CI number of repeats'
CI_SECS = 'CI duration [HH:MM:SS]'
CI_DV = 'CI data volume [MB]'
CI_POS_NUMS = 'CI positions values'
SP_CFG_NAME = 'SP config name'
CI_CFG_NAME = 'CI config name'
CI_BIN_X = 'CI X binning'
CI_BIN_Y = 'CI Y binning'
CI_ANALYZER = 'CI use analyzer'

CORONAL_FLUX = "Coronal relative Flux spectrum"
CORONAL_ABSOLUTE_FLUX = "Coronal absolute Flux spectrum"
DISK_FLUX = "Disk Flux spectrum"
DISK_SPECTRUM_1 = "Disk low resolution SP filter & spectrum"
DISK_SPECTRUM_2 = "Disk high resolution SP filter & spectrum"
DISK_SPECTRUM_3 = "Disk high resolution CI filter & spectrum"
CORONAL_SPECTRUM_1 = "Corona low resolution SP filter & spectrum"
CORONAL_SPECTRUM_2 = "Corona high resolution SP filter & spectrum"
CORONAL_SPECTRUM_3 = "Corona high resolution CI filter & spectrum"
LINE_COUNTS = "SP coronal line counts in filter passband"
V_SENSITIVITY = "SP velocity sensitivity for lines in filter passband"
B_SENSITIVITY = "SP magnetic sensitivity  lines in filter passband"

DISCRETE = "Discrete"
CONTINIOUS = "Continuous"
DEFAULT_CONFIG = "Default Configuration"

SP = "SP"
CI = "CI"
SP_CI = "SP&CI"
DUO = "duo"
DELIM = ' - '
SP_METRICS = [R,
              SKY_BRIGHT,
              ND_FILTER,
              SP_CENTER_WL,
              SP_EXP_TIME,
              SP_CO_ADD,
              SP_SECS,
              SP_DV
              ]

CI_METRICS = [R,
              SKY_BRIGHT,
              ND_FILTER,
              CI_EXP_TIME,
              CI_CO_ADD,
              CI_SECS,
              CI_DV]


SUNMAP_PARAMS = ['tSB', 'rSB', 'slitRotationSB', 'fovCB',
                 'occulterCB', 'stepSizeSB_SP', 'numStepsSB_SP', 'numPosCB_CI']


PARAMS_CHANGED_SP = "Saturation Check changed SP exposure time and coadds"
PARAMS_CHANGED_CI = "Saturation Check changed CI exposure time and coadds"

CI_SAT_LEVEL = "CI Saturation Level"
SP_SAT_LEVEL = "SP Saturation Level"

AUTO_SAT_CHECK_SP_CANCELLED = "SP camera has reached minimum exposure time. Please insert ND filter. Automatic Saturation Check has been disabled."
AUTO_SAT_CHECK_CI_CANCELLED = "CI camera has reached minimum exposure time. Please insert ND filter. Automatic Saturation Check has been disabled."

AUTO_SAT_CHECK_CI = "Automatic Saturation Check CI"
AUTO_SAT_CHECK_SP = "Automatic Saturation Check SP"

INPUT_MODEL = "Input Models"
FLUX_MODEL = "Flux Model"
DEL_ZANNA_QS = "QS"
DEL_ZANNA_QR = "QR"
DEL_ZANNA_AR = "AR"
CUSTOM = "CUSTOM"
DEL_ZANNA_QS_LABEL = "DelZanna QS Model"
DEL_ZANNA_QR_LABEL = "DelZanna QR Model"
DEL_ZANNA_AR_LABEL = "DelZanna AR Model"
CUSTOM_LABEL = "Custom Model"
CUSTOM_MODEL_FILENAME = "Custom Model Filename"

USE_BOTH_MESSAGE = "Using Spectrograph and Context Imager simultanously restricts the instrument configurations. Please check configurations before proceeding."
PROG_WL_ERROR = "Instrument program only supports a single center wavelength for the spectrograph."