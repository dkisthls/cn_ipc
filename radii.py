"""
Calculating the min and max radii for the SP and
CI field of view
"""
import numpy as np

def radius_rectangle(x, y, slitrotation, slitlength, nr_step, step_size, rsun):
    """ determine the extreme radii of a rectangel described by a
    center point, rotation, height, #steps, step size
    relative to a disk of given radius

    """
    # x, y and rsun are in arcmin
    # step size and slit_length is submitted in arcsec
    step_size = step_size/60.
    slitlength = slitlength/60.
    #print("step_size, rsun, slit_length",step_size,rsun, slitlength)
    top_center = np.zeros(2)
    bot_center = np.zeros(2)
    ul = np.zeros(2)
    ll = np.zeros(2)
    ur = np.zeros(2)
    lr = np.zeros(2)

    beta = np.deg2rad(slitrotation)
    # print('beta', beta)
    st = nr_step*step_size/2.

    dx = slitlength/2.*np.sin(beta)
    dy = slitlength/2.*np.cos(beta)
    # print('x,dx', x, dx)
    top_center[0] = x - dx
    top_center[1] = y + dy
    bot_center[0] = x + dx
    bot_center[1] = y - dy

    ul[0] = top_center[0] - st*np.cos(beta)
    ul[1] = top_center[1] - st*np.sin(beta)
    ll[0] = bot_center[0] - st*np.cos(beta)
    ll[1] = bot_center[1] - st*np.sin(beta)

    ur[0] = top_center[0] + st*np.cos(beta)
    ur[1] = top_center[1] + st*np.sin(beta)
    lr[0] = bot_center[0] + st*np.cos(beta)
    lr[1] = bot_center[1] + st*np.sin(beta)


    nr = 1001
    r = np.zeros(4*nr)

    if (ul[0]-ll[0]) == 0:
        left_x = np.linspace(ul[0], ll[0], num=nr)
        left_y = np.linspace(ul[1], ll[1], num=nr)
        bot_x = np.linspace(ll[0], lr[0], num=nr)
        right_x = np.linspace(lr[0], ur[0], num=nr)
        right_y = np.linspace(lr[1], ur[1], num=nr)
        top_x = np.linspace(ur[0], ul[0], num=nr)

        for i in range(nr):
            y = left_y[i]
            r[i] = np.sqrt(left_x[i]**2 + y**2)
            y = ll[1]
            r[i+nr] = np.sqrt(bot_x[i]**2 + y**2)
            y = right_y[i]
            r[i+2*nr] = np.sqrt(right_x[i]**2 + y**2)
            y = ur[1]
            r[i+3*nr] = np.sqrt(top_x[i]**2 + y**2)

    elif (ll[0]-lr[0]) == 0:
        left_x = np.linspace(ul[0], ll[0], num=nr)
        bot_x = np.linspace(ll[0], lr[0], num=nr)
        bot_y = np.linspace(ll[1], lr[1], num=nr)
        right_x = np.linspace(lr[0], ur[0], num=nr)
        top_x = np.linspace(ur[0], ul[0], num=nr)
        top_y = np.linspace(ur[1], ul[1], num=nr)

        for i in range(nr):
            y = ul[1]
            r[i] = np.sqrt(left_x[i]**2 + y**2)

            y = bot_y[i]
            r[i+nr] = np.sqrt(bot_x[i]**2 + y**2)

            y = lr[1]
            r[i+2*nr] = np.sqrt(right_x[i]**2 + y**2)

            y = top_y[i]
            r[i+3*nr] = np.sqrt(top_x[i]**2 + y**2)

    else:
        left_m = (ul[1]-ll[1])/(ul[0]-ll[0])
        right_m = (lr[1]-ur[1])/(lr[0]-ur[0])
        bot_m = (ll[1]-lr[1])/(ll[0]-lr[0])
        top_m = (ur[1]-ul[1])/(ur[0]-ul[0])

        left_x = np.linspace(ul[0], ll[0], num=nr)
        bot_x = np.linspace(ll[0], lr[0], num=nr)
        right_x = np.linspace(lr[0], ur[0], num=nr)
        top_x = np.linspace(ur[0], ul[0], num=nr)

        for i in range(nr):

            y = left_m*(left_x[i]-ul[0])+ul[1]
            r[i] = np.sqrt(left_x[i]**2 + y**2)

            y = bot_m*(bot_x[i]-ll[0])+ll[1]
            r[i+nr] = np.sqrt(bot_x[i]**2 + y**2)

            y = right_m*(right_x[i]-lr[0])+lr[1]
            r[i+2*nr] = np.sqrt(right_x[i]**2 + y**2)

            y = top_m*(top_x[i]-ur[0])+ur[1]
            r[i+3*nr] = np.sqrt(top_x[i]**2 + y**2)

    rmin = np.nanmin(r)/rsun
    rmax = np.nanmax(r)/rsun

    return rmin, rmax

def radiusCI(centerPositions, fieldrotation, fieldlength, nr_step, step_size, rsun):
    """ calculate min and max radius for a given CI field scanning

    """
    # find minimum of all center positions
    xmax = None
    ymax = None
    xmin = None
    ymin = None
    rmin = 1e6
    rmax = -1e6
    for i in range(len(centerPositions[0, :])):
        r = np.sqrt(centerPositions[0, i]**2. + centerPositions[1, i]**2.)
        if r < rmin:
            # print('newmin i, r', i, r)
            rmin = r
            xmin = centerPositions[0, i]
            ymin = centerPositions[1, i]
        if r > rmax:
            # print('newmax i, r', i, r)
            rmax = r
            xmax = centerPositions[0, i]
            ymax = centerPositions[1, i]
        # verification only
    rmin, _ = radius_rectangle(xmin, ymin, fieldrotation, fieldlength, nr_step, step_size, rsun)
    # print('done with rmin')
    _, rmax = radius_rectangle(xmax, ymax, fieldrotation, fieldlength, nr_step, step_size, rsun)
    return rmin, rmax

# test = np.array([[0.33,0.33,0.33],[0.1,0,-0.1]])
# a, b = radiusCI(test,0,1.2,2,1.2,1919/120)
# print(a,b)
