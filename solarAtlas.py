#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 09:25:18 2018

@author: andreef
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
from scipy.interpolate import interp1d
import platform
import sys

import commonFunctions as cf

class solarAtlas():
    def __init__(self, lwl, uwl, baseDir, atlas, verbose=False):
        # object has a lower and upper wavelength limit
        self.lwl = lwl
        self.uwl = uwl
        self.baseDir = baseDir
        self.atlas = atlas
        self.outliersRemoved = False
        self.verbose = verbose
        self.isInterpolated = False

    def convolve(self, kernel):
        # convolution should be done space with equal spacing
        self.kernel = kernel
        self.notconvolvedData = self.data
        tempData = self.data
        self.data = np.zeros((np.size(self.wl), np.size(self.components)))
        for i in range(0, np.size(self.components)):
            self.data[:, i] = (np.convolve(tempData[:, i], self.kernel,
                                           mode="same") / sum(self.kernel))

    def interpolate(self, step):
        # interpolates to chosen bin size and also removes NaNs
        self.isInterpolated = True
        self.notInterpolatedData = self.data
        self.notInterpolatedWl = self.wl
        self.notInterpolatedWn = self.wn
        self.interpolationWlStep = step
        # np.interp wants monotonically increasing data
        wl = self.wl
        newwl = np.arange(wl[0], wl[-1], step)
        self.wl = newwl
        self.wn = 10000./self.wl
        tempData = self.data
        self.data = np.zeros((np.size(self.wl), np.size(self.components)))
        plt.plot(wl, tempData[:, 0])
    
        for i in range(0, np.size(self.components)):
            #remove NaNs first
            temp = tempData[:, i]
            notNan = np.logical_not(np.isnan(temp))
            temp = np.interp(wl, wl[notNan], temp[notNan])
            # interpolate to newwl
            self.data[:, i] = np.interp(newwl, wl, temp)

    def read(self):
        if self.atlas is "acefts":
            # atlas only good from 2um upwards
            self.isWn = True
            self.components = ["solar","solar"]
            tempData = np.genfromtxt(self.baseDir+self.atlas+".txt", delimiter=",")
            if self.isWn is True:
                data = np.flipud(tempData)
            self.wn = data[:,0]
            self.wl = self.wl = cf.wnum2ang(self.wn)/10000.
            self.data = data[:, 1]
            #self.data = np.expand_dims(tempData[:,1], axis=1)
            # only region of interest
            ind = np.where((self.wl >= self.lwl) & (self.wl <= self.uwl))
            self.wn = self.wn[ind]
            self.wl = self.wl[ind]
            self.data = np.concatenate((np.expand_dims(self.data[ind], axis=1),
                                        np.expand_dims(self.data[ind], axis=1)), axis=1)

        else:
        # NSO atlases are different
            if self.atlas == "photatl":
                # This version of ``An Atlas of the Solar Spectrum in the Infrared from
                # 1850 to 9000 cm-1 (1.1 to 5.4 microns)'' (N.S.O. Technical Report #91-001,
                # July 1991) by W. Livingston and L. Wallace is meant to be used in conjunction
                # with the original hard-copy version, and the caveats in the text of that
                # version apply here equally.
                #
                # The three spectra in each page of the original are combined into one ascii file
                # covering the main 25 cm-1 of the page plus 2 cm-1 of overlap on each end.
                # Each file is given the name ``wnxxxx'' where xxxx is the wavenumber of the
                # beginning of the main 25 cm-1 of the page.  Thus, wn4525 contains the material
                # from 4525 to 4550 cm-1 plus 2 cm-1 of overlap on each end.  The first column
                # contains the wavenumber while the second, third and fourth contain the solar
                # component, the atmospheric component and the total, respectively.  The point
                # density of the original FTS spectra has been maintained with the result
                # that each file contains 3061 points.
                #
                # The parameter ``dg'', which controlled the extent of the gaps in the
                # original version of the solar component due to strong atmospheric absorption,
                # replaces the last point in the ``total''.  The user is warned that in this
                # version there are no gaps.  Instead, the regions where the solar component is
                # undetermined are replaced by linear interpolations between the last ``good''
                # points on each side of the undetermined region.  In order to identify these
                # interpolated regions the user must either refer to the plots in the
                # original version or use the parameter ``dg'' in the same way as the authors.
                #
                # Finally, the intensities of the spectra in each file have been adjusted
                # to predetermined values.  One result is that the intensities are not
                # continuous from file to file.  If continuity is required from one file
                # to another, the intensities can be readjusted using the spectra in the
                # overlap region.
                self.isWn = True
                self.filePrefix = "wn"
                self.components = ["solar","atmosphere","combined"]
                self.wnInt = 25
                rows2skip = 0
                filler = 4
                data = np.array([-99.,-99.,-99.,-99.],ndmin=2)
                # convert to wave number
                self.uwn = np.int(np.floor(10000./self.lwl))
                self.lwn = np.int(np.ceil(10000./self.uwl))
                # print(self.uwn, self.lwn)

                # modulo 25 to find nearest NSO FTS photatl file that contains data
                mul, rem = np.divmod(self.uwn, self.wnInt)
                self.upper = (mul+1)*self.wnInt
                mul, rem = np.divmod(self.lwn, self.wnInt)
                self.lower = (mul)*self.wnInt

            if self.atlas == "spot1atl":
                # This version of ``An Atlas of a Dark Sunspot Umbral Spectrum from 1970 to
                # 8640 cm-1 (1.16 to 5.1 microns)'' (N.S.O. Technical Report #92-001, March
                # 1992) by L. Wallace and W. Livingston is meant to be used in conjunction
                # with the original hard-copy version, and the caveats in the text of that
                # version apply here equally.
                #
                # The two spectra on each page of the original are combined into one ascii file
                # covering the main 20 cm-1 of the page plus 1 cm-1 of overlap on each end.
                # Each file is given the name ``wnxxxx'' where xxxx is the wavenumber of the
                # beginning of the main 20 cm-1 of the page.  Thus, wn4080 contains the material
                # from 4080 to 4100 cm-1 plus 1 cm-1 of overlap on each end.  The first column
                # contains the wavenumber while the second and third contain the umbral
                # component and the observed 2.3 air mass spectrum, respectively.
                #
                # The spectra have been processed so that no adjustment has been made in these
                # files to set the continuum levels at approximately unity; the intensity
                # level in each file is continuous with that in the adjacent files.  For
                # convenience however, the first line in each file contains, centered over
                # the second and third columns, approximate continuum levels appropriate to
                # these spectra in frequency range of the file.  The subsequent 2320 lines
                # of each file contain the spectra.  In the place of the gaps in the original
                # version of the umbral component we have inserted "9.999999".

                self.isWn = True
                self.filePrefix = "wn"
                self.components = ["umbral","2.3 airmass"]
                self.wnInt = 20
                rows2skip = 1
                filler = 4
                data = np.array([-99.,-99.,-99.],ndmin=2)
                # convert to wave number
                self.uwn = np.int(np.floor(10000./self.lwl))
                self.lwn = np.int(np.ceil(10000./self.uwl))
                # print(self.uwn, self.lwn)

                # modulo 12 to find nearest NSO FTS spot2atl file that contains data
                mul,rem = np.divmod(self.uwn,self.wnInt)
                self.upper = (mul+1)*self.wnInt
                mul,rem = np.divmod(self.lwn,self.wnInt)
                self.lower = (mul)*self.wnInt

            if self.atlas == "spot2atl":
                # This version of ``An Atlas of the Sunspot Spectrum from 470 to 1233 cm-1
                # (8.1 to 21 microns) and the Photospheric Spectrum from 460 to 630 cm-1
                # (16 to 22 microns)'' (N.S.O. Technical Report #1994-01, May 1994) by
                # L. Wallace, W. Livingston and P. Bernath, is meant to be used in conjunction
                # with the original hard-copy version, and the caveats in the text of that
                # version apply here equally.
                #
                # The first part of the sunspot section of the atlas (from 470 to 630 cm-1)
                # is given in ascii files each covering 12 cm-1 plus 0.2 cm-1 overlap on
                # each end.  Each file contains two spectra, the reference spectrum and
                # the umbral spectrum, and is named ``irspxxxx'' where xxxx is the wavenumber
                # of the beginning of the main 12 cm-1 section in the file.  Thus the file
                # irsp0594 covers the region 594 to 606 cm-1 plus some overlap.  The first
                # column contains the wavenumber, the second has the reference spectrum and
                # the third has the umbral spectrum.  The second part of the sunspot section
                # (720 to 1233 cm-1) is given in files covering 6 cm-1 plus 0.2 cm-1
                # overlap.  In these files the first column gives wavenumber, the second
                # and third give the corrected umbral spectra of 04/07/81 and 03/04/82
                # respectively, the fourth gives the reference spectrum and the fifth gives
                # the observed umbral spectrum of 03/04/82.  These files also have the names
                # ``irspxxxx''.
                #
                # The photospheric section is given in the files ``irphxxxx'' each covering
                # 12 cm-1 plus 0.2 cm-1 overlap.  The first column contains the frequency
                # and the second contains the sum of the eight spectra.
                #
                # All of the spectra in these files have been processed so that no adjustment
                # has been made to set the continuum levels at approximately unity; the intensity
                # level in each file is continuous with that in the adjacent files.  For
                # convenience however, the first line in each file contains, centered over
                # the appropriate columns of spectral data, approximate continuum levels
                # for these spectra in the frequency range of the file.  The occurrence of
                # "-1." in the corrected spectral files represents a failure in the
                # correction process corresponding to gaps in the hard-copy version.
                self.isWn = True
                self.filePrefix = "irsp"
                self.components = ["reference","umbral"]
                self.wnInt = 12
                rows2skip = 1
                filler = 4
                data = np.array([-99.,-99.,-99.],ndmin=2)
                # convert to wave number
                self.uwn = np.int(np.floor(10000./self.lwl))
                self.lwn = np.int(np.ceil(10000./self.uwl))
                # print(self.uwn, self.lwn)

                # modulo 12 to find nearest NSO FTS spot2atl file that contains data
                mul,rem = np.divmod(self.uwn,self.wnInt)
                self.upper = (mul+1)*self.wnInt
                mul,rem = np.divmod(self.lwn,self.wnInt)
                self.lower = (mul)*self.wnInt

            if self.atlas == "niratl":
                # This version of ``An Atlas of the Photospheric Spectrum from 8900 to 13600
                # cm-1 (7350 to 11230A)'' (N.S.O. Technical Report #93-001, April 1993) by
                # L. Wallace, K. Hinkle, and W. Livingston, is meant to be used in conjunction
                # with the original hard-copy version, and the caveats in the text of that
                # version apply here equally.
                #
                # The three spectra on each page of the original are combined into one ascii file
                # covering the main 25 cm-1 of the page plus 2 cm-1 of overlap on each end.
                # Each file is given the name ``phxxxxx'' where xxxxx is the wavenumber of the
                # beginning of the main 25 cm-1 of the page.  Thus, ph10050 contains the material
                # from 10050 to 10075 cm-1 plus 2 cm-1 of overlap on each end.  The first column
                # contains the wavenumber while the second, third and fourth contain the solar
                # component, the atmospheric component and the observed 1.0 air mass spectrum,
                # respectively.
                #
                # The spectra have been processed so that no adjustment has been made in these
                # files to set the continuum levels at approximately unity; the intensity level
                # in each file is continuous with that in the adjacent files.  For convenience,
                # however, the first line in each file contains, centered over the second,
                # third, and fourth columns, approximate continuum levels appropriate to these
                # spectra in the frequency range of the file.  The subsequent 4096 lines of
                # each file contain the spectra.  In the worst places of low signal-to-noise,
                # the regions have been filled with -1.0, but some regions which show gaps
                # in the hard-copy version will show noise here.
                self.isWn = True
                self.filePrefix = "ph"
                self.components = ["solar","atmosphere","combined"]
                self.wnInt = 25
                rows2skip = 1
                filler = 5
                data = np.array([-99.,-99.,-99.,-99.],ndmin=2)
                # convert to wave number
                self.uwn = np.int(np.floor(10000./self.lwl))
                self.lwn = np.int(np.ceil(10000./self.uwl))
                # print(self.uwn, self.lwn)

                # modulo 25 to find nearest NSO FTS niratl file that contains data
                mul,rem = np.divmod(self.uwn,self.wnInt)
                self.upper = (mul+1)*self.wnInt
                mul,rem = np.divmod(self.lwn,self.wnInt)
                self.lower = (mul)*self.wnInt


            # find the right files and put them into the data array
            # ! there are missing files so check
            counter = 0
            for i in range(self.lower, self.upper+self.wnInt, self.wnInt):
                filename = (self.baseDir+self.atlas+os.sep+
                                        self.filePrefix+str(i).zfill(filler))
                if self.verbose:
                    print(filename)
                if os.path.isfile(filename):
                    tempData = np.loadtxt(filename,skiprows=rows2skip)
                    # delete first row that was used to create array
                    if counter == 1:
                        data = np.delete(data,0,0)
                    if counter > 0:
                        # find overlap region only after first file read
                        indStart = np.where(data[:,0]==tempData[0,0])
                        indStart = indStart[0][0]
                        indEnd = np.where(tempData[:,0]==data[-1,0])
                        indEnd = indEnd[0][0]
                        end = data[indStart:,:]
                        start = tempData[:indEnd+1,:]
                        average = (end+start)/2.
                        scaleEnd = np.median(average/end, axis=0)
                        scaleStart = np.median(average/start, axis=0)

                        data[:,1:] = data[:,1:]*scaleEnd[1:]
                        tempData[:,1:] = tempData[:,1:]*scaleStart[1:]
                    data=np.append(data,tempData,axis=0)

                    #handle overlap of data sets
                else:
                    if self.verbose:
                        print("Does not exist")
                counter += 1


            if np.size(data) != 0:
                data = np.unique(data,axis=0)
                # if atlas uses wavenumbers then flip data
                if self.isWn is True:
                    data = np.flipud(data)
                    self.data = data[:,1:]
                    # vacuum wavenumber to wl
                    self.wl = cf.wnum2ang(data[:,0])/10000.
                    self.wn = data[:,0]


    def removeOutliers(self, lowerTreshold, upperTreshold, whichSet=-1):
        # set outliers to NaN
        self.outliersRemoved = True
        if whichSet == -1:
            if np.max(self.data) > upperTreshold:
                self.data[self.data > upperTreshold] = np.NaN
            if np.min(self.data) < lowerTreshold:
                self.data[self.data < lowerTreshold] = np.NaN
        else:
            self.data[self.data > upperTreshold, whichSet] = np.NaN
            self.data[self.data < lowerTreshold, whichSet] = np.NaN

    def saveObject(self, destination):
        with open(destination, 'wb') as output:  # Overwrites any existing file.
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

if __name__ == "__main__":
    

    # niratlas is photospheric from 0.75 to 1.1um use for 1.08 um
    # photatl use for 1.43, 3.934

    # following bands required
    # 0.83 - 0.878 for Ca II
    # lwl = 0.842 # lower boundary in um
    # uwl = 0.866   # upper boundary in um
    # atlas = "niratl"
    # saveas = 'atlas/SP854.pkl'

    # 1.06 - 1.1 for SP HeI, FeIII
    # lwl = 1.057 # lower boundary in um
    # uwl = 1.097   # upper boundary in um
    # atlas = "niratl"
    # saveas = 'atlas/SP1077.pkl'

    #   # 1.25 - 1.311 for SP SIX
    #   # can use CI1250 for this instead 
    #   # lwl = 1.225 # lower boundary in um
    #   # uwl = 1.279   # upper boundary in um
    #   # atlas = "photatl"
    #   # saveas = 'atlas/SP1252.pkl'
    #   #
    #   # 1.25 - 1.311 for SP HpaB
    #   # can use CI1250 for this instead 
    #   # lwl = 1.25 # lower boundary in um
    #   # uwl = 1.311   # upper boundary in um
    #   # atlas = "photatl"
    #   # saveas = 'atlas/SP1282.pkl'

    # 1.395 - 1.465 for SP SiX
    # lwl = 1.395 # lower boundary in um
    # uwl = 1.465   # upper boundary in um
    # atlas = "photatl"
    # saveas = 'atlas/SP1430.pkl'


    # # 2.175 - 2.261 for SP FeIX
    # lwl = 2.175 # lower boundary in um
    # uwl = 2.261   # upper boundary in um
    # atlas = "photatl"
    # saveas = 'atlas/SP2218.pkl'

    # 2.958 - 3.098, for SP MgVIII
    # lwl = 2.958 # lower boundary in um
    # uwl = 3.098   # upper boundary in um
    # atlas = "photatl"
    # saveas = 'atlas/SP3028.pkl'
    
    # 3.81 - 4.058, for SP SiIX
    # lwl = 3.81 # lower boundary in um
    # uwl = 4.058   # upper boundary in um
    # atlas = "photatl"
    # saveas = 'atlas/SP3934.pkl'

    ## 4.48 - 4.822 for SP CO
    # lwl = 4.48 # lower boundary in um
    # uwl = 4.822   # upper boundary in um
    # atlas = "acefts" #"photatl" # "acefts" 
    # saveas = 'atlas/SP4651.pkl'

    # 1.09 - 1.41 for CI Jband
    #lwl = 1.09 # lower boundary in um
    #uwl = 1.41   # upper boundary in um
    #atlas = "photatl"
    #saveas = 'atlas/CI1250.pkl'

    # 1.0485 - 1.0505 for CI Jband
    # lwl = 1.0485 # lower boundary in um
    # uwl = 1.0505   # upper boundary in um
    # atlas = "niratl"
    # saveas = 'atlas/CI1049.pkl'

    if atlas == "acefts":
        atlasBaseDir = "/home/andre/Dropbox (DKIST)/python/solarAtlas/ace-fts-atlas/"
    else:
        atlasBaseDir = "/home/andre/Dropbox (DKIST)/python/solarAtlas/NSO_atlases/"
    a = solarAtlas(lwl, uwl, atlasBaseDir, atlas, verbose=True)
    a.read()
    b = a.data
    a.saveObject(saveas)
    #a.interpolate(0.001/1000.)
    #my = np.zeros(100)
    #my[15:85]=1
    #a.convolve(my)
    plt.plot(a.wl, a.data[:, 1])
    plt.show()
