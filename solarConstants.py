# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 19:38:07 2018

@author: Andre
"""
import numpy as np

class solarConstants():
    def __init__(self):
        self.solarDiameterArcsec = 1919.  #arcsec
        self.solarDiameterArcmin = self.solarDiameterArcsec/60.
        self.solarDiameterDeg = self.solarDiameterArcsec/3600.
        self.solarDiameterRad = np.deg2rad(self.solarDiameterDeg)
        self.solarDiameterSterad = 2.*np.pi*(1.-np.cos(self.solarDiameterRad/2.))

        self.solarTemperature = 5778. #K

