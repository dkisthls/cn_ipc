# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 18:57:53 2018

@author: Andre

TODO:
  - radial dependence for aerosols, bb, DelZanna, DKIST, Rayleigh, mirror
    some can make use of bb dependence
"""
#TODO: some classes use disk flux as reference that is hard coded in the class
#      which might change if user changes the model for the disk
import numpy as np
import matplotlib.pyplot as plt
import os
import platform
import sys
import pickle
#if platform.system() == "Linux":
#  baseDir = "/home/andreef/Dropbox/python/"
#elif platform.system() == "Windows":
#    baseDir = ("C:\\Users\\Andre\\Dropbox\\python\\")
#
#sys.path.insert(0, os.path.join(baseDir,"myLibrary"))

import commonFunctions as cf
from solarConstants import solarConstants
sc = solarConstants()
from scipy.interpolate import interp1d, interp2d
import scipy.constants as pc
from solarRadialDependence import judgeRadialDependence

class solarFlux():
  def __init__(self,lam):
    self.lam = lam
    self.isInterpolated = False
    self.isInterpolatedWl = False
    self.radius = None
    
  def cgs2si(self):
    # convert CGS flux [erg cm^-2 mu^-1 st^-1 s^-1] units to SI
    # [W m^-2 m^-1 st^-1] units
    if self.fluxUnitsSystem == 'cgs':
      self.fluxUnitsSystem = "SI"
      self.fluxUnits = "[W m-2 m-1 st-1]"
      self.flux = self.flux * 1e-7*1e4*1e6
      try:
        self.lineBrightness = self.lineBrightness * 1e-7*1e4*1e6
      except AttributeError:
        pass
      try:
        self.originalLineBrightness = self.originalLineBrightness * 1e-7*1e4*1e6
      except AttributeError:
        pass
    else:
      print('Is already SI')
    
  def si2cgs(self):
    # convert SI flux [W m^-2 m^-1 st^-1] units to cgs
    # [erg cm^-2 mu^-1 st^-1 s^-1]
    if self.fluxUnitsSystem == 'SI':
      self.fluxUnitsSystem = "cgs"
      self.fluxUnits = "[erg cm-2 mu-1 st-1 s-1]"
      self.flux = self.flux / (1e-7*1e4*1e6)
      try:
        self.lineBrightness = self.lineBrightness / (1e-7*1e4*1e6)
      except AttributeError:
        pass
      try:
        self.originalLineBrightness = self.originalLineBrightness / (1e-7*1e4*1e6)
      except AttributeError:
        pass
    else:
      print('Is already cgs')
      
  def interpolate(self,newR=None,newL=None,forceNew=False):
    # make sure we use always SI units as function is stored
    if self.fluxUnitsSystem == 'cgs':
      self.cgs2si()
    # first time interpolation function has to be created
    if not (self.isInterpolated or self.isInterpolatedWl):
      self.interpolationFunction = interp2d(self.lam,self.radius,
                                            self.flux.T,
                                            kind='linear')
      try:
        # need 1d case for single lines
        if np.size(self.originalWl) == 1:
          print('here in 1d case')
          self.interpolationFunctionBrightness = interp1d(self.radius,
                                                        self.lineBrightness.T,
                                                        kind='linear')
          print('done creating 1d case')                                              
        else:
          self.interpolationFunctionBrightness = interp2d(self.originalWl,
                                                        self.radius,
                                                        self.lineBrightness.T,
                                                        kind='linear')
      except AttributeError:
        pass
    # the sky scaling needed a way to force a new interpolation when base is scaled  
    if forceNew:
      self.interpolationFunction = interp2d(self.lam,self.radius,
                                            self.flux.T,
                                            kind='linear')
      try:
        if np.size(self.originalWl) == 1:
          self.interpolationFunctionBrightness = interp1d(self.radius,
                                                        self.lineBrightness.T,
                                                        kind='linear')
        else:
          self.interpolationFunctionBrightness = interp2d(self.originalWl,
                                                        self.radius,
                                                        self.lineBrightness.T,
                                                        kind='linear')
      except AttributeError:
        pass
      
    if newL is None: 
      self.interpolatedRadius = newR
      self.isInterpolated = True
      self.interpolatedFlux = self.interpolationFunction(self.lam,
                                                       self.interpolatedRadius)
      try:
        if np.size(self.originalWl) == 1:
          tmp=np.array(self.interpolationFunctionBrightness(
              self.interpolatedRadius))
          self.interpolatedLineBrightness=np.expand_dims(tmp,axis=0)
        else:  
          self.interpolatedLineBrightness=self.interpolationFunctionBrightness(
              self.originalWl,self.interpolatedRadius)
      except AttributeError:
        pass
    elif newR is None:
      self.interpolatedWl = newL
      self.isInterpolatedWl = True
      self.interpolatedFluxWl = self.interpolationFunction(self.interpolatedWl,
                                                             self.radius)
  
  def saveObject(self, destination):
      with open(destination, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
    
class solarFluxAllen(solarFlux):
  # return solar disk flux in SI
  #  for tabulated values Allen 1973 p. 172
  def __init__(self,lam):
    solarFlux.__init__(self,lam)
    self.source = "Allen"
    l = np.array([.5,.75,1.,2.,2.5,3.,4.,5.,10.])
    #solar spectral radiance in [erg cm^-2 mu^-1 st^-1 s^-1] 
    # from Allen 1973 p. 172 
    flux = np.array([2.83e10, 1.88e10, 1.08e10, 1.71e9, 7.56e8, 3.86e8,
                          1.3e8, 5.5e7, 5e6])
    self.fluxUnitsSystem = "cgs"
    self.fluxUnits = "[erg cm-2 mu-1 st-1 s-1]"
    interFunction = interp1d(l,flux,kind='linear')
    self.flux = interFunction(lam)
    self.cgs2si()

class solarFluxAerosol(solarFlux):
  def __init__(self,lam,radius):  
    solarFlux.__init__(self,lam)
    self.radius = radius
    self.source = 'Tom'
    self.flux = 3.e-6 * solarFluxBlackbody(lam).flux
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"
    
    
class solarFluxBlackbody(solarFlux):
  def __init__(self,lam):  
    solarFlux.__init__(self,lam)
    self.source = 'Physics'
    # black body spectral radiance in SI units (W m^-2 ster^-1 m^-1)
    # wavelength input in meters and temperature in Kelvin
    self.flux = cf.blackbody(lam/1e6, sc.solarTemperature)
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"
    
class solarFluxDelZanna(solarFlux):
  def __init__(self,lam,lineWidth,region,radius,originalWl,diskCenter):  
    solarFlux.__init__(self,lam)
    self.source = 'DelZanna'
    self.lineWidth = lineWidth
    self.region = region
    self.originalRadius = 1.1
    self.radius = radius
    self.diskCenter = diskCenter
  #    l = np.array([1.0747,1.0798,1.4301,2.2183,2.5846,3.0285,3.9343])
    self.originalWl = originalWl
    
    # radial scale
    radialScale = np.zeros((np.size(self.originalWl),np.size(self.radius)))
    # use FeXIII for FeIX at 2218 for now
    lines = ['FeXIII1074','FeXIII1079','SiX1430','FeXIII1079','SiIX2584',
             'MgVIII3029','SiIX3928']
    for i in range(np.size(lines)):
      tmp = judgeRadialDependence(self.radius,lines[i],
                                  normalizedTo=self.originalRadius)
      radialScale[i,:] = tmp.radialScaleFactor

    
    # select regieon
    if self.region == 'AR':
      # flux in photons cm-2 s-1 asec-1
      f = np.array([4.3e2,3.4e2,262.,7.,34.,25.,10.])
    elif self.region == 'QR':
      # flux in photons cm-2 s-1 asec-1
      f = np.array([240.,140.,51.,11.,41.,34.,29.])
    elif self.region == 'QS':
      # flux in photons cm-2 s-1 asec-1
      f = np.array([ 35.,21.,37.,0.9,12.,0.7,6.])
    
    # convert to erg cm-2 s-1 asec-1
    f = f*cf.photonEnergy(self.originalWl/1e6)/pc.erg
    # convert to erg cm-2 s-1 st-1
    f = f/cf.arcsec2sterad(1,1)
    
    # convert to spectral flux in erg cm-2 s-1 st-1 um-1 for given line width
    # for each line seperately and add them all up
    hl = np.arange(0.5,5.3,0.00001) #0.1A resolution
    sp = np.zeros((np.size(hl),np.size(self.radius)))
    for j in range(np.size(self.radius)):
      for i in range(np.size(self.originalWl)):
        sp[:,j]= (sp[:,j] + cf.integratedLineIntensity2spectralIntensity(f[i]*
                                                             radialScale[i,j],
                                                             self.originalWl[i], 
                                                         lineWidth[i],hl))
    # need to do 2D interpolation now
    interf = interp2d(self.radius,hl,sp,kind='linear')
    self.flux = interf(self.radius,self.lam) 
    
    self.zannaWl = self.originalWl
    self.lineBrightness = interf(self.radius,self.zannaWl)
    self.originalLineBrightness = np.squeeze(interf(self.originalRadius,self.zannaWl))
    self.fluxUnitsSystem = "cgs"
    self.fluxUnits = "[erg cm-2 mu-1 st-1 s-1]"
    self.cgs2si()
    self.relativeLineBrightness = (self.originalLineBrightness/self.diskCenter)*1e6

class solarFluxCustomLines(solarFlux):
  def __init__(self,
               lam,
               lineCount,
               lineNames,
               lineWidths,
               lineWls,
               lineOriginalRadii,
               lineBrightnesses,
               lineBrightnessUnit,
               lineBrightnessesAreAbsolute,
               lineRadii,
               lineRadialDependence,
               lineRadialDependenceIsAbsolute,
               diskCenterAtCoronalWlFlux,
               radCorona):  
    solarFlux.__init__(self,lam)
    self.lineCount = lineCount
    self.lineNames = lineNames
    self.lineWidths = lineWidths
    self.lineWls = lineWls
    self.lineOriginalRadii = lineOriginalRadii
    self.lineBrightnesses = lineBrightnesses
    self.lineBrightnessesOriginal = lineBrightnesses
    self.lineBrightnessUnit = lineBrightnessUnit
    self.lineBrightnessesAreAbsolute = lineBrightnessesAreAbsolute
    self.lineRadii = lineRadii
    # self.lineRadialDependence = lineRadialDependence
    self.lineRadialDependenceIsAbsolute = lineRadialDependenceIsAbsolute
    self.diskCenterAtCoronalWlFlux = diskCenterAtCoronalWlFlux
    self.commonRadius = radCorona
    self.lam = lam
    # print(self.lineRadialDependence)
    
    # if smallest radius is not 1.0 then extrapolate to this value
    # interpolate and normalize to common radius
    self.extraLineRadii = []
    
    # self.commonRadius = np.arange(1.,1.6,0.05)
    self.lineRadialDependence = np.zeros((self.lineCount,len(self.commonRadius)))
    # self.flux = np.zeros((self.lineCount,len(self.commonRadius),len(self.lam)))
    ind = np.where(self.commonRadius == 1.1)[0]
    self.lineBrightnessesIntegrated = np.zeros(self.lineBrightnesses.shape)
    # arrays to crate high resolution spectrum
    # no need for higher resolution spectrum
    # hl = np.arange(0.5,5.3,0.0001) #1A resolution
    self.flux = np.zeros((np.size(self.lam),np.size(self.commonRadius)))
    self.relativeBrightnesses = np.zeros((self.lineCount,np.size(self.commonRadius)))
    #    self.relativeBrightnessInterpolationFunction=[]
    for i in range(self.lineCount):
      if not self.lineRadialDependenceIsAbsolute[i]:
        assert(1.0 in lineRadialDependence[i]),'Radial dependence does not seem to be normalized!'
      if lineRadii[i][0] == 1.0:
        self.extraLineRadii.append(self.lineRadii[i])
        
      else:
        temp = self.lineRadii[i]
        temp.insert(0,1.0)
        self.extraLineRadii.append(temp)
        # create value for r=1 using linear extrapolation
        dx=self.lineRadii[i][1]-self.lineRadii[i][0]
        dy=lineRadialDependence[i][1]-lineRadialDependence[i][0]
        dr=self.lineRadii[i][0]-1.
        di=dy/dx*dr
        lineRadialDependence[i].insert(0,lineRadialDependence[i][0]-di)
      # not all lines might have the same list length for radial dependence 
      # interpolate to common radius
      temp = np.interp(self.commonRadius,(self.extraLineRadii[i]),
                      (lineRadialDependence[i]))
      
      ########################################################################
      ## interpolate and normalize to R=1.1
      ########################################################################
      # is brightness given at R=1.1 this needs to be done before normalizing radial dependence
      if not self.lineOriginalRadii[i] == 1.1:
        # scale to that value
        sf = np.interp(1.1, self.commonRadius, temp)/np.interp(self.lineOriginalRadii[i], self.commonRadius, temp)
        self.lineBrightnesses[i] = sf*self.lineBrightnesses[i]
        
      # now normalize to r=1.1
      self.lineRadialDependence[i,:]=temp/temp[ind]
      
      ########################################################################
      # is the line Brightness absolute or relative to disk
      #########################################################################
      # absolute values are integrated
      if self.lineBrightnessesAreAbsolute[i]:
        #convert absolute integrated flux to cgs units as this is easier for what is to come
        if self.lineBrightnessUnit[i] == 'photons':
          # convert to cgs: erg cm-2 s-1 asec-1
          self.lineBrightnessesIntegrated[i] = self.lineBrightnesses[i]*cf.photonEnergy(self.lineWls[i]/1e6)/pc.erg
          # convert to erg cm-2 s-1 st-1
          self.lineBrightnessesIntegrated[i] = self.lineBrightnessesIntegrated[i]/cf.arcsec2sterad(1,1)
        elif self.lineBrightnessUnit[i] == 'cgs':
          pass
        elif self.lineBrightnessUnit[i] == 'SI':
          #convert SI [W m^-2 st^-1] units to CGS [erg cm^-2 st^-1 s^-1] units 
          self.lineBrightnessesIntegrated[i] = self.lineBrightnesses[i] / (pc.erg*1e4)
        else:
          assert(False),'LineBrightnessUnit is not supported! (SI,cgs,photons)'
        
        
        
        # put line brigthness amplitude into lineBrightnesses vector as SI
        self.lineBrightnesses[i] = 1e-7*1e4*1e6*cf.integratedLineIntensity2spectralIntensity(self.lineBrightnessesIntegrated[i],
                                                                                self.lineWls[i], 
                                                                                self.lineWidths[i],self.lam,
                                                                                returnAmplitude=True)
        
        # convert to spectral flux in erg cm-2 s-1 st-1 um-1 for given line width
        # for each line seperately, convert to SI and add them all up
        
        for j in range(np.size(self.commonRadius)):
          # do the conversion from cgs to SI here as relative cases will use SI disk flux
          self.flux[:,j]= (self.flux[:,j] + cf.integratedLineIntensity2spectralIntensity(self.lineBrightnessesIntegrated[i]*self.lineRadialDependence[i,j],
                                                                           self.lineWls[i],
                                                                           self.lineWidths[i],
                                                                           self.lam)* 1e-7*1e4*1e6)
          self.relativeBrightnesses[i,j] = 1e6*self.lineBrightnesses[i]*self.lineRadialDependence[i,j]/self.diskCenterAtCoronalWlFlux[i]
                                                                 
      else:
        for j in range(np.size(self.commonRadius)):
          self.flux[:,j]= (self.flux[:,j] + cf.gaussianLineSpectrum(self.lam,
                                                      self.lineBrightnesses[i]*self.lineRadialDependence[i,j]*self.diskCenterAtCoronalWlFlux[i]*1e-6,
                                                      self.lineWls[i],
                                                      self.lineWidths[i]))
          self.relativeBrightnesses[i,j] = self.lineBrightnesses[i]*self.lineRadialDependence[i,j]
        #      self.relativeBrightnessInterpolationFunction.append(interp1d(self.commonRadius,self.relativeBrightnesses[i],kind='linear')) 
      
      #use disk center flux for absolute value SI
        # relative brightnesses are amplitude in millionths of disk
      #   self.lineBrightnesses[i] = 1e-6*self.lineBrightnesses[i]*self.diskCenterAtCoronalWlFlux[i]
      # print(self.lineBrightnesses[i])
        #    print('rel line brightnesses',self.relativeBrightnesses[:,ind])  
    
    # create interpolation function
    #!!!: no need for 2D interpolation as self.lam will not change
    self.fluxInterpolationFunction = interp1d(self.commonRadius,self.flux,kind='linear')
    #    self.fluxInterpolationFunction = interp2d(self.commonRadius,self.lam,self.flux,kind='linear')
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"             
    
    # # convert to spectral flux in erg cm-2 s-1 st-1 um-1 for given line width
    # # for each line seperately and add them all up
    # hl = np.arange(0.5,5.3,0.00001) #0.1A resolution
    # sp = np.zeros((np.size(hl),np.size(self.radius)))
    # for j in range(np.size(self.radius)):
    #   for i in range(np.size(self.originalWl)):
    #     sp[:,j]= (sp[:,j] + cf.integratedLineIntensity2spectralIntensity(f[i]*
    #                                                          radialScale[i,j],
    #                                                          self.originalWl[i], 
    #                                                      lineWidth[i],hl))
    # # need to do 2D interpolation now
    # interf = interp2d(self.radius,hl,sp,kind='linear')
    # self.flux = interf(self.radius,self.lam) 
    
    # self.zannaWl = self.originalWl
    # self.lineBrightness = interf(self.radius,self.zannaWl)
    # self.originalLineBrightness = np.squeeze(interf(self.originalRadius,self.zannaWl))
    # self.fluxUnitsSystem = "cgs"
    # self.fluxUnits = "[erg cm-2 mu-1 st-1 s-1]"
    # self.cgs2si()
    # self.relativeLineBrightness = (self.originalLineBrightness/self.diskCenter)*1e6

class solarFluxCustomBgComponent(solarFlux):
    def __init__(self,
                 lam,
                 bgCount,
                 bgNames,
                 bgOriginalRadii,
                 bgOriginalWls,
                 bgBrightnesses,
                 bgBrightnessUnit,
                 bgBrightnessesAreAbsolute,
                 bgRadii,
                 bgRadialDependence,
                 bgRadialDependenceIsAbsolute,
                 bgWls,
                 bgWavelengthDependence,
                 bgWavelengthDependenceIsAbsolute,
                 diskCenter,
                 radCorona):  

        solarFlux.__init__(self,lam)
        self.bgCount = bgCount
        self.bgNames = bgNames
        self.bgOriginalRadii = bgOriginalRadii
        self.bgOriginalWls = bgOriginalWls
        self.bgBrightnesses = bgBrightnesses
        self.bgBrightnessesOriginal = bgBrightnesses
        self.bgBrightnessUnit = bgBrightnessUnit
        self.bgBrightnessesAreAbsolute = bgBrightnessesAreAbsolute
        self.bgRadii = bgRadii
        # self.bgRadialDependence = bgRadialDependence
        self.bgRadialDependenceIsAbsolute = bgRadialDependenceIsAbsolute
        self.bgWls = bgWls
        
        # self.bgWavelengthDependence = bgWavelengthDependence
        self.bgWavelengthDependenceIsAbsolute = bgWavelengthDependenceIsAbsolute
        self.diskCenter = diskCenter
        self.commonRadius = radCorona
        self.lam = lam
        self.bgTotalAtRef = None
        # print(self.lineRadialDependence)
        
        # if smallest radius is not 1.0 then extrapolate to this value
        # interpolate and normalize to common radius
        self.extraBgRadii = []
        
        self.bgRadialDependence = np.zeros((self.bgCount,len(self.commonRadius)))
        self.bgWavelengthDependence = np.zeros((self.bgCount,len(self.lam)))

        ind = np.where(self.commonRadius == 1.1)[0]
        indWl = np.where(np.abs(self.lam-1.1) < (self.lam[1]-self.lam[0])/10.)[0]
        
        self.flux = np.zeros((self.bgCount,np.size(self.lam),np.size(self.commonRadius)))
        self.fluxInterpolationFunction = []
        for i in range(self.bgCount):
            # Do some testing first
            assert((self.bgWls[i][0] <= self.lam[0]) & (self.bgWls[i][-1] >= self.lam[-1])),\
                'Wavelength range of given model does not cover full range of IPC'
            if not self.bgRadialDependenceIsAbsolute[i]:
                assert(1.0 in bgRadialDependence[i]),'Radial dependence does not seem to be normalized!'
            if not self.bgWavelengthDependenceIsAbsolute[i]:
                assert(1.0 in bgWavelengthDependence[i]),'Wavelength dependence does not seem to be normalized!'
            # extrapolation to R=1.0 if needed
            if bgRadii[i][0] == 1.0:
                self.extraBgRadii.append(self.bgRadii[i])
            else:
                temp = self.bgRadii[i]
                temp.insert(0,1.0)
                self.extraBgRadii.append(temp)
                # create value for r=1 using linear extrapolation
                dx=self.bgRadii[i][1]-self.bgRadii[i][0]
                dy=bgRadialDependence[i][1]-bgRadialDependence[i][0]
                dr=self.bgRadii[i][0]-1.
                di=dy/dx*dr
                bgRadialDependence[i].insert(0,bgRadialDependence[i][0]-di)

            ########################################################################
            # interpolate and normalize to R=1.1
            ########################################################################
            
            # not all background components might have the same list length for radial dependence 
            # interpolate to common radius
            temp = np.interp(self.commonRadius,(self.extraBgRadii[i]),
                            (bgRadialDependence[i]))
            
            # is brightness given at R=1.1 this needs to be done before normalizing radial dependence
            if not self.bgOriginalRadii[i] == 1.1:
                # scale to that value
                sf = np.interp(1.1, self.commonRadius, temp)/np.interp(self.bgOriginalRadii[i], self.commonRadius, temp)
                self.bgBrightnesses[i] = sf*self.bgBrightnesses[i]
        
            # now normalize to r=1.1
            self.bgRadialDependence[i,:]=temp/temp[ind]
            
            ########################################################################
            # interpolate and normalize to wl=1.1um
            ########################################################################
            
            # not all background components might have the same list length for wavelength dependence 
            # interpolate to common radius
            
            # make things look smooth in log plot
            temp = np.log10((bgWavelengthDependence[i]))
            tempf = interp1d((self.bgWls[i]),temp)
            temp = 10**tempf(self.lam)

            # is brightness given at 1.1um this needs to be done before normalizing wavelength dependence
            if not self.bgOriginalWls[i] == 1.1:
                # scale to that value
                sl = np.interp(1.1, self.lam, temp)/np.interp(self.bgOriginalWls[i], self.lam, temp)
                self.bgBrightnesses[i] = sl*self.bgBrightnesses[i]

            # now normalize to wl=1.1um
            self.bgWavelengthDependence[i,:]=temp/temp[indWl]

            ########################################################################
            # is the background component brightness absolute or relative to disk
            #########################################################################
            # absolute values are flux
            if self.bgBrightnessesAreAbsolute[i]:
                #convert absolute flux to SI units as this is easier for what is to come
                if self.bgBrightnessUnit[i] == 'cgs':
                    #convert CGS [erg cm^-2 st^-1 s^-1 um-1] units to SI [W m^-2 st^-1 m-1] units 
                    self.bgBrightnesses[i] = self.bgBrightnesses[i] * (pc.erg*1e4*1e6)
                elif self.bgBrightnessUnit[i] == 'SI':
                    pass
                    # #convert SI [W m^-2 st^-1 m-1] units to CGS [erg cm^-2 st^-1 s^-1 um-1] units 
                    # self.bgBrightnesses[i] = self.bgBrightnesses[i] / (pc.erg*1e4*1e6)
                else:
                    assert(False),'LineBrightnessUnit is not supported! (SI or cgs)'
            else:
                # turn relative flux into absolute flux at a fixed wl of 1.1um
                sd = np.interp(1.1,self.lam,self.diskCenter.flux)
                self.bgBrightnesses[i] = self.bgBrightnesses[i] * 1e-6*sd
                # in the relative case also need to account for wavelength dependence of disk spectrum to 
                # which model data is relative
                diskScale = self.diskCenter.flux/self.diskCenter.flux[indWl]
                self.bgWavelengthDependence[i] = diskScale*self.bgWavelengthDependence[i]
        
            # create the flux grid
            for j in range(len(self.commonRadius)):
                self.flux[i,:,j] = self.bgBrightnesses[i]*self.bgRadialDependence[i,j]*self.bgWavelengthDependence[i]
            # create interpolation function for each component individually
            #!!!: no need for 2D interpolation as self.lam will not change 
            self.fluxInterpolationFunction.append(interp1d(self.commonRadius,self.flux[i],kind='linear'))

            # need relative brightness for later scaling
            if self.bgNames[i] == 'sky':
                self.skyRelativeBrightness = (1e6*np.array(self.bgBrightnesses[i]) / 
                                              np.interp(1.1,self.lam,self.diskCenter.flux))
        
        self.totalBgRelativeBrightness = (1e6*np.sum(np.array(self.bgBrightnesses))/
                                          np.interp(1.1,self.lam,self.diskCenter.flux))
        self.bgTotalAtRef = 1
        self.fluxUnitsSystem = "SI"
        self.fluxUnits = "[W m-2 m-1 st-1]"             
      
      
    
class solarFluxDkist(solarFlux):
  # return solar disk flux in SI
  # wavelengths from DKIST flux budget from SMARTS so everthing beyon 4 um
  # is missing in [um]     
  def __init__(self,lam):
    solarFlux.__init__(self,lam)
    self.source = "DKIST"
    l = np.array([393.33,396.80,430.50,450.40,460.73,486.13,530.30,
                         555.00,587.60,589.00,617.33,630.20,637.00,656.30,
                         668.40,705.40,789.00,819.48,854.20,1074.60,1083.00,
                         1430.,1565.,1920.,2326.,2580.,2855.,3027.,3935.])/1e3
    # DKIST spectral radiance in [W m^-2 nm^-1 sun^-1] with sun diameter 32
    # arcminutes
    flux = np.array([0.409,0.3638,1.042,1.655,1.647,1.389,1.609,1.603,1.531,
                     1.513,1.404,1.392,1.425,1.177,1.347,1.302,1.092,0.9708,
                     0.7749,0.6023,0.5768,0.2045,0.2592,0.03344,0.06165,
                     0.000005,0.001095,0.01715,0.008271])
    # convert to SI
    flux = flux *1e9 / sc.solarDiameterSterad # [W m^-2 st^-1 m^-1]
    interFunction = interp1d(l,flux,kind='linear',bounds_error=False)
    self.flux = interFunction(lam)
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"

class solarFluxFCorona(solarFlux):   
  # Koutchmy-Lamy model of the F-coronal polarization
  def __init__(self,lam, radius):
    solarFlux.__init__(self,lam)
    self.source = 'Tom'
    self.radius = radius   # solar radii 
    # self.test = np.zeros(np.size(radius))
    self.flux = np.zeros((np.size(lam),np.size(radius)))
    for i in range(np.size(radius)):
      Fint = (14.86*self.radius[i]**(-7) + 
              4.99*self.radius[i]**(-2.5)) 
      # self.test[i] = Fint* (10.**(-8.))
      self.flux[:,i-1] = Fint * (10.**(-8.)) * solarFluxBlackbody(lam).flux
 
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"
    
class solarFluxHeKuhn(solarFlux):
  def __init__(self,lam, radius, lineWidth,diskCenter):  
    solarFlux.__init__(self,lam)
    self.source = 'Kuhn'
    self.lineWidth = lineWidth
    self.originalWl = np.array([1.0830])
    # self.radius = radius
    self.diskCenter = diskCenter
    self.diskCenterHeWl = np.interp(self.originalWl,lam, self.diskCenter.flux)
    #r = 1.045 #43 arcsec from limb
    
    # disk = solarFluxBlackbody(self.originalWl)
    allR = np.array([1. , 1.045, 1.069, 1.313, 1.521, 2.042])
    #    allF = np.array([50., 16.5 ,  5.3,  5.0,  4.8,   2.2])*1e-7*disk.flux
    # carefull - these are amplitudes not integrated line brightnesses
    allF = np.array([31.6, 16.5 ,  3.39,  3.16,  3.02,   1.66])*1e-7*self.diskCenterHeWl
    self.radius = allR
    f = allF
    # interpolate to requested r
    # f = np.interp(radius,allR,allF)
    # create spectrum
    hl = np.arange(0.5,5.3,0.00001) #0.1A resolution
    self.flux = np.zeros((np.size(lam),np.size(self.radius)))
    self.testWl = hl
    self.test = cf.gaussianLineSpectrum(hl,f[0],self.originalWl,self.lineWidth)
    for i in range(np.size(self.radius)):
      sp = cf.gaussianLineSpectrum(hl,f[i],self.originalWl,self.lineWidth)
      self.flux[:,i] = np.interp(lam,hl,sp)
    interf = interp2d(self.radius,self.lam,self.flux,kind='linear')
    self.lineBrightness = interf(self.radius,self.originalWl)
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"
    self.originalLineBrightness = np.squeeze(interf(1.1,self.originalWl))
    self.relativeLineBrightness = (self.originalLineBrightness/self.diskCenterHeWl)*1e6
    self.originalWl = np.array([1.0830])
    
class solarFluxKCorona(solarFlux):   
  # K-Corona (van del Hurst)
  def __init__(self,lam, radius):
    solarFlux.__init__(self,lam)
    self.source = 'Tom'
    self.radius = radius   # solar radii 
    # self.test = np.zeros(np.size(radius))
    self.test2 = solarFluxBlackbody(lam).flux
    # self.test2 = (10.**(-8.)) * solarFluxBlackbody(lam).flux
    self.flux = np.zeros((np.size(lam),np.size(radius)))
    for i in range(np.size(radius)):
      kmax =(355.6*self.radius[i]**(-17.)+177.80*self.radius[i]**
             (-7.) + 0.708*self.radius[i]**(-2.5))
      # self.test[i] = kmax * (10.**(-8.))
      self.flux[:,i-1] = kmax * (10.**(-8.)) * solarFluxBlackbody(lam).flux

    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"
    
class solarFluxKCoronaKuhn(solarFlux):   
  # K-Corona (van del Hurst)
  def __init__(self,lam,radius):
    solarFlux.__init__(self,lam)
    self.source = 'Kuhn'
    self.solarRadius = radius   # solar radii 
    
    r = np.array([1.,1.1,1.2,1.3,1.4,1.5,1.6])
    # continum in [W m-2 m-1 st-1]
    conth = np.array([.001,.0005,.00033,.0002,.00013,.00007,.00005])*1.e10
    allen = solarFluxAllen(lam)
    hcont = np.interp(1.6,lam,allen.flux)
    self.flux = np.zeros((np.size(lam),np.size(radius)))
    for i in range(np.size(radius)):
      self.flux[:,i-1] = np.interp(radius[i-1],r,conth)*allen.flux/hcont 

    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"
    
class solarFluxMirror(solarFlux):
  def __init__(self,lam,radius):  
    solarFlux.__init__(self,lam)
    self.source = 'Tom'
    # scattering (microroughness plus limited dust contamination)
    # Figure 15 of TN-0013 shows fairly flat, but can still do a linear
    self.solarRadius = radius   # solar radii 
    rprim  = np.array([1.0,2.0])
    sprim = np.array([2.75e-5,1.75e-5])  
    self.flux = np.zeros((np.size(lam),np.size(radius)))
    for i in range(np.size(radius)):
      self.flux[:,i-1] = (np.interp(radius[i],rprim,sprim)*
               solarFluxBlackbody(lam).flux)
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"
      
class solarFluxRayleigh(solarFlux):
  def __init__(self,lam):  
    solarFlux.__init__(self,lam)
    self.source = 'Tom'
    self.flux = 2.e-5 * solarFluxBlackbody(lam).flux * (0.5/lam)**4.
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"
    
class solarFluxSkyKuhn(solarFlux):
   #make sky signal in millionths, assume r**0.5 falloff
  def __init__(self,lam,radius,sky,cw,increment,diskCenter):  
    solarFlux.__init__(self,lam)
    self.source = 'Kuhn'
    self.skyInMillionth = sky
    ind= np.where(np.abs(lam-cw)<increment/10)[0]
    # print(ind)
    # implement lambda-4 fall off to 4um and then
    # find index of selected cw
    # otherScale = (lam[ind]/lam)
    # self.test = lamScale
    #lamScale[lam>4.1] = lamScale[lam>4.1]*10.
    modtranWl =    np.array([0.5, 1, 1.5, 2, 2.5, 2.6, 2.8, 3, 3.5, 4, 4.2, 4.5, 4.6, 5, 5.5])
    modtranScale = np.array([0.08 ,0.008, 0.001, 0.0004, 0.00007,0.0000007,0.000002, 0.00001, 0.00001, 0.00001, 0.00007, 0.00007, 0.00002,0.00007,0.00007 ])
    modtranScale = np.log10(modtranScale/modtranScale[0])
    interf = interp1d(modtranWl,modtranScale,kind='linear')
    lamScale = 10**(interf(lam))
    # lamScale = (interf(lam))
    #scale at the selected center wavelength
    lamScale = (lamScale/lamScale[ind])
    
    self.test = lamScale
    self.radius = radius   # solar radii 
    # allen = solarFluxAllen(lam)
    self.flux = np.zeros((np.size(lam),np.size(radius)))
    #    self.test = np.zeros((np.size(lam),np.size(radius)))
    #    self.test2 = np.zeros((np.size(lam),np.size(radius)))
    for i in range(np.size(radius)):
      #self.flux[:,i-1] = 1.e-6*sky*(radius[i-1]/1.5)**(-0.5)*allen.flux
      self.flux[:,i] = 1.e-6*self.skyInMillionth*(radius[i]/1.1)**(-0.5)*diskCenter.flux[ind]*lamScale
    #      self.test[:,i-1] = (radius[i-1]/1.5)**(-0.5)
    #      self.test2[:,i-1] = (radius[i-1]/1.1)**(-0.5)
    
    self.fluxUnitsSystem = "SI"
    self.fluxUnits = "[W m-2 m-1 st-1]"

if __name__ == "__main__":

    increment=.1
    lam = np.arange(0.5,5.6,increment)
    ## # # heWl = 1.083
    ## # # lineWidthHe =3e-4
    #lam =    np.array([0.5,0.6,0.8, 1, 1.5, 2, 2.5, 2.6, 2.8, 3, 3.5, 4, 4.2, 4.5, 4.6, 5, 5.5])
    ## # new = np.array([1.00000000e+00,1.22559841e-01,1.84096091e-02,7.98475945e-03,1.62995683e-03,2.44834424e-05,6.37563835e-05,2.76529166e-04,
    ## #                 2.76529166e-04,2.76529166e-04,1.62995683e-03,1.62995683e-03,5.20206153e-04,1.62995683e-03,1.62995683e-03])
    ## # so = np.interp(ne
    # wlam,lam,new)
    radius = np.arange(0.5,1.6,.05)
    radius = np.array([1.1])
    ## sky=1
    ## cw=0.5
    #
    diskCenter = solarFluxBlackbody(lam)
    print('disk center', diskCenter.flux.shape)
    a = solarFluxFCorona(lam, radius)
    print(a.flux.shape)
    #a=solarFluxKCorona(lam,radius)#,sky,cw,increment,diskCenter)  
    #k=a.test2
    #print(k/k[0])
    # b = np.squeeze(a.flux)
    # c = diskCenter.flux
    ratio = a.flux[:,0]/diskCenter.flux
    print('rel brightness B_disk', ratio*1e6)

    fig, ax=plt.subplots()
    ax.plot(lam, a.flux[:,0])#/b[0])#
    ax.set_yscale('log')

    fig, ax=plt.subplots()
    ax.plot(lam, diskCenter.flux)#/b[0])#
    ax.set_yscale('log')

    plt.show()

# zannaWl = np.array([ 1.0747 , 1.0798 , 1.4305  , 2.2183 , 2.5846 , 3.0285 , 3.9343])
# lineWidth = 3e-4*zannaWl
# diskCenterZannaWl = np.interp(zannaWl,lam, diskCenter.flux)

# model = "QS"
# #
# z = solarFluxDelZanna(lam,lineWidth,model,radius,zannaWl,diskCenterZannaWl)

# # print(z.lineBrightness[:,0])
# # print(z.originalLineBrightness)
# print(z.relativeLineBrightness)
# # print(diskCenterZannaWl/1e6)
# # fig, ax=plt.subplots()
# # ax.plot(lam,z.flux[:,0])
# # plt.show()
# start = 5746
# end = 5748
# a = z.flux
# f = z.flux[start:end,0]
# val1 = np.trapz(f,lam[start:end]/1e6)
# # val1 = np.trapz(f,dx=.0001/1e6)
# val2 = diskCenter.flux[5747]*0.000000001
# val4 = a[5747,0]*0.000000001
# # val2 = np.trapz(diskCenter.flux[start:end],lam[start:end]/1e6)
# val3 = z.relativeLineBrightness[0]
# print((val1)/val2*1e6)

