# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 18:57:53 2018

@author: Andre
"""
# import numpy as np
# import matplotlib.pyplot as plt

import os
# import platform
import sys
# import pickle
from scipy.interpolate import interp1d, interp2d

baseDir=os.path.dirname(os.path.realpath(sys.argv[0]))

sys.path.insert(0, os.path.join(baseDir,"myLibrary"))

#import commonFunctions as cf
#from solarConstants import solarConstants
#sc = solarConstants()
#from scipy.interpolate import interp1d

class solarRadialDependence():
  def __init__(self,r):
    self.radius = r
    
  def interpolate(self,newR,newL):
    
      self.interpolatedRadius = newR
      self.interpolatedWl = newL
      self.isInterpolated = True
      self.radialScaleFactor = self.radialScaleFactorFunction(newR,newL)
  
  def saveObject(self, destination):
      with open(destination, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
      
class solarLimbDarkening(solarRadialDependence):
  # return solar scale factor to disk center 
  # Allen 3rd edition 
  #-(a1+2*a2)
  def __init__(self,r):
    solarRadialDependence.__init__(self,r)
    
    ro = np.arange(0.,1.01,0.01)
    lo = np.array([0.55, 1., 1.5, 2., 3., 5.])
    a1o = np.array([-0.47, -0.24, -0.15, -0.12, -0.11, -0.08])
    a2o = np.array([-0.23, -0.20, -0.21, -0.18, -0.12, -0.07])
   
    scaleF = np.zeros((np.size(ro), np.size(lo)))
    x = np.arcsin(ro)
    for i in range(np.size(lo)):
      scaleF[:,i] = 1. + a1o[i]*(1.-np.cos(x))+a2o[i]*(1.-np.cos(x))**2.
    
    # need to do 2D interpolation now
    self.radialScaleFactorFunction = interp2d(ro,lo,scaleF.T,kind='linear')
    
    #x = np.arcsin(self.radius)
    
#    self.radialScaleFactorFunction = interf(np.arcsin(self.radius),self.lam) 
#    self.x = np.rad2deg(x)
    
class forwardRadialDependence(solarRadialDependence):
  # return off disk scale factor
  def __init__(self,r,baseDir,line='SiX',normalizedTo=1.,crop=None):
    solarRadialDependence.__init__(self,r)
    self.baseDir = baseDir
    self.line = line
    self.normalizedTo = normalizedTo
    self.r = r
    if line == 'SiX':
      tempData =np.loadtxt(os.path.join(self.baseDir,
                                       'radialData','siX.txt'), skiprows=1)
    elif line == 'FeXIII':
      tempData =np.loadtxt(os.path.join(self.baseDir,
                                       'radialData','feXIII.txt'), skiprows=1)
    elif line == 'continuum':
      tempData =np.loadtxt(os.path.join(self.baseDir,
                                    'radialData','continuum.txt'), skiprows=1)
    # remove values at the beginning
    if crop is not None:
      tempData = np.delete(tempData,np.arange(0,crop),axis=0)
    # create value for r=1 using linear extrapolation
    dx=tempData[1,0]-tempData[0,0]
    dy=tempData[1,1]-tempData[0,1]
    dr=tempData[0,0]-1.
    di=dy/dx*dr
    
    self.originalR = tempData[:,0]
    
    self.originalBrightness = tempData[:,1]/tempData[0,1]
    a = np.array([[1.,tempData[0,1]-di,-99,-99]])
    tempData = np.insert(tempData,0,a,axis=0)
    
    # normalize to specific value for R
    self.radialScaleFactor = np.interp(self.r, tempData[:,0],tempData[:,1])
    self.normalizationFactor = np.interp(self.normalizedTo, tempData[:,0],tempData[:,1])
    self.radialScaleFactor = self.radialScaleFactor/self.normalizationFactor

    
class judgeRadialDependence(solarRadialDependence):
  # return off disk scale factor
  # modification to FeXIII ratios for R 1.03 to 1.13
  # only valid for normalization to 1.1
  def __init__(self,r,line,normalizedTo=1.1,correction=True):
    solarRadialDependence.__init__(self,r)
    self.line = line
    if correction:
      assert (normalizedTo == 1.1), "correction holds only for R=1.1 normalization"
    self.normalizedTo = normalizedTo
    self.r = r
    self.originalR = np.array([1.03, 1.05, 1.07, 1.1, 1.2, 1.3, 1.5, 1.7])
    self.rMod = np.arange(1.,1.1,0.01)
    self.valMod = np.array([ 0.93419267,0.92586801,0.91761752,0.90944056,
                            0.89618208,0.88311688,0.89067384,0.89829545,
                            0.9309924,0.96487947,1.])
    if self.line == 'FeXIII1074':
      tempData = np.array([51.,40.,31.,22.,8.8,4.0,2.05,0.43])/51.
    elif self.line == 'FeXIII1079':
      tempData = np.array([13.,10.5,8.,5.1,1.1,0.42,0.09,0.021])/13.
    elif self.line == 'SiX1430':
      tempData = np.array([11.,9.,7.2,5.1,2.,0.9,0.22,0.09])/11.
    elif self.line == 'SiIX2584':
      tempData = np.array([2.7,2.,1.6,1.1,0.39,0.13,0.033,0.01])/2.7
    elif self.line == 'SiIX3928':
      tempData = np.array([1.1,1.,0.9,0.7,0.31,0.13,0.054,0.021])/1.1
    elif self.line == 'MgVIII3029':
      tempData = np.array([0.9,0.7,0.59,0.41,0.14,0.083,0.022,0.01])/0.9
    
    # create value for r=1 using linear extrapolation
    dx=self.originalR[1]-self.originalR[0]
    dy=tempData[1]-tempData[0]
    dr=self.originalR[0]-1.
    di=dy/dx*dr
#    self.originalScale = tempData
    self.originalScale = np.insert(tempData,0,tempData[0]-di)
    self.originalR = np.insert(self.originalR,0,1.)
    
    # normalize to specific value for R
    temp = np.log10(self.originalScale)
    
    temp=np.interp(self.r,self.originalR, np.log10(self.originalScale))
    self.radialScaleFactor = 10**temp
    # force constant ratio below R=1.1 for iron lines by altering 1079 line
    if (self.line == 'FeXIII1079' and correction):
      rtemp = self.r[self.r<=1.1]
      stemp = np.interp(rtemp,self.rMod,self.valMod)
      self.radialScaleFactor[self.r<=1.1] = self.radialScaleFactor[self.r<=1.1]*stemp
    self.normalizationFactor = np.interp(self.normalizedTo,
                                         self.originalR,self.originalScale)
    self.radialScaleFactor = self.radialScaleFactor/self.normalizationFactor

class zannaRadialDependence(solarRadialDependence):
  # return off disk scale factor
  def __init__(self,r,line='FeXIII1074',normalizedTo=1.):
    solarRadialDependence.__init__(self,r)
    self.line = line
    self.normalizedTo = normalizedTo
    self.r = r
    
    self.originalR = np.array([1.0, 1.05 ,1.1, 1.15, 1.2, 1.3, 1.4, 1.5, 1.6])
    self.originalScale = ((10.**np.array([2.65, 2.35, 2.0, 1.8, 1.566, 1.2, 
                                          0.9, 0.7, 0.5]))/(10.**2.65))
    temp = np.log10(self.originalScale)
    temp = np.interp(self.r,self.originalR,temp)
#    # normalize to specific value for R
    self.radialScaleFactor=10**temp
    self.normalizationFactor = np.interp(self.normalizedTo,
                                         self.originalR,self.originalScale)
    self.radialScaleFactor = self.radialScaleFactor/self.normalizationFactor

class kuhnRadialDependence(solarRadialDependence):
  # return off disk scale factor
  def __init__(self,r,line='FeXIII1074',normalizedTo=1.):
    solarRadialDependence.__init__(self,r)
    self.line = line
    self.normalizedTo = normalizedTo
    self.r = r
#    cw = 1.0747
#    disk = solarFluxAllen(cw)
    self.originalR = np.array([1.0 , 1.069, 1.521, 2.042 ])
    self.originalScale = (np.array([29.51,14.66 , 2.39, 0.63 ])*1e-7)/(29.51e-7)
    
    temp = np.log10(self.originalScale)
    temp = np.interp(self.r,self.originalR,temp)
#    # normalize to specific value for R
    self.radialScaleFactor=10**temp
    self.normalizationFactor = np.interp(self.normalizedTo,
                                         self.originalR,self.originalScale)
    self.radialScaleFactor = self.radialScaleFactor/self.normalizationFactor
    
    
# radius = np.array([1.03, 1.05, 1.07, 1.1, 1.2, 1.3, 1.5, 1.7])   
# #radius = np.arange(1.0, 1.5,0.01)
# lines = ['FeXIII1074','FeXIII1079']
# fig, ax=plt.subplots()
# a = judgeRadialDependence(radius,lines[0],normalizedTo=1.03,correction=False)
# b = judgeRadialDependence(radius,lines[1],normalizedTo=1.03,correction=False)
# ax.plot(radius,a.radialScaleFactor/b.radialScaleFactor)
# print(b.radialScaleFactor)
#  
##ax.set_yscale('log')
##ax.set_xlim([1.0, 1.15])
#ax.set_xlabel('distance [R$_{Sun}$]')
#ax.set_ylabel('FeXIII radial scale factor ratio')
##ax.legend(lines)
##ax.set_title('FeXIII 1074')
#plt.show()
    
#limb=solarLimbDarkening(np.arange(0,1.01,.01))
#plt.plot(limb.radius,limb.radialScaleFactor)
#radius = np.arange(1., 1.6,0.001)
#si = forwardRadialDependence(radius,baseDir, 'SiX',1.1)
#fe = forwardRadialDependence(radius,baseDir, 'FeXIII',1.1)
#con = forwardRadialDependence(radius,baseDir, 'continuum',1)
#fig, ax=plt.subplots()
#ax.plot(radius,si.radialScaleFactor,'k', radius, fe.radialScaleFactor, 'b',
#         radius,con.radialScaleFactor, 'r')#, si.originalR, si.originalBrightness, 'g')
#ax.legend(['SiX','FeXIII','continuum'])
#ax.set_xlabel('distance [R$_{Sun}$]')
#ax.set_ylabel('relative line intensity')
#plt.show()
#
#radius = np.arange(1., 1.6,0.001)
#fez = zannaRadialDependence(radius,normalizedTo=1.1)
#fej = judgeRadialDependence(radius,'FeXIII1074',normalizedTo=1.1)
#fef = forwardRadialDependence(radius,baseDir,'FeXIII',normalizedTo=1.1)
#fek = kuhnRadialDependence(radius,normalizedTo=1.1)
#fig, ax=plt.subplots()
#
#ax.plot(fez.r,fez.radialScaleFactor,fej.r,fej.radialScaleFactor, 'r',
#        fef.r,fef.radialScaleFactor,'g',fek.r,fek.radialScaleFactor,'m')
#ax.set_yscale('log')
#ax.set_xlabel('distance [R$_{Sun}$]')
#ax.set_ylabel('relative line intensity')
#ax.legend(['DelZanna','Judge','FORWARD', 'Kuhn 1996'])
#ax.set_title('FeXIII 10747')
##
##fig, ax=plt.subplots()
##ax.plot(fek.r,fek.radialScaleFactor*4.5e-6)
##ax.set_yscale('log')
#plt.show()
##
#
## use FeXIII for FeIX at 2218 for now
#radius = np.arange(1.0, 1.6,0.001)
#lines = ['FeXIII1074','FeXIII1079','SiX1430','SiIX2584',
#         'MgVIII3029','SiIX3928']
#fig, ax=plt.subplots()
#for i in range(np.size(lines)):
#  tmp = judgeRadialDependence(radius,lines[i],
#                              normalizedTo=1.1)
#  ax.plot(radius,tmp.radialScaleFactor)
#  
##ax.set_yscale('log')
#ax.set_xlim([1.0, 1.15])
#ax.set_xlabel('distance [R$_{Sun}$]')
#ax.set_ylabel('relative line intensity')
#ax.legend(lines)
##ax.set_title('FeXIII 1074')
#plt.show()
    
#radius = np.arange(0., 1.01,0.01)
#lam = np.arange(0.5, 5.5,0.5)
#ld = solarLimbDarkening(radius)
#x = np.rad2deg(np.arcsin(radius))
#
#fig, ax=plt.subplots()
#for i in range(np.size(lam)):
#  tmp = ld.radialScaleFactorFunction(radius,lam[i])
#  ax.plot(radius,tmp)
#
##ax.set_yscale('log')
#ax.set_xlabel('distance [R$_{Sun}$]')
#ax.set_ylabel('relative intensity')
#ax.legend(lam)
##ax.set_title('FeXIII 1074')
#plt.show()


