#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 27 14:56:26 2017

@author: andreef
"""
# import sys
# import conda
# import os
from PyQt5 import QtCore # QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QWidget, QVBoxLayout
from matplotlib.backends.backend_qt5agg \
import FigureCanvasQTAgg as FigureCanvas

import numpy as np
import matplotlib.pyplot as plt

import matplotlib as mpl
from matplotlib.patches import Rectangle, Circle
from matplotlib.collections import PatchCollection
import sunpy.map
# import sunpy.data.sample
import astropy.units as u
from astropy.coordinates import SkyCoord
from sunpy.visualization.wcsaxes_compat import get_world_transform

#from matplotlib.lines import Line2D
from shapely.geometry import Point
from descartes import PolygonPatch

class DraggableCircle:
    def __init__(self, x0, y0, ax1, ax2,
                 rfov, spscan1, spscan2, ciscan1, ciscan2, occulter2, m, largeFov):
        self.x0 = x0
        self.y0 = y0
        self.press1 = None
        self.press2 = None
    
        self.spscan1 = spscan1
        self.spscan2 = spscan2
        self.ax1 = ax1
        self.ax2 = ax2
        self.m = m
        self.rfov = rfov
        self.largeFov= largeFov
        self.ciscan1 = ciscan1
        self.ciscan2 = ciscan2
        self.occulter2 = occulter2
        self.circ1 = plt.Circle((self.x0, self.y0), radius=self.rfov, fill=None,
                           edgecolor='red', transform=get_world_transform(ax1))#, linewidth=map2.mylinewidth)
        self.circ2 = plt.Circle((self.x0, self.y0), radius=self.rfov, fill=None,
                           edgecolor='red', transform=get_world_transform(ax2))#, linewidth=map2.mylinewidth)
 
        self.occulterOn = False
        ax1.add_patch(self.circ1)
        ax2.add_patch(self.circ2)

    def updateCircle(self, x, y, rfov, slit_length,
                     nr_steps, step_size, field_rotation, numPos, ocOn):
        self.rfov = rfov
        self.x0 = x
        self.y0 = y
        center = [x, y]
        self.spscan1.remove()
        self.spscan2.remove()
        self.ciscan1.remove()
        self.ciscan2.remove()
        self.occulterOn = ocOn

        try:
            self.occulter2.remove()
        except Exception:
            pass

        #update slit
        self.spscan1.slit_length = slit_length
        self.spscan2.slit_length = slit_length
        #update num Steps
        self.spscan1.nr_steps = nr_steps
        self.spscan2.nr_steps = nr_steps
        #update Step size
        self.spscan1.step_size = step_size
        self.spscan2.step_size = step_size
        #update center
        self.spscan1.center = center
        self.spscan2.center = center

        if self.occulterOn:
            self.occulter2.center = center
            self.occulter2.rfov = self.rfov
            self.occulter2.field_rotation = field_rotation
            self.occulter2.redraw()
        #update rotation
        self.spscan1.field_rotation = field_rotation
        self.spscan2.field_rotation = field_rotation
        self.spscan1.redraw()
        self.spscan2.redraw()

        #update center
        self.ciscan1.center = center
        self.ciscan2.center = center
        self.ciscan1.numPos = numPos
        self.ciscan2.numPos = numPos
        self.ciscan1.rfov = self.rfov
        self.ciscan2.rfov = self.rfov
        #update rotation
        self.ciscan1.field_rotation = field_rotation
        self.ciscan2.field_rotation = field_rotation
        self.ciscan1.slit_length = slit_length
        self.ciscan2.slit_length = slit_length

        self.ciscan1.redraw()
        self.ciscan2.redraw()

        # update limits on zoomed region
        bottomLeft = SkyCoord(3600*(x - 1.1*self.largeFov)*u.arcsec,
                              3600*(y - 1.1*self.largeFov)*u.arcsec,
                              frame=self.m.coordinate_frame)
        topRight = SkyCoord(3600*(x + 1.1*self.largeFov)*u.arcsec,
                            3600*(y + 1.1*self.largeFov)*u.arcsec,
                            frame=self.m.coordinate_frame)
        pixelbl = self.m.world_to_pixel(bottomLeft)
        pixeltr = self.m.world_to_pixel(topRight)

        self.ax2.set_xlim([pixelbl[0].value, pixeltr[0].value])
        self.ax2.set_ylim([pixelbl[1].value, pixeltr[1].value])

        self.circ1.center = (x, y)
        self.circ2.center = (x, y)
        self.circ1.radius = self.rfov
        self.circ2.radius = self.rfov
        # both cirles are on the same figure
        self.circ1.figure.canvas.draw()
        # self.circ2.figure.canvas.draw()

    def connect(self):
        'connect to all the events we need'
        self.cidpress1 = self.circ1.figure.canvas.mpl_connect(
            'button_press_event', self.on_press)
        self.cidrelease1 = self.circ1.figure.canvas.mpl_connect(
            'button_release_event', self.on_release)
        self.cidmotion1 = self.circ1.figure.canvas.mpl_connect(
            'motion_notify_event', self.on_motion)
        self.cidpress2 = self.circ2.figure.canvas.mpl_connect(
            'button_press_event', self.on_press)
        self.cidrelease2 = self.circ2.figure.canvas.mpl_connect(
            'button_release_event', self.on_release)
        self.cidmotion2 = self.circ2.figure.canvas.mpl_connect(
            'motion_notify_event', self.on_motion)

    def on_press(self, event):
        'on button press we will see if the mouse is over us and store some data'
        #if ((event.inaxes != self.circ1.axes) or (event.inaxes !=self.circ2.axes)): return
        if event.inaxes == self.circ1.axes:

            contains, attrd = self.circ1.contains(event)
            if not contains:
                return
            # # print('event contains', self.circ.center)
            x0, y0 = self.circ1.center
            # convert to pixel coordinates
            trans = self.m.world_to_pixel(SkyCoord(3600*x0*u.arcsec, 3600*y0*u.arcsec,
                                          frame=self.m.coordinate_frame))
            x0 = trans[0].value
            y0 = trans[1].value
            self.press1 = x0, y0, event.xdata, event.ydata

        elif event.inaxes == self.circ2.axes:
            contains, attrd = self.circ2.contains(event)
            if not contains:
                return
            # print('event contains', self.circ.center)
            x0, y0 = self.circ2.center
            trans = self.m.world_to_pixel(SkyCoord(3600*x0*u.arcsec, 3600*y0*u.arcsec,
                                          frame=self.m.coordinate_frame))
            x0 = trans[0].value
            y0 = trans[1].value
            self.press2 = x0, y0, event.xdata, event.ydata
        else:
            return
        self.spscan1.remove()
        self.spscan2.remove()
        self.ciscan1.remove()
        self.ciscan2.remove()
        try:
            self.occulter2.remove()
        except Exception:
            pass

    def on_motion(self, event):
        'on motion we will move the rect if the mouse is over us'
        #if ((self.press1 is None) and (self.press2 is None)): return
        #if ((event.inaxes != self.circ1.axes) or (event.inaxes !=self.circ2.axes)): return
        if event.inaxes == self.circ1.axes:
            if self.press1 is not None:
                x0, y0, xpress, ypress = self.press1
                dx = event.xdata - xpress
                dy = event.ydata - ypress
            else: return
        elif event.inaxes == self.circ2.axes:
            if self.press2 is not None:
                x0, y0, xpress, ypress = self.press2
                dx = event.xdata - xpress
                dy = event.ydata - ypress
            else: return
        else: return

        # update limits on zoomed in map
        # events return pixel values so we need to transform to coordiantes
        trans = self.m.pixel_to_world((x0+dx)*u.pix, (y0+dy)*u.pix)

        nx = trans.Tx.value/3600
        ny = trans.Ty.value/3600

        self.circ1.center = (nx, ny)
        self.circ2.center = (nx, ny)

        bottomLeft = SkyCoord(3600*(nx - 1.1*self.largeFov)*u.arcsec,
                              3600*(ny - 1.1*self.largeFov)*u.arcsec,
                              frame=self.m.coordinate_frame)
        topRight = SkyCoord(3600*(nx + 1.1*self.largeFov)*u.arcsec,
                            3600*(ny + 1.1*self.largeFov)*u.arcsec,
                            frame=self.m.coordinate_frame)
        pixelbl = self.m.world_to_pixel(bottomLeft)
        pixeltr = self.m.world_to_pixel(topRight)

        self.ax2.set_xlim([pixelbl[0].value, pixeltr[0].value])
        self.ax2.set_ylim([pixelbl[1].value, pixeltr[1].value])

        # both circles are on the same figure
        self.circ1.figure.canvas.draw()
        # self.circ2.figure.canvas.draw()

    def on_release(self, event):
        'on release we reset the press data'
        if event.inaxes == self.circ1.axes:
            if self.press1 is not None:
                if self.occulterOn:
                    self.occulter2.center = self.circ2.center
                    self.occulter2.redraw()
                self.spscan1.center = self.circ1.center
                self.spscan1.redraw()
                self.spscan2.center = self.circ2.center
                self.spscan2.redraw()
                self.ciscan1.center = self.circ1.center
                self.ciscan1.redraw()
                self.ciscan2.center = self.circ2.center
                self.ciscan2.redraw()
                self.circ1.figure.canvas.draw()
                # self.circ2.figure.canvas.draw()

        elif event.inaxes == self.circ2.axes:
            if self.press2 is not None:
                if self.occulterOn:
                    self.occulter2.center = self.circ2.center
                    self.occulter2.redraw()
                self.spscan1.center = self.circ1.center
                self.spscan1.redraw()
                self.spscan2.center = self.circ2.center
                self.spscan2.redraw()
                self.ciscan1.center = self.circ1.center
                self.ciscan1.redraw()
                self.ciscan2.center = self.circ2.center
                self.ciscan2.redraw()
                self.circ1.figure.canvas.draw()
                # self.circ2.figure.canvas.draw()

        self.press1 = None
        self.press2 = None

class CiScanRegion:
    def __init__(self, center, field_size, field_rotation, rfov, slit_length, numPos,
                 mylinewidth):
        self.press = None
        self.center = center # given in degrees
        self.field_size = field_size # given in degrees
        self.field_rotation = field_rotation
        self.rfov = rfov # given in degrees (radius of fov)
        self.slit_length = slit_length # given in degrees
        self.ax = plt.gca()
        self.numPos = numPos
        self.mylinewidth = mylinewidth
        self.ciCenterPositions = [[1], [1]]
        self.redraw()

    def redraw(self):
        ci_boxes = []
        self.ciCenterPositions = self.center_positions(self.numPos)
        bot_x = self.ciCenterPositions[0, :]-self.field_size/2.
        bot_y = self.ciCenterPositions[1, :]-self.field_size/2.
        for x, y, in zip(bot_x, bot_y):
            rect = Rectangle((x, y), self.field_size, self.field_size)
            ci_boxes.append(rect)

        ts = self.ax.transData
        tr = mpl.transforms.Affine2D().rotate_deg_around(self.center[0],
                                                         self.center[1],
                                                         self.field_rotation)
        t = tr + ts
        self.pc = PatchCollection(ci_boxes, facecolor='None', edgecolors='m',
                                  linewidth=self.mylinewidth, zorder=3,
                                  )

        self.pc.set_transform(tr + get_world_transform(self.ax))

        self.ax.add_collection(self.pc)

    def remove(self):
        try:
            self.pc.remove()
        except Exception:
            pass

    def center_positions(self, nrPos):
        #returns positions of center positions for CI fields
        # fov is in degrees (for ci it is radius of fov)
        if self.rfov <= 1.5/60.:
            dx = self.field_size/2 - (self.field_size-self.rfov)
            dy = self.field_size/2 - (self.field_size-self.rfov)
            return{
                1: np.array([[self.center[0]], [self.center[1]]]),
                2: np.array([[self.center[0],
                              self.center[0]],
                             [self.center[1]+dy,
                              self.center[1]-dy]]),
                4: np.array([[self.center[0]-dx,
                              self.center[0]-dx,
                              self.center[0]+dx,
                              self.center[0]+dx],
                             [self.center[1]+dy,
                              self.center[1]-dy,
                              self.center[1]-dy,
                              self.center[1]+dy]])
            }[nrPos]
        else:
            dx = self.field_size/2 - (self.field_size-self.rfov)
            dy = self.field_size/2 - (self.field_size-self.rfov)
            dm = self.field_size/2 - (self.field_size - self.rfov/np.sqrt(2))
            dn = self.field_size/2 - (self.field_size - self.rfov/np.sqrt(2))
            # slit length given in degrees
            if self.slit_length <= 2./60.:
                ds = self.field_size/2 - (2*self.field_size-self.slit_length)/4.
                return{
                    1: np.array([[self.center[0]], [self.center[1]]]),
                    2: np.array([[self.center[0],
                                  self.center[0]],
                                 [self.center[1]+ds,
                                  self.center[1]-ds]]),
                    4: np.array([[self.center[0]-dm,
                                  self.center[0]-dm,
                                  self.center[0]+dm,
                                  self.center[0]+dm],
                                 [self.center[1]+dn,
                                  self.center[1]-dn,
                                  self.center[1]-dn,
                                  self.center[1]+dn]]),
                    9: np.array([[self.center[0]-dx,
                                  self.center[0]-dx,
                                  self.center[0]-dx,
                                  self.center[0]+0,
                                  self.center[0]+0,
                                  self.center[0]+0,
                                  self.center[0]+dx,
                                  self.center[0]+dx,
                                  self.center[0]+dx],
                                 [self.center[1]+dy,
                                  self.center[1]-0,
                                  self.center[1]-dy,
                                  self.center[1]-dy,
                                  self.center[1]-0,
                                  self.center[1]+dy,
                                  self.center[1]-dy,
                                  self.center[1]-0,
                                  self.center[1]+dy]])
                    }[nrPos]

            else:
                ds = (np.sqrt((self.rfov)**2.-(self.field_size/2.)**2.) -
                      self.field_size/2.)
                return{
                    1: np.array([[self.center[0]], [self.center[1]]]),
                    3: np.array([[self.center[0],
                                  self.center[0],
                                  self.center[0]],
                                 [self.center[1]+ds,
                                  self.center[1],
                                  self.center[1]-ds]]),
                    4: np.array([[self.center[0]-dm,
                                  self.center[0]-dm,
                                  self.center[0]+dm,
                                  self.center[0]+dm],
                                 [self.center[1]+dn,
                                  self.center[1]-dn,
                                  self.center[1]-dn,
                                  self.center[1]+dn]]),
                    9: np.array([[self.center[0]-dx,
                                  self.center[0]-dx,
                                  self.center[0]-dx,
                                  self.center[0]+0,
                                  self.center[0]+0,
                                  self.center[0]+0,
                                  self.center[0]+dx,
                                  self.center[0]+dx,
                                  self.center[0]+dx],
                                 [self.center[1]+dy,
                                  self.center[1]-0,
                                  self.center[1]-dy,
                                  self.center[1]-dy,
                                  self.center[1]-0,
                                  self.center[1]+dy,
                                  self.center[1]-dy,
                                  self.center[1]-0,
                                  self.center[1]+dy]])
                    }[nrPos]

class SpScanRegion:
    def __init__(self, center, slit_length, nr_steps, step_size, field_rotation,
                 slit_width, mylinewidth):
        self.press = None
        # all coordinates and lengths are in degrees
        self.center = center
        self.slit_length = slit_length
        self.nr_steps = nr_steps
        self.step_size = step_size
        self.field_rotation = field_rotation
        self.ax = plt.gca()
        self.slit_width = slit_width
        self.mylinewidth = mylinewidth
        self.redraw()

    def remove(self):
        try:
            self.pc.remove()
        except Exception:
            pass

    def redraw(self):
        center = self.center
        top_center = np.zeros(2)
        bot_center = np.zeros(2)
        ul = np.zeros(2)
        ll = np.zeros(2)
        ur = np.zeros(2)
        lr = np.zeros(2)
        beta = np.deg2rad(self.field_rotation)

        st = self.nr_steps*self.step_size/2.

        dx = self.slit_length/2.*np.sin(beta)
        dy = self.slit_length/2.*np.cos(beta)
        top_center[0] = center[0] - dx
        top_center[1] = center[1] + dy
        bot_center[0] = center[0] + dx
        bot_center[1] = center[1] - dy

        ul[0] = top_center[0] - st*np.cos(beta)
        ul[1] = top_center[1] - st*np.sin(beta)
        ll[0] = bot_center[0] - st*np.cos(beta)
        ll[1] = bot_center[1] - st*np.sin(beta)

        ur[0] = top_center[0] + st*np.cos(beta)
        ur[1] = top_center[1] + st*np.sin(beta)
        lr[0] = bot_center[0] + st*np.cos(beta)
        lr[1] = bot_center[1] + st*np.sin(beta)
        radii = np.zeros(8)
        radii[0] = np.sqrt(ul[0]**2.+ul[1]**2)
        radii[1] = np.sqrt(ll[0]**2.+ll[1]**2)
        radii[2] = np.sqrt(ur[0]**2.+ur[1]**2)
        radii[3] = np.sqrt(lr[0]**2.+lr[1]**2)

        radii[4] = np.sqrt(top_center[0]**2.+top_center[1]**2)
        radii[5] = np.sqrt(bot_center[0]**2.+bot_center[1]**2)


        #need better way see case were slit parallel to limb
        nr = self.nr_steps + 1
        r = np.zeros(4*nr)

        if (ul[0]-ll[0]) == 0:
            left_x = np.linspace(ul[0], ll[0], num=nr)
            left_y = np.linspace(ul[1], ll[1], num=nr)
            bot_x = np.linspace(ll[0], lr[0], num=nr)
            bot_y = np.zeros(nr)
            right_x = np.linspace(lr[0], ur[0], num=nr)
            right_y = np.linspace(lr[1], ur[1], num=nr)
            top_x = np.linspace(ur[0], ul[0], num=nr)

            for i in range(nr):
                y = left_y[i]
                r[i] = np.sqrt(left_x[i]**2 + y**2)
                y = ll[1]
                bot_y[i] = y
                r[i+nr] = np.sqrt(bot_x[i]**2 + y**2)
                y = right_y[i]
                r[i+2*nr] = np.sqrt(right_x[i]**2 + y**2)
                y = ur[1]
                r[i+3*nr] = np.sqrt(top_x[i]**2 + y**2)

        elif (ll[0]-lr[0]) == 0:
            left_x = np.linspace(ul[0], ll[0], num=nr)
            bot_x = np.linspace(ll[0], lr[0], num=nr)
            bot_y = np.linspace(ll[1], lr[1], num=nr)
            right_x = np.linspace(lr[0], ur[0], num=nr)
            top_x = np.linspace(ur[0], ul[0], num=nr)
            top_y = np.linspace(ur[1], ul[1], num=nr)

            for i in range(nr):

                y = ul[1]
                r[i] = np.sqrt(left_x[i]**2 + y**2)

                y = bot_y[i]
                r[i+nr] = np.sqrt(bot_x[i]**2 + y**2)

                y = lr[1]
                r[i+2*nr] = np.sqrt(right_x[i]**2 + y**2)

                y = top_y[i]
                r[i+3*nr] = np.sqrt(top_x[i]**2 + y**2)

        else:
            left_m = (ul[1]-ll[1])/(ul[0]-ll[0])
            right_m = (lr[1]-ur[1])/(lr[0]-ur[0])
            bot_m = (ll[1]-lr[1])/(ll[0]-lr[0])
            top_m = (ur[1]-ul[1])/(ur[0]-ul[0])

            left_x = np.linspace(ul[0], ll[0], num=nr)
            bot_x = np.linspace(ll[0], lr[0], num=nr)
            bot_y = np.zeros(nr)
            right_x = np.linspace(lr[0], ur[0], num=nr)
            top_x = np.linspace(ur[0], ul[0], num=nr)

            for i in range(nr):

                y = left_m*(left_x[i]-ul[0])+ul[1]
                r[i] = np.sqrt(left_x[i]**2 + y**2)

                y = bot_m*(bot_x[i]-ll[0])+ll[1]
                bot_y[i] = y
                r[i+nr] = np.sqrt(bot_x[i]**2 + y**2)

                y = right_m*(right_x[i]-lr[0])+lr[1]
                r[i+2*nr] = np.sqrt(right_x[i]**2 + y**2)

                y = top_m*(top_x[i]-ur[0])+ur[1]
                r[i+3*nr] = np.sqrt(top_x[i]**2 + y**2)

        slit_boxes = []

        # loop through all positions and add to list
        for x, y in zip(bot_x, bot_y):
            rect = Rectangle((x-self.slit_width/2, y), self.slit_width,
                             self.slit_length, self.field_rotation,
                             linewidth=self.mylinewidth,
                             )
            slit_boxes.append(rect)

        self.pc = PatchCollection(slit_boxes, facecolor=None, edgecolor='c', zorder=4,
                                  transform=get_world_transform(self.ax))
        self.ax.add_collection(self.pc)


        self.rmin = np.nanmin(r)
        self.rmax = np.nanmax(r)

class occulter:
    def __init__(self, center, rfov, rsun):
        self.press = None
        self.center = center # in degrees
        self.rfov = rfov    # radius of fov in degrees
        self.rsun = rsun    # in degrees
        self.ocr = self.rsun
        self.ax = plt.gca()
        self.redraw()

    def remove(self):
        # try:
        self.pc.remove()
        # except:
        #     pass

    def redraw(self):
        #TODO: hardcoded oculter values in this class
        if self.rfov <= 1.5/60:
            self.ocd = 56.92913/3600
        else:
            self.ocd = 82.40157/3600
        # if self.center[0] is 0:
        #     self.theta = 0
        # else:
            # self.theta = (np.rad2deg(np.arctan2(self.center[1]-self.rsun,
            #                                     self.center[0]-self.rsun)))
        self.theta = (np.rad2deg(np.arctan2(self.center[1],
                                            self.center[0])))
        alpha = np.deg2rad(self.theta)
        center = self.center
        a = Point(center[0], center[1]).buffer(self.rfov)
        b = Point(center[0]-(self.ocr+self.ocd)*np.cos(alpha),
                  center[1]-(self.ocr+self.ocd)*np.sin(alpha)).buffer(self.ocr)
        c = a.intersection(b)
        self.pc = PolygonPatch(c, fc='k', ec='k', alpha=1, zorder=2, linewidth=2,
                               transform=get_world_transform(self.ax))
        self.ax.add_patch(self.pc)

class MplCanvas(FigureCanvas):
    def __init__(self):
        self.m = sunpy.map.Map('sunpyObjects/aiaToUse.fits')
        # for transform purposes in sunpy to go from solar coordinates to pixel coordinates
        # the astropy coordinate sytem/axes expects values in degrees
        self.rsun = self.m.rsun_obs.value/3600. # degrees
        self.maxR = 1.5
        self.x0 = 1.1*self.rsun  # degrees
        self.y0 = 0 #0.1*self.rsun # degrees
        self.fov = 5./60. # degrees
        self.largeFov = self.fov
        self.center = [self.x0, self.y0]
        self.slit_length = 233/3600. # degrees
        self.nr_steps = 10
        self.step_size = 1/3600. # degrees
        self.field_rotation = 0. # degrees
        self.slit_width = 0.5/3600 # degrees
        self.ciFieldSize = 110/3600 # degrees
        self.numPos = 4
        self.invertedColors = True
        self.bgColor = 'white'
        self.lineColor = 'black'
        if hasattr(QtCore.Qt, 'AA_EnableHighDpiScaling'):
            self.mylinewidth = 0.5
        else:
            self.mylinewidth = 1.0
        if self.invertedColors:
            self.cmap = 'Greys'

        self.fig1 = plt.figure(frameon=True)
        self.fig1.set_facecolor(self.bgColor)
        FigureCanvas.__init__(self, self.fig1)

        self.ax1 = self.fig1.add_subplot(121, projection=self.m)
        # clip the brightest and faintest pixels
        self.m.plot_settings['cmap'] = plt.get_cmap(self.cmap)
        self.m.plot(annotate=False, clip_interval=(5, 98.)*u.percent)
        self.ax1.set_autoscale_on(False)
        self.ax1.coords[0].set_ticks_visible(False)
        self.ax1.coords[1].set_ticks_visible(False)
        self.ax1.coords[0].set_ticklabel_visible(False)
        self.ax1.coords[1].set_ticklabel_visible(False)
        self.ax1.coords[0].set_axislabel('')
        self.ax1.coords[1].set_axislabel('')
        self.ax1.coords.frame.set_color(self.bgColor)
        plt.grid(None)

        # plotting a grid takes long time?
        # self.m.draw_grid(grid_spacing=45*u.deg)
        self.m.draw_limb()
        c = Circle((0., 0.), self.maxR*self.rsun, edgecolor=self.lineColor,
                   transform=get_world_transform(self.ax1), facecolor='None', linestyle='dashed')
        self.ax1.add_patch(c)

        self.spscan1 = SpScanRegion(self.center, self.slit_length, self.nr_steps,
                                    self.step_size, self.field_rotation, self.slit_width,
                                    self.mylinewidth)
        # ci region wants fov radius
        self.ciscan1 = CiScanRegion(self.center, self.ciFieldSize, self.field_rotation,
                                    self.fov/2., self.slit_length, self.numPos,
                                    self.mylinewidth)

        # center coordinates and fov are in degrees here
        self.bottomLeft = SkyCoord(3600*(self.x0 - 1.1*self.largeFov/2)*u.arcsec,
                                   3600*(self.y0 - 1.1*self.largeFov/2)*u.arcsec,
                                   frame=self.m.coordinate_frame)
        self.topRight = SkyCoord(3600*(self.x0 + 1.1*self.largeFov/2)*u.arcsec,
                                 3600*(self.y0 + 1.1*self.largeFov/2)*u.arcsec,
                                 frame=self.m.coordinate_frame)


        self.ax2 = self.fig1.add_subplot(122, projection=self.m)
        self.m.plot(annotate=False, clip_interval=(5, 98.)*u.percent)
        self.ax2.set_autoscale_on(False)
        self.ax2.coords[0].set_ticks_visible(False)
        self.ax2.coords[1].set_ticks_visible(False)
        self.ax2.coords[0].set_ticklabel_visible(False)
        self.ax2.coords[1].set_ticklabel_visible(False)
        self.ax2.coords[0].set_axislabel('')
        self.ax2.coords[1].set_axislabel('')
        self.ax2.coords.frame.set_color(self.bgColor)
        plt.grid(None)

        self.pixelbl = self.m.world_to_pixel(self.bottomLeft)
        self.pixeltr = self.m.world_to_pixel(self.topRight)


        self.ax2.set_xlim([self.pixelbl[0].value, self.pixeltr[0].value])
        self.ax2.set_ylim([self.pixelbl[1].value, self.pixeltr[1].value])

        # plotting a grid takes long time?
        # self.subm.draw_grid()
        self.m.draw_limb(zorder=1)
        c = Circle((0., 0.), self.maxR*self.rsun, edgecolor=self.lineColor,
                   transform=get_world_transform(self.ax2), facecolor='None', linestyle='dashed')
        self.ax2.add_patch(c)
        plt.tight_layout(h_pad=0.5)

        self.occulter2 = occulter(self.center, self.fov/2, self.rsun)
        self.spscan2 = SpScanRegion(self.center, self.slit_length, self.nr_steps,
                                    self.step_size, self.field_rotation, self.slit_width,
                                    self.mylinewidth)
        self.ciscan2 = CiScanRegion(self.center, self.ciFieldSize, self.field_rotation,
                                    self.fov/2, self.slit_length, self.numPos,
                                    self.mylinewidth)


        self.dc = DraggableCircle(self.x0, self.y0, self.ax1, self.ax2,
                                  self.fov/2, self.spscan1,
                                  self.spscan2, self.ciscan1, self.ciscan2, self.occulter2,
                                  self.m, self.largeFov)

    def updateMap(self):
        pass

class sunmapwidget(QWidget):
    def __init__(self, parent=None):
        # initialization of Qt MainWindow widget
        QWidget.__init__(self, parent)
        # set the canvas to the Matplotlib widget
        self.canvas = MplCanvas()

        # create a vertical box layout
        self.vbl = QVBoxLayout()
        # set the layout to th vertical box
        self.setLayout(self.vbl)
         # add mpl widget to vertical box
        self.vbl.addWidget(self.canvas)
